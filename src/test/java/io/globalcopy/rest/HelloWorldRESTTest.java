package io.globalcopy.rest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HelloWorldRESTTest {
    @Test
    public void testHelloRest() {
        HelloWorldREST h = new HelloWorldREST();
        Assertions.assertEquals(h.hello(), "Hello World");
    }
}
