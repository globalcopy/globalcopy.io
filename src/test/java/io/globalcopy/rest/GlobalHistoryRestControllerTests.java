package io.globalcopy.rest;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.jupiter.api.Assertions.assertEquals;

//TODO: Add testing for headers in response entities
@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class GlobalHistoryRestControllerTests {

    //FIXME: Tests are outdated with encrytion implementation.
//    @Autowired
//    private CopyEntryRepository copyEntryRepository;
//    private GlobalHistoryRest res;
//
//
//    @Test
//    public void testUserHistory(){
//        res = new GlobalHistoryRest(copyEntryRepository);
//        res.generateAccounts();
//
//        List<CopyEntry> rtn1 = res.historyAt("Vuk").getBody();
//        assertEquals(rtn1.size(), 3);
//        assertEquals(rtn1.get(0).author, "Vuk");
//        assertEquals(rtn1.get(1).author, "Vuk");
//        assertEquals(rtn1.get(2).author, "Vuk");
//
//        assertEquals(rtn1.get(2).data, "Test 2");
//        assertEquals(rtn1.get(1).data, "Test 3");
//        assertEquals(rtn1.get(0).data, "Test 4");
//
//        assertEquals(rtn1.get(2).timestamp, OffsetDateTime.of(LocalDate.of(2023, 11, 9), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()));
//        assertEquals(rtn1.get(1).timestamp, OffsetDateTime.of(LocalDate.of(2023, 2, 2), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()));
//        assertEquals(rtn1.get(0).timestamp, OffsetDateTime.of(LocalDate.of(2023, 8, 16), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()));
//
//
//
//        List<CopyEntry> rtn2 = res.historyAt("Jermaine").getBody();
//        assertEquals(rtn2.size(), 1);
//        assertEquals(rtn2.get(0).author, "Jermaine");
//        assertEquals(rtn2.get(0).data, "Test 1");
//        assertEquals(rtn2.get(0).timestamp, OffsetDateTime.of(LocalDate.of(2023, 10, 4), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()));
//
//    }
//
//    @Test
//    public void testUserHistoryWithParameters(){
//        res = new GlobalHistoryRest(copyEntryRepository);
//        res.generateAccounts();
//
//        List<CopyEntry> rtn1 = res.historyAtSpecification("Vuk", 2L).getBody();
//        assertEquals(rtn1.size(), 2);
//        List<CopyEntry> rtn2 = res.historyAtSpecification("Vuk", 3L).getBody();
//        assertEquals(rtn2.size(), 3);
//        List<CopyEntry> rtn3 = res.historyAtSpecification("Vuk", 4L).getBody();
//        assertEquals(rtn3.size(), 3);
//
//        assertEquals(rtn3.get(0).data, "Test 4");
//        assertEquals(rtn3.get(1).data, "Test 3");
//        assertEquals(rtn3.get(2).data, "Test 2");
//
//        assertEquals(rtn3.get(2).timestamp, OffsetDateTime.of(LocalDate.of(2023, 11, 9), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()));
//        assertEquals(rtn3.get(1).timestamp, OffsetDateTime.of(LocalDate.of(2023, 2, 2), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()));
//        assertEquals(rtn3.get(0).timestamp, OffsetDateTime.of(LocalDate.of(2023, 8, 16), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()));
//
//
//        List<CopyEntry> rtn4 = res.historyAtSpecification("Jermaine", 1L).getBody();
//        assertEquals(rtn4.size(), 1);
//        assertEquals(rtn4.get(0).data, "Test 1");
//        assertEquals(rtn4.get(0).timestamp, OffsetDateTime.of(LocalDate.of(2023, 10, 4), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()));
//
//    }
//
//    @Test
//    public void testGetAllCopyEntries() {
//
//        res = new GlobalHistoryRest(copyEntryRepository);
//        res.generateAccounts();
//
//
//
//        assertEquals(4, res.getCopyEntries().getBody().size());
//    }
//
//    @Test
//    public void testPostingSingleEntries() {
//
//        res = new GlobalHistoryRest(copyEntryRepository);
//        res.generateAccounts();
//
//        assertEquals(4, res.getCopyEntries().getBody().size());
//        CopyEntry testEntry =  new CopyEntry(OffsetDateTime.of(LocalDate.of(2023, 4, 12), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()),
//                                          "Test 117");
//        testEntry.construct("Ben");
//        res.create(testEntry);
//        assertEquals(5, res.getCopyEntries().getBody().size());
//        assertEquals("Ben", res.historyAt("Ben").getBody().get(0).getAuthor());
//
//    }
//
//    @Test
//    public void testDelete() {
//
//        res = new GlobalHistoryRest(copyEntryRepository);
//        res.generateAccounts();
//
//        assertEquals(4, res.getCopyEntries().getBody().size());
//        res.deleteHistoryAtSpecification("Vuk", 2L);
//        assertEquals(2, res.getCopyEntries().getBody().size());
//        assertEquals(1, res.historyAt("Vuk").getBody().size());
//
//        res.deleteHistoryAtSpecification("Jermaine", 2L);
//        assertEquals(1, res.historyAt("Jermaine").getBody().size());
//
//        res.deleteHistoryAtSpecification("Jermaine", 1L);
//        assertEquals(0, res.historyAt("Jermaine").getBody().size());
//        assertEquals(1, res.getCopyEntries().getBody().size());
//
//
//
//    }
}

