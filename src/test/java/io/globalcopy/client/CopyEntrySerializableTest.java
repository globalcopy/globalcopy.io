package io.globalcopy.client;

import io.globalcopy.client.CopyEntry.CopyEntry;
import io.globalcopy.client.CopyEntry.Seralizer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;

import org.junit.jupiter.api.io.TempDir;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CopyEntrySerializableTest {

    @TempDir
    static Path tempDir;
    static Path tempFile1;
    static Path tempFile2;

    @BeforeAll
    public static void init() throws IOException {
        tempFile1 = Files.createFile(tempDir.resolve("CopyEntryTest.txt"));
        tempFile2 = Files.createFile(tempDir.resolve("CopyEntryTest2.txt"));

    }

    @Test
    void testSetCopyEntrySerializable(@TempDir Path mydir) throws IOException, ClassNotFoundException {

        ArrayList<CopyEntry> midEntry = new ArrayList<>();
        midEntry.add(new CopyEntry("test", OffsetDateTime.of(LocalDate.of(2023, 10, 4),
                LocalTime.of(0, 0, 0),
                OffsetDateTime.now().getOffset()), "globalcopy"));
        //store this in sample file.
        Seralizer s = new Seralizer();
        s.serialize(midEntry, mydir + "/CopyEntryTest.txt");


        //store retrieve from a sample file.
        ArrayList<CopyEntry> returnEntry = (ArrayList<CopyEntry>) s.deserialize(mydir + "/CopyEntryTest.txt");
        assertEquals(returnEntry.get(0).data, midEntry.get(0).data);



        assertEquals("globalcopy", midEntry.get(0).data);
        assertEquals(OffsetDateTime.of(LocalDate.of(2023, 10, 4),
                LocalTime.of(0, 0, 0),
                OffsetDateTime.now().getOffset()), midEntry.get(0).timestamp);
    }
    /*
    Generator Class for sample
     */
    @Test
    void testSetCopyEntrySerializableLoader(@TempDir Path mydir) throws IOException, ClassNotFoundException {

        ArrayList<CopyEntry> midEntry = new ArrayList<>();
        midEntry.add(new CopyEntry("test", OffsetDateTime.of(LocalDate.of(2023, 10, 4), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()), "globalcopy"));
        //store this in sample file.
        Seralizer s = new Seralizer();
        s.serialize(midEntry, mydir + "/CopyEntryTest.txt");



        //store retrieve from a sample file.
        ArrayList<CopyEntry> newMidEntry = (ArrayList<CopyEntry>) s.deserialize(mydir + "/CopyEntryTest.txt");
        assertEquals(newMidEntry.get(0).data, midEntry.get(0).data);



        assertEquals("globalcopy", midEntry.get(0).data);
        assertEquals(OffsetDateTime.of(LocalDate.of(2023, 10, 4),
                LocalTime.of(0, 0, 0),
                OffsetDateTime.now().getOffset()), midEntry.get(0).timestamp);
    }


}
