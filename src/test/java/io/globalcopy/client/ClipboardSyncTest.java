package io.globalcopy.client;

import io.globalcopy.client.CopyEntry.CopyEntry;
import io.globalcopy.client.CopyRetriever.CopyRetriever;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import java.time.*;

import java.util.ArrayList;

public class ClipboardSyncTest {


    @Test
    public void testClipBoardSyncInit() throws InterruptedException {

        CopyRetriever testRetriever =  new CopyRetriever() {
            ArrayList<CopyEntry> tempentry = new ArrayList<>();
            @Override
            public ArrayList<CopyEntry> getClipboard() {

                //We should update to this new CopyEntry.
                return tempentry;
            }

            @Override
            public void setClipboard(ArrayList<CopyEntry> entry) {
                //not usin
            }

            @Override
            public void setClipboardToEntry(CopyEntry data) {

            }

            @Override
            public void addNewClipboardEntry(CopyEntry entry) {
                    tempentry.add(entry);
            }

            @Override
            public CopyEntry getLastentry() {
                return tempentry.get(tempentry.size() -1 );
            }

            @Override
            public CopyEntry getCopyEntryAt(int index) {
                return tempentry.get(index);
            }


            @Override
            public boolean fixDesync() {
                // real implementations will have GUI solution here.
                //true -> run a POST request for the new data.
                //make sure we didn't tocuch this tempentry.
                return false;
            }

            @Override
            public boolean verifyData(CopyEntry entry) {
                return false;
            }

            @Override
            public boolean existsinEntryList(String dataCheck) {
                return false;
            }
        };
        CopyEntry entry = new CopyEntry("test", OffsetDateTime.of(LocalDate.now(), LocalTime.now(),
                OffsetDateTime.now().getOffset()),
                "new copy");
        testRetriever.addNewClipboardEntry(entry);



        RequestSender rs = Mockito.mock(RequestSender.class);


        ClipboardSync sync = new ClipboardSync(rs, testRetriever);

        sync.toggleSync(); //should disable the thread...
        sync.join();

        //we should have "new copy" and sync is complete.
        Assertions.assertEquals("new copy", sync.retriever.getLastentry().data);
        Assertions.assertEquals("new copy", testRetriever.getLastentry().data);
        Assertions.assertTrue(testRetriever.getLastentry().timestamp.isEqual(sync.retriever.getLastentry().getTimestamp()));





    }


    @Test
    public void testClipBoardSyncUpdate() throws Exception {

        CopyRetriever testRetriever =  new CopyRetriever() {

            ArrayList<CopyEntry> tempentry = new ArrayList<>();


            @Override
            public ArrayList<CopyEntry> getClipboard() {
                return tempentry;
            }


            @Override
            public void addNewClipboardEntry(CopyEntry entry) {
                tempentry.add(entry);
            }

            @Override
            public CopyEntry getLastentry() {
                return tempentry.get(tempentry.size() - 1);
            }

            @Override
            public CopyEntry getCopyEntryAt(int index) {
                //unused
                return null;
            }

            @Override
            public void setClipboard(ArrayList<CopyEntry> entry) {
                // real implementations will have GUI solution here.
                tempentry = entry;
            }

            @Override
            public void setClipboardToEntry(CopyEntry data) {

            }

            @Override
            public boolean fixDesync() {
                // real implementations will have GUI solution here.
                //true -> run a POST request for the new data.
                //also make sure that we changed this tempentry
                return false;
            }

            @Override
            public boolean verifyData(CopyEntry entry) {
                return false;
            }

            @Override
            public boolean existsinEntryList(String dataCheck) {
                return false;
            }
        };

        //This copy is outdated, we should give it a new copyentry.
        testRetriever.addNewClipboardEntry(new CopyEntry("test",OffsetDateTime.of(LocalDate.of(1800 , 10, 4),
                LocalTime.of(12 , 0, 0),
                OffsetDateTime.now().getOffset()),
                "old copy"));

        RequestSender sender = Mockito.mock(RequestSender.class);


        long offset = OffsetDateTime.now().toInstant().toEpochMilli() + 10000; //a future timestamp...
        Mockito.when(sender.sendGetReq()).thenReturn(new CopyEntry("test", OffsetDateTime.of(LocalDate.now(), LocalTime.now(), OffsetDateTime.now().getOffset()), "new copy"));



        ClipboardSync sync = new ClipboardSync(sender, testRetriever);
        sync.checkSync();
        Assertions.assertEquals("new copy", sync.retriever.getLastentry().data);

        Mockito.when(sender.sendGetReq()).thenReturn(new CopyEntry("test",OffsetDateTime.of(LocalDate.now(), LocalTime.now(), OffsetDateTime.now().getOffset()).plusSeconds(offset), "even more new copy"));
        sync.checkSync();
        Assertions.assertEquals("even more new copy", sync.retriever.getLastentry().data);




        //default copyrequestor -> CopyEntry recentPost = new CopyEntry(OffsetDateTime.of(LocalDate.of(2023, 10, 4), LocalTime.of(2, 55, 10), OffsetDateTime.now().getOffset()), "REPLACEME");
        //TODO: in future iterations this will be replaced with the actual GlobalCopy Data. It should lose since our local clipboard gets the timestamp of "now" at startup.





        //we have replaced the copy

        Assertions.assertEquals("even more new copy", sync.retriever.getLastentry().data);



    }

    @Test
    public void testClipBoardSyncMultipleEntries() throws InterruptedException {

        CopyRetriever testRetriever =  new CopyRetriever() {
            ArrayList<CopyEntry> tempentry = new ArrayList<>();
            @Override
            public ArrayList<CopyEntry> getClipboard() {

                //We should update to this new CopyEntry.
                return tempentry;
            }

            @Override
            public void setClipboard(ArrayList<CopyEntry> entry) {
                //not usin
            }

            @Override
            public void setClipboardToEntry(CopyEntry data) {

            }

            @Override
            public void addNewClipboardEntry(CopyEntry entry) {
                tempentry.add(entry);
            }

            @Override
            public CopyEntry getLastentry() {
                return tempentry.get(tempentry.size() -1 );
            }

            @Override
            public CopyEntry getCopyEntryAt(int index) {
                return tempentry.get(index);
            }


            @Override
            public boolean fixDesync() {
                // real implementations will have GUI solution here.
                //true -> run a POST request for the new data.
                //make sure we didn't tocuch this tempentry.
                return false;
            }

            @Override
            public boolean verifyData(CopyEntry entry) {
                return false;
            }

            @Override
            public boolean existsinEntryList(String dataCheck) {
                return false;
            }
        };
        testRetriever.addNewClipboardEntry(
                new CopyEntry("test", OffsetDateTime.of(LocalDate.now(), LocalTime.now(),
                        OffsetDateTime.now().getOffset()),
                        "new copy1"));
        testRetriever.addNewClipboardEntry(
                new CopyEntry("test", OffsetDateTime.of(LocalDate.now(), LocalTime.now(),
                        OffsetDateTime.now().getOffset()),
                        "new copy2"));
        testRetriever.addNewClipboardEntry(
                new CopyEntry("test", OffsetDateTime.of(LocalDate.now(), LocalTime.now(),
                        OffsetDateTime.now().getOffset()),
                        "new copy3"));
        testRetriever.addNewClipboardEntry(
                new CopyEntry("test", OffsetDateTime.of(LocalDate.now(), LocalTime.now(),
                        OffsetDateTime.now().getOffset()),
                        "new copy4"));


        RequestSender rs = Mockito.mock(RequestSender.class);

        ClipboardSync sync = new ClipboardSync(rs, testRetriever);

        sync.toggleSync(); //should disable the thread...
        sync.join();

        //we should have "new copy" and sync is complete.
        Assertions.assertEquals("new copy4", sync.retriever.getLastentry().data); //moost recent entry -> end of list
        Assertions.assertEquals("new copy4", testRetriever.getCopyEntryAt(3).data); //for clarity
        Assertions.assertEquals("new copy3", testRetriever.getCopyEntryAt(2).data);
        Assertions.assertEquals("new copy2", testRetriever.getCopyEntryAt(1).data);
        Assertions.assertEquals("new copy1", testRetriever.getCopyEntryAt(0).data);
        Assertions.assertTrue(testRetriever.getLastentry().timestamp.isEqual(sync.retriever.getLastentry().getTimestamp()));





    }




}
