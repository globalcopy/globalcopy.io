package io.globalcopy.client;

import org.junit.Rule;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


public class PropertyManagerTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();



    @Test
    public void generateNewPropertyTest() throws IOException {
        folder.create();

        List<String> fileList = Arrays.asList((Objects.requireNonNull(folder.getRoot().list())));
        Assertions.assertTrue(fileList.isEmpty());

        //add our new property
        PropertyManager.generateNewPropertyFile(folder.getRoot().toPath().toAbsolutePath() + "/user.properties", "Vuk", "globalcopy.io", "password", "foo@example.com");

        List<String> fileListAfter = Arrays.asList((Objects.requireNonNull(folder.getRoot().list())));
        Assertions.assertFalse(fileListAfter.isEmpty());
        folder.delete();
    }


    @Test
    public void generateropertyAlreadyExistsTest() throws IOException {
        folder.create();
        //add our new property
        List<String> fileList = Arrays.asList((Objects.requireNonNull(folder.getRoot().list())));
        Assertions.assertTrue(fileList.isEmpty());
        PropertyManager.generateNewPropertyFile(folder.getRoot().toPath().toAbsolutePath() + "/user.properties", "Vuk", "globalcopy.io", "password", "foo@example.com");


        List<String> fileListAfter = Arrays.asList((Objects.requireNonNull(folder.getRoot().list())));
        Assertions.assertFalse(fileListAfter.isEmpty());

        //Exception caused on new generate
        PropertyManager.generateNewPropertyFile(folder.getRoot().toPath().toAbsolutePath() + "/user.properties", "Vuk", "globalcopy.io", "password", "foo@example.com");
        fileListAfter = Arrays.asList((Objects.requireNonNull(folder.getRoot().list())));

        Assertions.assertFalse(fileListAfter.isEmpty());
        folder.delete();
    }

    @Test
    public void generateropertyAlreadyExistsVerifyDataTest() throws IOException {
        folder.create();
        //add our new property
        List<String> fileList = Arrays.asList((Objects.requireNonNull(folder.getRoot().list())));
        Assertions.assertTrue(fileList.isEmpty());
        PropertyManager.generateNewPropertyFile(folder.getRoot().toPath().toAbsolutePath() + "/user.properties", "test", "globalcopy.io", "password", "foo@example.com");

        List<String> fileListAfter = Arrays.asList((Objects.requireNonNull(folder.getRoot().list())));
        Assertions.assertFalse(fileListAfter.isEmpty());

        //parse the properties file
        File file = new File(folder.getRoot().toPath().toAbsolutePath() + "/user.properties");
        if (!file.createNewFile()) {
            //generate a key for the user.
            Properties appProps = new Properties();
            appProps.load(new FileInputStream(file));
            assertEquals("test", PropertyManager.getAuthor());
            assertEquals("globalcopy.io", PropertyManager.getLink());
            assertFalse(appProps.get("key").toString().isEmpty());
        folder.delete();
        } else {
            fail("Attempted to create new file after generateNewPropertyFile");
        }
    }

    @Test
    public void generateropertyLoad() throws IOException {
        folder.create();
        //add our new property
        List<String> fileList = Arrays.asList((Objects.requireNonNull(folder.getRoot().list())));
        Assertions.assertTrue(fileList.isEmpty());
        PropertyManager.generateNewPropertyFile(folder.getRoot().toPath().toAbsolutePath() + "/user.properties", "test", "globalcopy.io", "password", "foo@example.com");

        List<String> fileListAfter = Arrays.asList((Objects.requireNonNull(folder.getRoot().list())));
        Assertions.assertFalse(fileListAfter.isEmpty());

        //parse the properties file
        File file = new File(folder.getRoot().toPath().toAbsolutePath() + "/user.properties");
        if (!file.createNewFile()) {
            //generate a key for the user.
            PropertyManager.loadPropertyFile(folder.getRoot().toPath().toAbsolutePath() + "/user.properties");
            assertEquals("test", PropertyManager.getAuthor());
            assertEquals("globalcopy.io", PropertyManager.getLink());
            folder.delete();
        } else {
            fail("Attempted to create new file after generateNewPropertyFile");
        }
    }

}
