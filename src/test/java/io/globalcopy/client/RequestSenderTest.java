package io.globalcopy.client;


import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.*;

import java.util.Base64;
import java.util.Properties;

import io.globalcopy.client.CopyEntry.CopyEntry;
import io.globalcopy.security.Encryptor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.crypto.spec.IvParameterSpec;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class RequestSenderTest {
    HttpClient mockHC = Mockito.mock(HttpClient.class);
    HttpResponse<String> mockHR = Mockito.mock();

//    HttpClient client = HttpClient.newHttpClient();

//TODO: Still in progress. Requires app to be running to pass. Consider as manual test
//    @Test
//    void testClientPostSend() throws IOException, InterruptedException {
//        RequestSender sender = new RequestSender("http://localhost:8080", "Marcus");
//        CopyEntry entry1 =  new CopyEntry(OffsetDateTime.of(LocalDate.of(2023, 12, 20), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()),
//                "Test 20");
//        entry1.construct("Marcus");
//        CopyEntry sentEntryRes = sender.sendPostReq(entry1);
//        Assertions.assertEquals("Test 20", sentEntryRes.getData());
//        Assertions.assertEquals("Marcus", sentEntryRes.getAuthor());
//    }

//TODO: Still in progress. Requires app to be running to pass. Consider as manual test
//    @Test
//    void testClientHeadSend() {}

//TODO: Still in progress. Requires app to be running to pass. Consider as manual test
//    @Test
//    public void testClientGetSend() throws IOException, InterruptedException {
//        RequestSender sender = new RequestSender(client, "Jermaine");
//        sender.sendGenerateReq();
//        CopyEntry latestEntry = sender.sendGetReq();
//        System.out.println("Latest Entry: " + latestEntry);
//    }

    @Test
    public void testMockClientGetSend() throws Exception {

        Properties appProps = new Properties();
        appProps.setProperty("key", Base64.getEncoder().encodeToString(Encryptor.generateAESKey("password").getEncoded()));
        appProps.setProperty("hmac", Base64.getEncoder().encodeToString(Encryptor.generateHMACKey("password").getEncoded()));
        IvParameterSpec spec = new IvParameterSpec(new byte[16]);
        Encryptor encryptor1 = new Encryptor(spec);

        Mockito.when(mockHC.send(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any())).thenReturn(mockHR);
        RequestSender sender = new RequestSender(mockHC, "test", encryptor1, "popo");
        Mockito.when(mockHR.body()).thenReturn("[{\"timestamp\": 1696643790, \"data\": \""+encryptor1.encryptData("new copy")+"\", \"author\": \"test\"}]");

        CopyEntry res = sender.sendGetReq();
        Assertions.assertEquals("new copy", res.getData());
        Assertions.assertEquals(OffsetDateTime.of(LocalDateTime.of(LocalDate.of(2023, 10, 7), LocalTime.of(1, 56, 30)), OffsetDateTime.now().toZonedDateTime().getOffset()).toLocalDateTime(), res.getTimestamp().toLocalDateTime());

        Mockito.verify(mockHC).send(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any());
        Mockito.verify(mockHR).body();
    }
// TODO: Needs to be fixed along with getting the last-modified header
//    @Test
//    public void testMockClientHeadSend() throws Exception {
//
//        Properties appProps = new Properties();
//        appProps.setProperty("key", Base64.getEncoder().encodeToString(Encryptor.generateAESKey("password").getEncoded()));
//        appProps.setProperty("hmac", Base64.getEncoder().encodeToString(Encryptor.generateHMACKey().getEncoded()));
//        IvParameterSpec spec = new IvParameterSpec(new byte[16]);
//        Encryptor encryptor1 = new Encryptor(appProps, spec);
//        Mockito.when(mockHR.headers()).thenReturn(HttpHeaders.of(new HashMap<>(){{put("last-modified",new ArrayList<>(){{add("1696643790");}});}}, (k, val) -> true));
//        Mockito.when(mockHC.send(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any())).thenReturn(mockHR);
//        RequestSender sender = new RequestSender(mockHC, "test token", encryptor1);
//
//        String res = sender.sendHeadReq();
//        Assertions.assertEquals("1696643790", res);
//
//        Mockito.verify(mockHC).send(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any());
//        Mockito.verify(mockHR).headers();
//    }

    @Test
    public void testMockClientPostSend() throws Exception {
        //Create Encryptor for Request Sender
        Properties appProps = new Properties();
        appProps.setProperty("key", Base64.getEncoder().encodeToString(Encryptor.generateAESKey("password").getEncoded()));
        appProps.setProperty("hmac", Base64.getEncoder().encodeToString(Encryptor.generateHMACKey("password").getEncoded()));
        IvParameterSpec spec = new IvParameterSpec(new byte[16]);
        Encryptor encryptor1 = new Encryptor(spec);
        //Establish Mocked Client Response
        Mockito.when(mockHC.send(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any())).thenReturn(mockHR);
        //Create Request Sender
        RequestSender sender = new RequestSender(mockHC, "test token", encryptor1, "popo");
        //String offset = "2023-12-20T04:00:00Z";
        OffsetDateTime timeToSend = OffsetDateTime.of(LocalDate.now(), LocalTime.now(), OffsetDateTime.now().getOffset());
        //Establish body of HttpResponse based on manual testing
        Mockito.when(mockHR.statusCode()).thenReturn(HttpStatus.CREATED.value());
        //Mockito.when(mockHR.body()).thenReturn("{\"timestamp\" : \""+timeToSend+"\", \"data\": \""+encryptor1.encryptData("new copy")+"\" , \"author\": \"test\"}");

        boolean res = sender.sendPostReq(new CopyEntry("test",timeToSend, "new copy"));
        Assertions.assertTrue(res);
        Mockito.verify(mockHC).send(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any());
        Mockito.verify(mockHR).statusCode();
    }
}
