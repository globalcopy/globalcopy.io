package io.globalcopy.client;


import io.globalcopy.client.CopyEntry.CopyEntry;
import org.junit.jupiter.api.Test;


import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class CopyEntryTest {

    @Test
    void testSetCopyEntry() {
        CopyEntry midEntry = new CopyEntry("test", OffsetDateTime.of(LocalDate.of(2023, 10, 4), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()), "globalcopy");

        assertEquals("globalcopy", midEntry.data);
        assertEquals(OffsetDateTime.of(LocalDate.of(2023, 10, 4), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()), midEntry.timestamp);
    }

    @Test
    void testCompareCopyEntryTimeStamp() {
        CopyEntry midEntry = new CopyEntry("test",OffsetDateTime.of(LocalDate.of(2023, 10, 4), LocalTime.MIDNIGHT, OffsetDateTime.now().getOffset()), "globalcopy");
        CopyEntry  oneEntry = new CopyEntry("test",OffsetDateTime.of(LocalDate.of(2023, 10, 4), LocalTime.of(13, 55, 0), OffsetDateTime.now().getOffset()), "globalcopy");

        assertTrue(midEntry.timestamp.isBefore(oneEntry.timestamp));
        assertEquals(midEntry.data, oneEntry.data);
    }

    @Test
    void testCompareCopyEntryData() {
        CopyEntry midEntry = new CopyEntry("test",OffsetDateTime.of(2023, 10, 4, 13, 55, 0, 0, OffsetDateTime.now().getOffset()), "new string data");
        CopyEntry  oneEntry = new CopyEntry("test",OffsetDateTime.of(2023, 10, 4, 13, 55, 0, 0, OffsetDateTime.now().getOffset()), "globalcopy");
        assertNotEquals(midEntry.data, oneEntry.data);
        assertEquals(midEntry.timestamp, oneEntry.timestamp);
    }



}
