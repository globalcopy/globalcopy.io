package io.globalcopy.client.conversion;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/*
Verifying the image is necessarily similiar requires a UI.
 */

public class ParseImgTest {
    String base64png = "iVBORw0KGgoAAAANSUhEUgAAAF8AAAAlCAYAAAAk9TVSAAAF60lEQVR4Xu1ZTWgbRxQ2LKjUAgssbGowxBYEgbDTiBQfDHYNbQyqLiIxOYhS04sPFpT6shf1Ih90CQ4FHVwXfDKiaQkmxhBoQPgQQgUursFuCcYEBftQ+SCfdNhLvr7dnZ2d/VNXjp1Nwn7wDpqZN7PzzZv3M+pBiMDQY28I8fYQkh8gQvIDREh+gAjJDxAh+QEiJD9AhOQHCAv5+3ICPT09nhLpG0RqehFre+dQRMUPBO2XNawsTiM12IcI33cEfV/8iFf2wR3RxEY2xvSz2LB3M3RFvikRjBV3P5wDUE7w6/woPnLsk0lCxr5dxxMKduUkJK7fLfm02F+vX4tdNOc5TutryA1LbNIhFJ5bh7yXUHZRnjCsNILRuRXUDs/Q1jrbeFlbQe7Lom/ymxtZxCyHdxnkGzguYZxNnCz+be99z0BWusT2LCWx8Pu/9gHdoVnBlKRyk8DS0uwVkE9TZRn5CdmvPbyjOC4jrZElIV0+hteOfYFukJxUvYKEpEwumW7AlZLvZvmtPytYvKMGrUH0RYTrF+nDYCoDuXrArrQ72gdVyJkU+nsN99YDqbefdKexuLyFvXP3SGPoDfZFmJ6E3v4UMqUnOHFXwfPCkD52aAE7HmP8wQywsewG/SJcBfnK5j1E1TFSGmQsDvgJ2rGJ+y4+lFxAcUzIMtzFedtIrzxh87NWkYbz2NQYEbEPOaH3Dyzs2Du7gBlgpaSMXeMQL5X89hnqa3MY1aw5hpnKC9dsZ794E9cpHV3eqqNxZtp4+6yOasEgScLMakvQgmqGGGJ9ya+rqHNdBeenDdS3lrE4fR03i1byzQAnYThHwfKIpcGUIBw9ljER0wmOkkVaVmyRf2aHk10/x15VRibVj17NDalCKebILcyvPPO8OSr4+jEiWTzgC5PfUT7Gze+38bKT7/BEC6szzJ3MrlsOj699bQm7QntHKNvIM3ITS+6pr7KzwA51HCXxpu7LSLA9SZLp4twkMlbAU8fNITTXMRtVx1CA5SbPcDXkM5HiuO1h/Z0g3i7Rhnl79B42fU6qBjUtn+6oY7qXdFkolTg5RH78Bll4DQeW21bHWm6Y5+tRMhYL//YAK/apuDD5bm5HBbmeRr2KAs+LY8g9tLkPDfTxe1t4IH+DO9MUBCn4Dvb3CoWHk3weSwwyKLjWG0a+7Y6dhQF9LtstsmMjq89r8e2cnAQcYYRDoTVYUGbjdFZcAqwdl06+AeUf/DDuQeKLCr76xMg4OoijamxiM29amim6770rV3FgOwmDVN+SFWjwRT4s7mmq0mLk6xmfJcDacWXkE16V02zyKVQM4+dFBonUi+sZGQ9+qeGw0UCDWbGX29Gh3hhKGe/ewoj9pqgSGUNR2O0bkb+7hGta+wA6JzvWukanxWzrSsT18Qbku1kO16dqUfYwic7k26A9aWxhef5TxI1DTZf5I9d2Puq6KV8Qsh3Vor1hrWveCfLN4GxeK26JHXxwV+QLOC6NO9ZrVab0tgEqksTBvvAK5bT+vdLMqjUNFUHZ0oC2bhT5bcPn+8CVuZ0mnTxL8cQPf5hjKdt4CS71F1S//tsdFiRt5G/MT0B+cuJ5aJzoaB7bvHEVM+x5wDXj4GjjYC2H7x5aW5s0p+7ahvDt07YLsU2sz7LbFaN1vRdw4nLJV9OvQ9R+LmAyzki2uZfW6gzbTAwT8mMcGU8BaoFWLeBzMRDbyddujYT4jXms1A7Bsz7SPRSKpRiZn8iBSSDpThZQrZ/CWFY5P6V1qXga1dd1eCeeLqp7GUbupz/YurTXoxqWb8f5frKWCsoHLky+H6HgV3BUHuKfCE6R4pPITblb/qO5/8+QImNFl+xCfV6YNGOCp0Qw98iuS2huIs+fyV3kgvXM5ZOvPo55pH0cygmelDJI8Qcua5nu7fM9Mh3tQW4ai2udH+SUk2fOf6Eo4+pn3+v1IKeBvvnZyjw+GzF11ce8VKaExxcr5bsnP8TbRUh+gAjJDxAh+QEiJD9AhOQHiJD8ABGSHyBC8gPEf+ts72AQxVznAAAAAElFTkSuQmCC";
    String globalcopyasciipng = "iVBORw0KGgoAAAANSUhEUgAAAvQAAACaCAYAAADGvdwVAAAf40lEQVR4Xu3d73NURbrA8fv39GYmsYaKg0UkIVoLlmJVqFTgJlURxKVWgqFyWZCi1HKDmtxQGHe1MKiA0QUsIiFkDJn8c89lJpeAc5jTZ/rpnvNjvi8+L5bm6T79dJ/uZwl4/usvf/mLAAAAAMin/2r9BQAAAAD5QUEPAAAA5BgFPQAAAJBjFPQAAABAjlHQAwAAADlGQQ8AAADkGAU9AAAAkGMU9AAAAECOUdADAAAAOUZBDwAAAOQYBT0AAACQYxT0AAAAQI5R0AMAAAA5RkEPAAAA5FimCnrzzhW5v7UlWxHXZbpkdO1lS7stPkF763y6zfZ8tvbW/pAtseuXYH+39ge8yLZ/bO2t/eVNY373InNLPv+021vnA6C3ZKug76/KyOiojEYMScWYzLe3zqfbbM9na2/tD9liWz9be2t/wIts+8fW3tpf3tjml/X21vkA6C2ZKugBAAAAdIaCHgAAAMgxCnoAAAAgxyjoAQAAgByjoAcAAAByjIIeAAAAyDEKegAAACDHKOgBAACAHKOgD8CYPjly8pJ8OFaNtPlgphaffyVw9aK86fmjIqGfH/FYX4TU2F9/sL8Ki/wDvYmCPgBjyvL+NzuyemE00uaDmb4mO/fmZaLxlcDhqpS9X8hhnx/xWF+E1NhfdfZXYZF/oDelUtAbc1Dm7uzIT+cPRtqKIPSB2iz4frkko54v4mdCPz/isb4IqVnQ9/D+4v4BUEQU9AGEPlAp+IqN9UVIFPTcPwCKJ6WCvirnbu7Iytn9kbYiCH2gUvAVG+uLkCjouX8AFE9KBX1JTi3XZflUKdJWBKEPVAq+YmN9ERIFPfcPgOJJpaAvutAHKgVfsbG+CKnXC/qiI/9Ab6KgDyD0gUrBV2ysL0KioC828g/0Jgr6AEIfqBR8xcb6IiQK+mIj/0BvClbQlweHpPpKX+TXe0HoA5WCr9hYX4TUCwU990+6+QfQfUEKelM9Ld/WdmT77mU5HOjSyLLQByoFX7Gxvgip6AU990+6+QeQjjAF/b4T8tWDmjxaOSeHOFC9o+ArNtYXIRW+oOf+STX/ANIRpKAPxbxzRe5vbclWxHWZLpn49rKlveTv4A99oDYLvvq2PGk8++pFedPzpRX6+RGP9UVIzYKe/dUx2/2hbW8dz1VR8w8gXr4K+v6qjIyOymjEkFSeXkra9tbxXIU+UE3lwPNnH65K2eOzN4R+fsRjfRES+8uN7f7QtreO56qo+QcQL1cFPQAAAIA/o6AHAAAAcoyCHgAAAMgxCnoAAAAgxyjoAQAAgByjoAcAAAByjIIeAAAAyDEKegAAACDH1AV9deysXPzgbRnw+GGMtBgzIpNzF2TykG4uxvTJkZOX5MOxaqStG7Tja+MRlnZ9tPEoNu3+0MZ3gvsnqpv5B5AdqoLemP0ys7JdmC/SGVOR099syW+fHI20dSLtL/Vpx9fGIyzt+mjjUWza/aGNT4r75+W6lX8A2aIr6A/MyI/1VZlT/olCK2MOytydHfnp/MFIW2iVU8uy9eAzOdrnPqe0D1Tt+Np4hKVdH208ik27P7TxSXH/vFy38g8gW1QF/dBHP0j9zpwMe/5xZ5oHqqlMy3JtTT59ty/SllTaB6p2fG08wtKujzYexabdH9r4pLh/Xq5b+QeQLc4FvTFDMnurLrfOD0fatIypyrmbO7Jydn+kLTRjBuS96zVZ+3xM+hwvirQPVO342niEpV0fbTyKTbs/tPFJcP+01438A8ge94L+9Vm5Xb8ls6+7HTpxjCnJqeW6LJ8qRdq6YWBqSWoP52XM8ceeaR+o2vG18QhLuz7aeBSbdn9o45Pg/mmvG/kHkD3OBf3I3G2p/zgrQ45/ipBlpn9SljbXZf6Y24Ge9oGqHV8bj7C066ONR7Fp94c2PoneuH/KkbYkupF/ANnjVNAbMywX7tRl5dxQpK0IGgfi5OJjWf9iXEoOF0baB6p2fG18msyRi/Lr1pZsPVNblP9+YQ0b7b/EtOeBdn008b2Q3zhJ5h/Xngea/eEj3qZ5//zM/dNO6Pxr2N4PW3uvs+XH1o5icyvoD12Qn+s3Zea14m6U8vEFefxoQcbLnc8x7QNVO742Pk2mvyrDo6MyuueAVF488CzteaBdH028LX+29ryzzc/Wngea/eEj3qZx/6z2wP2zkdP7J47t/bC19zpbfmztKDangv6Nf6zK9vczUlVslPLgkFRfcf+X/KGZ0oQsbGzIwkTnP/ZM+0DVjq+N74bQ+yd0/xra9UkSH3r+ofvPuizPP8n+iKONt+mZ++dRPu+fhtD5Dd1/2kLPL3T/SEfHBb0xb8jHv27Ljb+7f4XOVE/Lt7Ud2b57WQ4rDuWQGofixJfrsrF4ouMfe6Z9oGrH18aHFnr/NPvfDNe/lnZ9bPFdyW/A/rWMeU3GP7okl84cDfKnW9mff/z+sNHGx+mt++dhPu+fwPkN3X/aQs8vdP9IT+cF/eGP5e6TG3LmVfeNYPadkK8e1OTRyjk5lOENVR6/KuubS3Kiv7NnTP1AVY6vjQ8t9P4J3b+Wdn1s8aHnH7p/LbPvb/JdfUd+ufjXSJsPmZ+/ZX/YaOPjcP/Yhcx/EqHzG7r/tIWeX+j+kZ6OC/q3Lt+VJ9+dkcEe2AimNCZXH9ZkaWpg93+/c0Xuv/gPTvZcl+nS83ykfqAqx9fGIyzt+mjji65v4kvZrN+W2eHin3Evo90f2vg43D+td0/27p9elXR94Cbt/NrGt7W39hdCRwW9MUfkyv1t+fcHg5G2IjKmT8bm16R2bVoGnl4gjX9wMvKnf3DyzNCf/2FKygeqdnxtPMLSro82vugON4rGlbOyvweKxpfR7g9tfDvcP/m4f3pV0vWBm7Tzaxvf1t7aXwgdFfQAUGTGvCoz32/1TNEIACgGCnoAAAAgxyjoAQAAgByjoAcAAAByjIIeAAAAyDEKegAAACDHKOgBAACAHKOgBwAAAHLMqaBvfPDiyMlL8uFYNdKWhDYe8bT51cZ3ojp2Vi5+8Hbzwymtbd1mzIhMzl2QyUNuz6KNTyqv66PNjzY+tKw/X1La/aWNt9H2r43vJT7ffy3t+6WN3+tnavH5V0BXL8qbAXPjM//a+Wvjk3LNr6/ncx0/CVM5Lf/a+4LsLTnv+WvkjgV940t0decv0fElu7C0+dXGJ2XMfplZ2Q4+Tqvq1JL8vrYoxwf//DIZU5HT32zJb58cjcQkoY1PKuvrk/X8tns+LV/Plzbt/tLG22j718b3Ctf3X6vd+6l9v7Txe/1MX5Ode/My0fgK6HBVyh4Lvhe55j8+f384z99X/mxc8+vr+VzHT8L07ZOhRr9HzsvKzqrMKf/PRysK+gLS5lcbn5Q5MCM/1v1vamMOytydHfnp/MFI2257RY798578/vVJ2d/yslZOLcvWg8/kaJ/bM2njk9Cujy0/e7+vzfrY4rOe37jn222Pn18cH8+XNv3+0sXbaPvXxveKdu+/lu39ins/G+/XH4r3y8f72Sz4frkkoy85O5KwzX/v97XJvy3elj/N/LX5T0KTX+38GjTjJ2X635dvKeiRhDa/2vikhj76Qep35mTY84tjO/DimMq0LNfW5NN3+yJtSWjjk9CuT9L8tFufpPEvo82PNj6JNOeXBfr9pYu30favje8V7d5/LfX7ten+fvl4P7UFX9L5t8t/0viX0c5fm/8kNPnVzq/Zh2L8pCjokZg2v9r4JIwZktlbdbl1fjjSpmVMVc7d3JGVs/sjbTbGDMh712uy9vmY9Dm80Nr4JLTrkyQ/ceuTJL4dbX608UmkOb9mH5UJ+eK3mqzfmJERxz409PtLF2+j7V8b3wvi3n8t/fu16fx+eXk/lQVfkvnH5T9JfDva+Wvzn4Qmv9r5NftQjJ8UBT0S0+ZXG5+EeX1Wbtdvyezrfjd0gzElObVcl+VTpUhbEgNTS7L5cF7GHH9s14ivKeJttOuTJD9x65MkPo42P9p4m7TnZ6pPD/vajmzfvSxHAl4q7ej3ly7eRtu/Nr4XxL3/Wmm/X9rzXVvwJZl/XP6TxMfxkT9NvI02v9rn046fBAU9EtPmVxufxMjcban/OCtDAV8aV6Z/UpYer8v8MbcDsxm/6R5vk/f10ebneXw50pYF2vk1lAeHpFpx/7GxhnZ/aeNttP1r43tByPdfS/t+qc/3LhR8IfPvJX+KeBttfrXPpx0/CQp6JKbNrzbexphhufBzXVbODUXasqAx/8nFDVn/YlxKDi/1bvxj53ibvK+PNj/a+NCy/nw22v2ljbfR9q+NL7rQ77+W9v1Sn++BC77Q+feTP/d4G21+tc+nHT8JCnokps2vNt7GHLogq/WbMvOa383sU/n4gjx+tCDjZbdn1MbHKcL6aPPTiN9QxIemnV+atPtLG2+j7V8bX3TdeP+1tO+XJj50wdeN/Gvm7yM+jo/8ap7Px/g2FPRITJtfbbzNG/9Yle3vZ6SqeGGafyXhlXB/JcGUJmRhY0MWJtz+Woc2Pk4e1sdGm59m/CP3+NC08+vrr8grDpeRD9r9pY230favjS86H+9/kc/n0AWfj/zbaObvIz6Oj/xqns/H+DYU9EhMm19tfBxj3pCPf92WG393/0qjqZ7e+0eDhwO9dI0cTHy5LhuLJ9x+bKeMj5P19UlCm5/d+IfO8aFp5mcGp+XrjbpsrV6Qv3YY64N2f2njbbT9a+OLzMf7X/TzOWTB5yP/SWjm7yM+jo/8vvh8nX4Yysf4NhT0SEybX218HHP4Y7n75IacedV9I5t9J+SrBzV5tHJODgV86crjV2V9c0lO9LuNoY1vJ+vrk5Q2P9r40Fyfz+w73pX93Y52f2njbbT9a+OLzMf7X/TzOWTB5yP/SbnO31d8O77yu/d8A53142v8OBT0Hph3rsj9rS3Zirgu0yXjpf1epO15e+vzhKLNrzY+zluX78qT787IYMCXxRdTGpOrD2uyNDUQaUtCG99OUdZHmx9tfKdi3/+X/PWYbj+fL9r9pY230favjS+ybr7/Wtr3yzW+WfDVt+VJ491fvShvesxVN/PvOn9f8e34yq/r8/ka/2VM5bT8q3lnbEudgl7H9FdlZHRURiOGpPJ00UK3tz5PKNr8auPbMeaIXLm/Lf/+YDDSlkXG9MnY/JrUrk3LgMP6aePbKcr6aPOjje9Up+93t5/PF+3+0sbbaPvXxhdVt99/Le375RpvKgeev/vD1Y7/Skc73c6/6/x9xbfjK7+uz+dr/JcxfftkaO/eOCiDnv+g16mgBwAAAJANFPQAAABAjlHQAwAAADlGQQ8AAADkGAU9AAAAkGMU9AAAAECOUdADAAAAOUZBDwAAAORYRwW9MSMyOXdBJj1/3SpLnn/Jq+GWnB/2P9fGBw+OnLwkH45VI20+aPvXxtu49p+X/ec6v6RC9Z9GfqtjZ+XiB2939uEPj/O3jW9rfxnt82njtbTja+PbSWN/5omv/Liun6/xQ3OdX1Ku/aeRvzTON1d5yY/P+E51WNBX5PQ3W/LbJ0cjbZ2oTi3J72uLcnywO5PsxN6XvI6cl5UAn+ZtCP2lQm3/2ngb1/53998f6v0Xmuv8kgrVv6/3Oylj9svMynbH8/A1f9v4tvZ2tM+njdfSjq+Nb8fX/szy/aPhKz+u6+dr/NB25+f+pXubvOQvrfPNVV7y4yveRUcFfUPl1LJsPfhMjva5H4aNhTn2z3vy+9cnZX+X/p9Lp0z/+/ItBX2kzQdN/z72X2ia+SURsv9u5tccmJEf652/Y77mbxvf1t6O9vm08Vra8bXxcXzszzzcP3GMOShzd3bkp/MHI21+8uO+fj7GDy2rBX1DN/OX1vmm4SM/ce/Pn36fY358xbvouKA3lWlZrq3Jp+/2RdqKhILePd5G038e9p9mfkmE7L+b+R366Aep35mT4Q6LKl/zt41va29H+3zaeC3t+Nr4ON3cn1kVV5D4yI9m/XyMH1qWC/pu5i+t803DR37i3p8XuebHV7yLzgt6MyDvXa/J2udj0tfFB+02Cnr3eBtN/3nYf5r5JRGy/27l15ghmb1Vl1vnhyNtNj7mbxvf1h5H+3zaeC3t+Nr4ON3an1lmTFXO3dyRlbP7X9Kmz49m/XyMH1qmC/ou5S/N803DR37i3p/nv8c9Pz7iXXVc0DcMTC3J5sN5GVP82CPrKOjd4220/Tf2Xy3D+087P5vQ/Xcjv+b1WbldvyWzr3c+ho/528a3tcfRPp82Xks7vjbephv7M8uMKcmp5bosnypF2hq0+dGun3b80LJc0Dd0I39pnm9a2vzY3p/m71Hkx0e8K6eC3vRPytLjdZk/1j4heUdB7x5vo+2/uf82s7v/tPOzCd5/F/I7Mndb6j/OypDDn7L4mL9tfFt7HO3zaeO1tONr4226sT/zTJsf7fppxw8t6wV9N/KX5vmmlfX8+Ih35VbQP13QycUNWf9iXEpdfuBuoaB3j7fR9r+7/x5ndv9p52fTjf5D5teYYbnwc11Wzg1F2pLQzt82vq3dRv98ungt7fjaeJvQ+zPvXsxP2SE/2vXL+vrszi/DBX3g/KV9vmlp97eNPj+6eA2ngr6hfHxBHj9akPGy/4RmAQW9e7yNj/6zvP98zC9O6P4bQubXHLogq/WbMvOaW9/a+dvGt7XbqJ9PGa+lHV8bn0TI/VkEe/np7zw/PtYvy+uzO7/sFvQNIfOX9vnmg2Z/26jzo4zXcC7oTWlCFjY2ZGGiHGnzoTw4JNVX3P8lsxYFvXu8jY/+s7z/fMwvTuj+G0Lm941/rMr29zNSdfzTFe38bePb2m20z6eN19KOr41PIuT+bNC8/z5ox3+en/5Im42P9cvy+uShoA+Zv7TPNx80+9tGmx9tvIZ7Qf90USe+XJeNxRPefyxkqqfl29qObN+9LIc9950UBb17vI2P/oPvv033/edjfnFC998QKr/GvCEf/7otN/7u/pVBzfxt49vak9A8X5J4Y16T8Y8uyaUzR6XicW2esY1vo41PItT+bEj7/vExviY/PtZPM76NNj+788t4QR8of1k433zIan608VrOBX1DefyqrG8uyQnPP/Yw+07IVw9q8mjlnBzyuFidoKB3j7fx1X9W95+v+bUTuv9nQuTXHP5Y7j65IWdede9TM3/b+Lb2JDTPlyTe7PubfFffkV8u/jXS5oNtfBttfFIh9meD9v3X8jV+Iz8PHfLja/2yuj6788t2Qd8QIn9ZON98cd3fcbT50cZrqQp6UxqTqw9rsjQ1EGkLwbxzRe5vbclWxHWZLpn49rKlvbS7AKZyWv7V/LVtqVPQR9p88NV/6/6zra+tvbV/V77m107o/p95WX7vRXLXWX7funxXnnx3RgYdLuK951LM3za+rT0JzfMlie+b+FI267dldtj9GePYxrfRxifVuj9Ds+1vbXvreFrN/PzeeX58rV/r+tjmbztfWvt3lZeC3iV/ce2NPt66/J/UzzdfXPd3HO35r43X0hX0pk/G5tekdm1aBrowAdNflZHRURmNGGr+6Fnb3hyjb58M7f36QRn0eJA8E/qF0Pavjbfx1X/r/rOtr629tX9XvubXTuj+n/GdX2OOyJX72/LvDwYjY3XCdf628W3tSbk+X9L4w41LY+Ws7Pe4Z19kG99GG59U6/5sbffNur+V7a3jae3m50HH+fG1fq3rY5u/rb21f1e5Keg95695vt1L/3zzxXV/t6M9/7XxPqgKegBA9xjzqsx8v5XqpQEAyB4KegAAACDHKOgBAACAHKOgBwAAAHKMgh4AAADIMQp6AAAAIMco6AEAAIAco6AHAAAAcoyCPofM1OLzr8CtXpQ3PXxUIannX9JtuCXnA32tspelub5FZsyITM5dkMkAX39OS3XsrFz84O2OPqzS2F9/pLS/OD9QdI0PHh05eUk+HKtG2vLG5XyxaXe/5fF8DpEfjSAFfXVqSX5fW5Tjg9mYZNGY6Wuyc29eJhpfgRuuSrmLm2nvS7pHzsvKzqrM5ejly4s017fIjKnI6W+25LdPjkba8siY/TKzst3xFxsb+6ue0v7qxvnB/YM0ZeVLqlqu54tNu/stb+dzqPxoBCnoGwtz7J/35PevTwb7PHmWGXNQ5u7syE/nD0bafGi+EL9cktEUc2v635dvA13IofOnFfr5srC+aWrm93Y9SH4rp5Zl68FncrSvfW5Dr68v5sCM/Fjv/B1sFvQp76+w5wf3T8j9G7p/rZDnRxKFKegdzxebuPstyfmcFaHyoxGkoO91oQ+8uBeiW8JeyGHzpxX6+bKwvmkKeSGbyrQs19bk03f7Im17vyfw+voy9NEPUr8zJ8Md7pOiF/S9LvT+Dd2/VsjzI4miFPSu54tN3P2W5HzOilD50aCgD8CYqpy7uSMrZ/dH2nyIeyG6JeSFHDp/WqGfL/T6msEp+d+1Ldn8YTbYGBq7+a0Hya8xA/Le9ZqsfT4mfW3mrl3fbuTXmCGZvVWXW+eHI202FPTFpt2/NqH71wp5fiShLei7cX7YaM4Xm7j7Lcn5bGMqE/LFbzVZvzEjI4592ITMjwYFfQDGlOTUcl2WT5UibT7EvRDdEvJCDp0/refPV460+RB6fc2BD2XlyY7Uf/tE3g40hsZufreDrf/A1JLUHs7LWJsf62r3Xzfya16fldv1WzL7euf9U9AXm3b/2oTuXyv0+WGjLui7cH7YaM4XG9v9ZjufbUz16dlS25Htu5flSJsxtELmR4OCPodsL0Q3cCGH0431HagOy9C+dC68tJn+SVnaXJf5Y+HmHzq/I3O3pf7jrAw57BEKeiAcbUHfEPr8sNGcLza2++35+ez+B2blwSGpVsL9tZ2Q+dGgoM8h2wvRDVzI4WRhfYusceFOLj6W9S/GpdRhjs2Ri/Lr3n928anaovx3h31oGTMsF36uy8q5oUhbEhT0QDg+Cvo0ac8XG9v9pjmfuyF0fjQo6HPI9kJ0AxdyOFlY36IrH1+Qx48WZLzcWY5Nf1WGG/+5tT0HpNLldTKHLshq/abMvOY2LgU9EE7uC3rl+WKT5H5rnM8bDudzN4TOj0ZqBX3zRyKvhPuRSGhpPn+SFyK0tC/k0PkP3X+cLKxv6PmH7t/GlCZkYWNDFibcf6ybljf+sSrb389I1XF/UNCnv/+0sv78oZ8vdP8a3SjoQ85fe77YJLnfmufzI/fzua+/Iq8E+j8DofOjkUpBb6qn9/7RwuEMJsUm7edP8kKEluaFHDr/zf43w/VvE3p9Gz8ynFleldWlM/LaS8boSn4D9p9E49Kd+HJdNhZPeP+wki2/Gsa8IR//ui03/u7+FcpeL+izsP80sv78oZ8vdP9a2oLedn6EnL+P88Umyf22ez4/bJ7Pnf61GzM4LV9v1GVr9YL8tcNYm27kRyOdgn7fCfnqQU0erZyTQ54T3g1pP3+SFyK0VC/kwPkP3b9N6PU1lacXwvaO/Nzmwgk9/9D9J1Uevyrrm0tyYsDvM9jyq2EOfyx3n9yQM6+6P3PPF/QZ2X+usv78oZ8vdP9a6oLecn6EnL+P88Um6f22dz73x/++Vmbf8VznRyOVgr4d884Vuf/iPzjbc12mS0bXXra02+JL2VnA5gtR35YnjWdbvShv/v+mtT2/tr05xtPD5l/NX9uWekoXcju257e1t/aXFqf1TbC/9/o/9k95tHNHzg93NudG//cifT/vP+n4WWFKY3L1YU2WpgYibRrt8mvLjy2/jT7euvwfefLdGRlUXFTNgr7T/ZVgfW3tzTEyfn7E5d82P2176/MUjW3+tvy39pdV6oK+zflh4yO/Ps4Xm3b3W6vW89k2v9b4EN66fDd4fjSyVdD3V2XkT//g7Jmh5j88S7u99XnTYioHnj/bcHXvrwzYnl/b3hyjb58M7f36QRns0ouUhO35be2t/aUl1Po+6//Q//ws9Z86/09u2fq3tbf2lzZj+mRsfk1q16ZlwOPztcuvLT/WdnNErtzbln9/MBgZsxOh9petvTlGgc8PbXvr8xSNbf629tb+skpb0Lc7P2xs+bO2ezpfbNqdP61az2fb87fG+9bMz/3w+dHIVEEPIKzGl/je/2ZLVmYORNqgR34BuOL8gAYFPQAAAJBjFPQAAABAjlHQAwAAADlGQQ8AAADkGAU9AAAAkGMU9AAAAECOUdADAAAAOUZBDwAAkICZWnz+ldKYL526anxQ6cjJS/LhWDXSVmTGjMjk3AWZzNDXo/OGgh4AACABM31Ndu7Ny4TlS6eutF+azStjKnL6my357ZOjkTYkQ0EPAACQQLOg/+WSjHou5J/p1YK+oXJqWbYefCZH+8LkNgljDsrcnR356fzBSFvWUdADAAAkQEEfjqlMy3JtTT59ty/S1i0U9AAAAAVHQR+OMQPy3vWarH0+Jn2B8mtjTFXO3dyRlbP7I21ZR0EPAACQAAV9WANTS1J7OC9jKf21G2NKcmq5LsunSpG2rKOgBwAASICCPizTPylLm+syf6wcaUM8CnoAAIAEKOjDasx/cvGxrH8xLqVAOS4qCnoAAIAEKOjDKx9fkI1HCzJeDpPjoqKgBwAASICCPjxTmpCFRxuyMBHmr92UB4ek+kp6/yWdUCjoAQAAEqCgD6+Rg4kvH8rG4gnvf+3GVE/Lt7Ud2b57WQ577jttFPQAAAAJUNB3R3n8qqxvLsmJfr95NvtOyFcPavJo5ZwcCrSGaaGgBwAASKBZ0Ne35cnWlmytXpQ3/78oNO9ckfuNX4u4LtMlY23f65+CvsmUxuTqw5osTQ3s/m9L/mztrf0XEQU9AABAAqZyQEZHR3cNV6X8rKDvr8rIs1//kyGpPP09tva9/inom4zpk7H5Naldm5aBBPmztbf2X0QU9AAAAECOUdADAAAAOUZBDwAAAOQYBT0AAACQYxT0AAAAQI79H/fW7Ji7IfmyAAAAAElFTkSuQmCC";


    //Helper methods for comparison testing.
    BufferedImage createImageFromFile(String path) {
        //USe this created buffered image, creation of the image shouldn't matter, as long as generation is consistent.
        BufferedImage in = null;
        try {
            in = ImageIO.read(new FileInputStream(path));
            BufferedImage newImage = new BufferedImage(in.getWidth(), in.getHeight(), BufferedImage.TYPE_INT_ARGB);

            Graphics2D g = newImage.createGraphics();
            g.drawImage(in, 0, 0, null);
            g.dispose();
            return in;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    //Solution is From: https://stackoverflow.com/questions/11006394/is-there-a-simple-way-to-compare-bufferedimage-instances

    //Compare images byte by byte.
    public static boolean compareImages(BufferedImage imgA, BufferedImage imgB) {
        // The images must be the same size.
        if (imgA.getWidth() != imgB.getWidth() || imgA.getHeight() != imgB.getHeight()) {
            return false;
        }

        int width  = imgA.getWidth();
        int height = imgA.getHeight();

        // Loop over every pixel.
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                // Compare the pixels for equality.
                if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
                    return false;
                }
            }
        }

        return true;
    }

    @Test
    void ParseImgTestConvertToString() {
        ParseImg parseImg = new ParseImg();
        assertEquals(globalcopyasciipng,parseImg.convertToString(createImageFromFile("src/test/java/io/globalcopy/client/conversion/testimages/GlobalCopyASCII.png")));
        assertEquals(base64png, parseImg.convertToString(createImageFromFile("src/test/java/io/globalcopy/client/conversion/testimages/Base64.png")));

    }

    @Test
    void ParseImgTestConvertToImage() {
        ParseImg parseImg = new ParseImg();
        BufferedImage img = parseImg.convertToImage(base64png); //decode test
        BufferedImage compare = createImageFromFile("src/test/java/io/globalcopy/client/conversion/testimages/Base64.png"); //original file
        BufferedImage wrong = createImageFromFile("src/test/java/io/globalcopy/client/conversion/testimages/GlobalCopyASCII.png");
        assertTrue(compareImages(img, compare));
        assertFalse(compareImages(img, wrong));
    }

    @Test
    void ParseImgTestCheckBase64() {
        //Base64 should always be consistent and fail on others.
        ParseImg parseImg = new ParseImg();
        try {
            parseImg.convertToImage("not real data");
            fail();
        } catch (IllegalArgumentException ignored) {

        }
    }

    /*
    iVBORw0KGgoAAAANSUhEUgAAAF8AAAAlCAYAAAAk9TVSAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAeBSURBVGhD7Zp/TBRHFMcfXTlSLmGRi1BbKnrGXuWHilpsbUDSUhI4Y/FHbEUtTU1J46Wp1GT5A9sEjFptTtNiopjQ2MDZmiiR+ittQzUS9SoqGM6qxB9XwVTOljM1kKwWX2dv5vb2jr3jPG02NvtJJszNzpvZ+c7Mmzcb4pAAOprwDPurowG6+Bqii68huvgaoouvIbr4GqKLryG6+BoSJP6F6skQFxcXNiXwaZA1zwa7Ou/CfWbzf2Loxi+w1TYPstJ4SJDHnQB80Vdwk9WJDg80z09m9vOhmZWOQLrh+ukSzNJtN4pkwOyaDhSZ3VOP2It7KyZhgupYSTIL2MWqjo6IHYIFOdneik3sSSjqbscsQOfDh9LEBJLohT5nA5Slk2bJundtWACfnqLVn2run4VN+dnwzrc3QAQDTFpihzaXBwZ94x6E6212KDPHs8qj42leBEVbrsAw+x0RaQb8yCufzDQRn5WGcLUWc9isWmoussKnFbJKq9iYOQtW/niblcdIfz3mc5I2ZqyqKqbtPvLKj8RkM0xg2QcPHrDcU8o1O1R+fZ1kOMhdfxh2FqfR8lggO6i6YA20D3NgEfbCFzNH3y2PFe3Ex4/swHtuO9gWS4dWGvAJgcM6LoGHtKxSqN7jgiFWV40h1x6oLs0Ck3GMbDvGaCK288C2/gfovKt+1Pvt0vgEZjcGjKYsKK07Cn1hooNT2+qhU/IP41fB1rUk2KDFMUAO2EVFsOXKMPDW3XBi82ziwKKA7QAf0bgdsWUpGn3bNBc3XmWFCqI5tPm8L1UOMOICarKRvLSqjT+ZhVBLYrcxD3mVuv7EpZdjSz+rLtOFgpk+H1d5jJXFQuCA5SwCdvijkCYr6z+824le/EEPOhuW4CSD1CCPhfWXSbcj6aqZgVMKVmNdqxPdHnJsMQY9TnTY/CJxWLhjgD1hnLThePbMssKBTtlWRG+fG52tdbi6YArOqAkWv58M0t9mepkd23q89L1EL/YcEDCPl54BGq1NGNTjAPHPPjtAa6MXzzsELMlMwUSfz5aSAZMmzsIKezv2Rgjr5P55IrJygmMWP2J6FmesOYjXA7o+AgO4o5Cj7RQ3Bk2e3HdGFXawslERD2I5E9dcpR76iscq2aTmYK1yp3YJaPaVkxXLsXcKkwzZNvxpxM4h9DdisVGqQw5Yeckz/hvxWeJM+FaY1R8J5e5SrmG53LgUW6JsVCQD9MXTEW0C7iV34++sjCCLQ8Q3TSMrvA27g3abExvK0uV43UgWS5D+YgcKFmnSyE4VVCY+ZvHD+XzietxOB9ryeNYwj2Xfh7gPH+Tlz7fiVuE9XFSQiampqZiakqi4eNA+lOLLZwlJPjHqWtHp9mCkDXaschxtK2QXhdJkpe0G+XZZHDOOOEZkRNLH+KB6VJV+Yk414Ik7U9sUT158P+JvuC6HDmiEiJfrsfQ5A+s4QgqxkwbUUh5YaYFEfe9iwYHdITPhFzXqRISSiUp8gsI95dcPMPGb0Ep+Bx2woUQhfmyhpmEqfLgsl+avn4Z2L82CZzsUZX0MR26T2I5LhCklAmz9rg1cbje43fTWSCaYVQ4lFRY6rsGf5x0gLJ4FE1MSSfQtcR/+dp+D/VuWQ87YHFh39gl9VXr5JcjwZe7BXf/7qzE9B6ay7K3em3QaGMNXtsArynBamVYeZrUOw0p/2fzgrzwxx/kTXnye5W6B9E4SF+zbyCWDZDgLCE4v9BzZDFXvvgFZGRmQkTEOEmm1CBggObccNu87Czf+GoR/fJ80WqGuYjqYpJm474INlXb5I1cyTxyVhLVJ2sGjp0MraH0J+bJ4By65IqkfQO1e8zjELP6F7kssNxVyptNc90Xptkh4U4D1s6O6ZkTGkAwvzFkAn+3ugl8/z6FlnSfhBM3B66/NpJkz7XCc5qJn7AIoYZv31P69EFb+4+1wxpcxwqxXM4HexFbAIbXJVSbiEylWIG6HliknnxCb+J5mqGmgQnOF80nzlPh46ijgjz7opbkQPNBz9R7LB9P8/hyoPtoX9lN1SkoyzRh5YDkYu7wcCqUu7zTCR9VnI3zmHgLXroXwyV7208cEWPVBvs+1DR+vg7U/Dyk9CsMD32xykL1B4N+GZcVM+ycFmRGZyAeuFH65sG2XDeeaWFzMWVBQnDgDOwrZgcljnnAAe7zsmXRBc9hwnvIgDjlw6eHJoWlaBdrbXChHfcTWpbgs8eUHgyKb/vp81iexnWtDh7MP/d2K3j7SL7k8TaL9Ks9bH3K4SOy5dCzbeZr1S8ba04Z1b5nk8ViDblBREHO0E00yZKNtxM0jEIKpJc40F8vyWXgYIv6+JaNHSIbsGpXoQvq8MBfJmaBqE0gGXLKPmSjpb8Hy9AiXrBjvM09efEMSpoYJ+2TEXjxSW4KZSX4xg6/pyt0VHOFJdwMHkkgHSaQTCDmlPjMLcHVDd8SYX+xtR/vqAsxMTQp8H+ISMYW973n/dlCDvHO7vQJnTwzYcokpmFlSiwdiu8pHJb7+v5oa8liflHUeD118DdHF1xBdfA3RxdcQXXwN0cXXEF18zQD4FzwRJp0j5tXKAAAAAElFTkSuQmCC
     */
}
