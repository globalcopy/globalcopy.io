package io.globalcopy.controllers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.ui.Model;

public class HelloControllerTest {

    @Test
    public void testController() {
        HelloController h = new HelloController();
        Model m = Mockito.mock(Model.class);
        Assertions.assertEquals(h.hello("somename", m), "hello");
        Mockito.verify(m, Mockito.times(1)).addAttribute("name", "somename");
    }
}
