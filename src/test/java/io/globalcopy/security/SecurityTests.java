package io.globalcopy.security;


import io.globalcopy.client.CopyEntry.CopyEntry;
import io.globalcopy.client.PropertyManager;
import org.junit.jupiter.api.Test;
import javax.crypto.spec.IvParameterSpec;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.*;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;



public class SecurityTests {

    //FIXME: Remove reliance on user.properties in Encryter. Seems to fail for seperate hmacs

    @Test
    public void TestEncryptionDecryptionWithEntriesTests() throws Exception{

        Properties appProps = new Properties();
        PropertyManager.setProperty(appProps);
        appProps.setProperty("key", Base64.getEncoder().encodeToString(Encryptor.generateAESKey("password").getEncoded()));
        appProps.setProperty("hmac", Base64.getEncoder().encodeToString(Encryptor.generateHMACKey("password").getEncoded()));
        IvParameterSpec spec = new IvParameterSpec(new byte[16]);
        Encryptor encryptor1 = new Encryptor(spec);


        CopyEntry entry1 =  new CopyEntry("Jermaine",OffsetDateTime.of(LocalDate.of(2023, 10, 4), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()),
                "Test 1");



        CopyEntry entry2 =  new CopyEntry("Vuk",OffsetDateTime.of(LocalDate.of(2023, 11, 9), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()),
                "Test 2");


        CopyEntry entry3 =  new CopyEntry("Vuk",OffsetDateTime.of(LocalDate.of(2023, 2, 2), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()),
                "Test 3");


        CopyEntry entry4 =  new CopyEntry("Vuk",OffsetDateTime.of(LocalDate.of(2023, 8, 16), LocalTime.of(0, 0, 0), OffsetDateTime.now().getOffset()),
                "Test 4");

        List<CopyEntry> rtn1 = new ArrayList<>();
        rtn1.add(entry2);
        rtn1.add(entry3);
        rtn1.add(entry4);
        rtn1.add(entry1);

        entry2.setData(encryptor1.encryptData(rtn1.get(0).getData()));
        entry3.setData(encryptor1.encryptData(rtn1.get(1).getData()));
        entry4.setData(encryptor1.encryptData(rtn1.get(2).getData()));

        assertNotEquals("Test 2", entry2.getData());
        assertNotEquals("Test 3", entry3.getData());
        assertNotEquals("Test 4", entry4.getData());

        entry2.setData(encryptor1.decryptData(entry2.getData()));
        entry3.setData(encryptor1.decryptData(entry3.getData()));
        entry4.setData(encryptor1.decryptData(entry4.getData()));

        assertEquals("Test 2", entry2.getData());
        assertEquals("Test 3", entry3.getData());
        assertEquals("Test 4", entry4.getData());


        Properties appProps2 = new Properties();
        PropertyManager.setProperty(appProps2);
        appProps2.setProperty("key", Base64.getEncoder().encodeToString(Encryptor.generateAESKey("phrase").getEncoded()));
        appProps2.setProperty("hmac", Base64.getEncoder().encodeToString(Encryptor.generateHMACKey("password").getEncoded()));
        IvParameterSpec spec1 = new IvParameterSpec(new byte[16]);
        Encryptor encryptor2 = new Encryptor(spec1);
        entry1.setData(encryptor2.encryptData(rtn1.get(3).getData()));

        assertNotEquals("Test 1", entry1.getData());

        entry1.setData(encryptor2.decryptData(entry1.getData()));

        assertEquals("Test 1", entry1.getData());

        try{
            encryptor1.decryptData(encryptor2.encryptData(entry1.getData()));
            fail("Wrong encryptor so HMACs should not match");
        }
        catch (Exception e){
            System.out.println("Exception caught");
        }

        try{
            encryptor2.decryptData(encryptor1.encryptData(entry3.getData()));
            fail("Wrong encryptor so HMACs should not match");
        }
        catch (Exception e){
            System.out.println("Exception caught");
        }

        try{
            encryptor2.decryptData(encryptor1.encryptData(entry4.getData()));
            fail("Wrong encryptor so HMACs should not match");
        }
        catch (Exception e){
            System.out.println("Exception caught");
        }

        try{
            encryptor2.decryptData(encryptor1.encryptData(entry2.getData()));
            fail("Wrong encryptor so HMACs should not match");
        }
        catch (Exception e){
            System.out.println("Exception caught");
        }









    }

    
}
