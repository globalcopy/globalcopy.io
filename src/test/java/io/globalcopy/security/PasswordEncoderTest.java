package io.globalcopy.security;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PasswordEncoderTest {
    @Test
    public void testEmptyPWFail() {
        GlobalCopyPasswordEncoder pwencoder = new GlobalCopyPasswordEncoder();
        String encodedpw = pwencoder.encode("password");
        assertNotEquals("", encodedpw);
    }
    @Test
    public void testPasswordEncoder() {
        GlobalCopyPasswordEncoder pwencoder = new GlobalCopyPasswordEncoder();
        String encodedpw = pwencoder.encode("password");
        assertNotEquals("password", encodedpw);
        assertTrue(pwencoder.matches("password", encodedpw));
    }
}
