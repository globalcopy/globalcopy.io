package io.globalcopy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class GlobalCopyApp {

    public static void main(String[] args) {
        SpringApplication.run(GlobalCopyApp.class, args);
    }

}
