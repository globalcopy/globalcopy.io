package io.globalcopy.security;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

    /**
     * The {@code GlobalCopyPasswordEncoder} class provides a password encoding and verification service
     * using the BCrypt hashing algorithm. This service implements Spring Security's {@link PasswordEncoder}
     * interface, making it suitable for integration with Spring Security authentication mechanisms.
     */
    @Service
    public class GlobalCopyPasswordEncoder implements PasswordEncoder {
        //Ref: https://dzone.com/articles/spring-security-and-custom-passwordnbspencoding

        /**
         * Encodes the provided raw password using BCrypt hashing algorithm.
         * The strength of the hash can be controlled by the 'log rounds' parameter which is set to 12.
         *
         * @param rawPassword The raw password to encode.
         * @return The BCrypt hash string of the raw password.
         */
        @Override
        public String encode(CharSequence rawPassword) {
            return BCrypt.hashpw(rawPassword.toString(), BCrypt.gensalt(12));
        }

        /**
         * Verifies if the provided raw password matches the encoded (hashed) password.
         * The comparison is performed using BCrypt's checkpw method.
         *
         * @param rawPassword    The raw password that was entered by a user.
         * @param encodedPassword The encoded password stored in the system.
         * @return true if the raw password matches the encoded password, false otherwise.
         */
        @Override
        public boolean matches(CharSequence rawPassword, String encodedPassword) {
            return BCrypt.checkpw(rawPassword.toString(), encodedPassword);
        }
    }
