package io.globalcopy.security;
import io.globalcopy.client.PropertyManager;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;


/**
 * The {@code Encryptor} class provides encryption and decryption functionality
 * using AES/CBC/PKCS5Padding for encryption and HmacSHA256 for message authentication.
 */
public class Encryptor {

    SecretKey key;
    SecretKey hmacKey;
    private final IvParameterSpec paramSpec;

    /**
     * Constructs an {@code Encryptor} with specified properties and initialization vector.
     *
     * @param spec      The IV parameter specification for AES encryption.
     */
    public Encryptor(IvParameterSpec spec){
        this.paramSpec = spec;
        cycleEncryption();

    }


    //Putting this code in the constructor means we have no way of updating encryption keys
    //used at runtime.
    public void cycleEncryption() {
        byte[] encodedKey = Base64.getDecoder().decode(PropertyManager.getKey());
        byte[] encodedHMAC = Base64.getDecoder().decode(PropertyManager.getHmac());
        key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        hmacKey = new SecretKeySpec(encodedHMAC, 0, encodedHMAC.length, "HmacSHA256");
    }


    /**
     * Encrypts the provided data string using AES encryption and appends an HMAC for integrity verification.
     *
     * @param data The string data to encrypt.
     * @return A base64 encoded string containing the encrypted data followed by its HMAC.
     * @throws Exception If there are issues initializing the cipher or generating the MAC.
     */

    public String encryptData(String data) throws Exception {
        //get secretKey from Properties.


        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
        byte[] encryptedBytes = cipher.doFinal(data.getBytes());

        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(hmacKey);
        byte[] hmac = mac.doFinal(encryptedBytes);
       // System.out.println("Hmac key 1: " + hmacKey);

        // Combine encrypted data and HMAC
        byte[] combined = new byte[encryptedBytes.length + hmac.length];
        System.arraycopy(encryptedBytes, 0, combined, 0, encryptedBytes.length);
        System.arraycopy(hmac, 0, combined, encryptedBytes.length, hmac.length);

        return Base64.getEncoder().encodeToString(combined);

    }
    /**
     * Generates a SecretKey for AES encryption based on a specified string.
     * This method uses SHA-256 to hash the string input to generate a 256-bit key hash, then truncates or pads
     * this hash to fit the key size requirement for AES.
     *
     * @param keyString The input string from which to generate the key.
     * @return A {@link SecretKey} instance suitable for AES encryption.
     * @throws Exception If there is an error during the key generation process, such as a NoSuchAlgorithmException.
     */
    public static SecretKey generateAESKey(String keyString) throws Exception {
        // Use SHA-256 to hash the string and get a 256-bit representation
        MessageDigest sha = MessageDigest.getInstance("SHA-256");
        byte[] key = sha.digest(keyString.getBytes(StandardCharsets.UTF_8));

        // AES supports key sizes of 128, 192, and 256 bits.
        // You might want to truncate or pad the key depending on your AES key size requirement.
        // For example, for 128-bit key, use only the first 16 bytes of the hash.
        byte[] keyBytes = Arrays.copyOf(key, 16); // for AES 128-bit key

        return new SecretKeySpec(keyBytes, "AES");
    }
    /**
     * Decrypts a base64 encoded string that contains encrypted data followed by an HMAC.
     * Verifies the HMAC before decrypting to ensure data integrity.
     *
     * @param encryptedData The base64 encoded string containing the encrypted data and its HMAC.
     * @return The decrypted string data.
     * @throws Exception If HMAC verification fails or there are issues initializing the cipher.
     */
    public String decryptData(String encryptedData) throws Exception {
      //  System.out.println("Hmac key 2: " + hmacKey);
        byte[] combined = Base64.getDecoder().decode(encryptedData);

        // Extract HMAC and encrypted data
        byte[] encryptedBytes = new byte[combined.length - 32]; // Assuming HMAC SHA-256 (32 bytes)
        byte[] hmac = new byte[32];
        System.arraycopy(combined, 0, encryptedBytes, 0, encryptedBytes.length);
        System.arraycopy(combined, encryptedBytes.length, hmac, 0, hmac.length);

        // Verify HMAC
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(hmacKey);
        byte[] expectedHmac = mac.doFinal(encryptedBytes);
        if (!java.util.Arrays.equals(expectedHmac, hmac)) {
           // System.out.println("Expected: " + Arrays.toString(expectedHmac) + " actual: " + Arrays.toString(hmac));
            throw new Exception("HMAC verification failed");
        }

        // Decrypt the data
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
        byte[] decryptedBytes = cipher.doFinal(encryptedBytes);

        return new String(decryptedBytes);
    }
    /**
     * Generates a {@link SecretKey} for HMAC-SHA256 based on a specified string.
     * The key string is converted to bytes and then used to create a new {@link SecretKeySpec}.
     *
     * @param key The string to be used as the basis for the HMAC key.
     * @return A {@link SecretKey} suitable for HMAC-SHA256.
     */
    public static SecretKey generateHMACKey(String key) {
        return new SecretKeySpec(key.getBytes(), "HmacSHA256");
    }

}

