package io.globalcopy.client;


import io.globalcopy.client.FrontEnd.CopyEntryViewer;
import io.globalcopy.client.CopyEntry.CopyEntry;
import io.globalcopy.client.CopyRetriever.CopyRetriever;

import java.util.ArrayList;

public class ClipboardSync extends Thread {

    Integer SYNC_REFRESH_TIME = 1000;

    RequestSender requester;
    CopyRetriever retriever;

    boolean syncEnabled = false;
    private CopyEntryViewer viewer;
    private String appPropsPath;


    public ClipboardSync(RequestSender requestor, CopyRetriever retriever) {
        this.retriever = retriever;
        this.requester = requestor;
        checkServerData();
    }

    private void checkServerData() {
        try {
            ArrayList<CopyEntry> lastSession = (ArrayList<CopyEntry>) this.requester.sendGetAllReq();
            if (!lastSession.isEmpty()) {
                this.retriever.setClipboard(lastSession);
            }
        } catch (Throwable e) {
            if (e.getMessage().contains("HMAC verification failed")) {
                //throw new RuntimeException(e);
                this.viewer = new CopyEntryViewer();
                this.viewer.setRequestorRetriever(requester, retriever);
                this.viewer.openHmacDialouge();
            } else {
                throw new RuntimeException(e);
            }

        }
    }

    public void initViewer(){
        this.viewer = new CopyEntryViewer();
        viewer.setRequestorRetriever(requester, retriever);
        viewer.createUI();
        viewer.setVisible(true);
    }


    public void checkSync() {


        // retriever will check the local clipboard
        // requester will check globalcopy.io
        // if they aren't the same, one of these values updated previously.
        // the most recent timestamp wins.


        //TODO: replace with head request check
        CopyEntry recentRequest = null;
        try {
            recentRequest = requester.sendGetReq();
        } catch (Exception e) {
            if (e.getMessage().contains("HMAC verification failed")) {
                if (this.viewer == null) {
                    this.viewer = new CopyEntryViewer();
                    this.viewer.setRequestorRetriever(requester, retriever);
                    viewer.openHmacDialouge();
                }
            }
        }
        if (recentRequest == null) {
            System.out.println("request failed, retrying");
            return;
        }

        if (!retriever.getLastentry().compareTo(recentRequest)) {
            if(retriever.getLastentry().getTimestamp().isBefore(recentRequest.timestamp)) {
                //globalcopy updated more recently
                retriever.addNewClipboardEntry(recentRequest);

            }
        } //if our entry data does not match our actual clipboard data
        if (retriever.fixDesync()) {
            //send a new post request
            try {
                requester.sendPostReq(retriever.getLastentry());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        if(viewer != null){
            viewer.updateTableData();
        }

    }


    //use checksync within a thread.
    @Override
    public void run() {
        while (syncEnabled) {
            try {
                checkSync();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            try { //do this every ----- milliseconds.

                //is this "busy-waiting" bad?
                Thread.sleep(SYNC_REFRESH_TIME);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void toggleSync() {
        syncEnabled = !syncEnabled;
    }

    public CopyRetriever getRetriever() {
        return this.retriever;
    }



    public RequestSender getRequestor() {
        return requester;
    }
}
