package io.globalcopy.client.CopyRetriever;

import io.globalcopy.client.CopyEntry.CopyEntry;
import io.globalcopy.client.CopyEntry.CopyTag;
import io.globalcopy.client.CopyEntry.Seralizer;
import io.globalcopy.client.PropertyManager;
import io.globalcopy.client.conversion.ParseImg;
import io.globalcopy.client.conversion.TransferableImage;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WindowsLinuxRetriever implements CopyRetriever, NativeKeyListener {

    private ArrayList<CopyEntry> entryList;

    private String author;
    boolean ctrlPressed = false;
    boolean cPressed = false;

    int numSelection = 0; // 0 == no buffer object selected

    Clipboard clipboard = Toolkit.getDefaultToolkit()
            .getSystemClipboard();

    int ENTRY_LIMIT = 5; //no more than 5 entries

    ParseImg pi = new ParseImg(); //parsing tool for images

    public WindowsLinuxRetriever() {
         this.author = PropertyManager.getAuthor();

        //Create a new retiever and have the sync happen at startup.
        this.entryList = new ArrayList<>();
        CopyEntry value = null;
        //FIXME need to think about logic for image.
        try {
            Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
            if (t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                value = new CopyEntry(author, OffsetDateTime.now(), (String) clipboard.getData(DataFlavor.stringFlavor));
                value.setTag(CopyTag.STRING);
            }

            if (t.isDataFlavorSupported(DataFlavor.imageFlavor)) {
                value = new CopyEntry(author,OffsetDateTime.now(), (String) clipboard.getData(DataFlavor.stringFlavor));
                value.setTag(CopyTag.IMAGE);
            }

            this.entryList.add(value);
        } catch (UnsupportedFlavorException | IOException ignored) {
            //We're trying to get data that isn't a string. obviously will be fixed at a later date
        }

        if (value == null) { //We do not have a clipboard object to use, so give the default
            value = new CopyEntry(author, OffsetDateTime.now(), "Welcome To GlobalCopy");
            value.setTag(CopyTag.STRING);
            addNewClipboardEntry(value);
            this.entryList.add(value);
        }

        //JnativeHook setup
        //Disable logger
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);

        // Don't forget to disable the parent handlers.
        logger.setUseParentHandlers(false);
        try {
            GlobalScreen.registerNativeHook();
        }
        catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());

            System.exit(1);
        }

        GlobalScreen.addNativeKeyListener(this);
    }

    //Pass in a filepath with a preset list of CopyEntries
    public WindowsLinuxRetriever(String filepath) {

        //Have the sync happen at startup.
        Seralizer s = new Seralizer();
        try {

            //The expectation is that the next fixdesync call will update this clipboard data with what is currently here
            //Assuming, that there is a change since we last loaded this user's file.
            entryList = (ArrayList<CopyEntry>) s.deserialize(filepath);

        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ArrayList<CopyEntry> getClipboard() {
        return entryList;
    }

    @Override
    public void setClipboard(ArrayList<CopyEntry> data) {
        this.entryList = data;

        //also update our system clipboard with the most recent object
        setClipboardToEntry(this.entryList.get(this.entryList.size() - 1));
    }
    @Override
    public void setClipboardToEntry(CopyEntry data) {
        //Don't change clipboard directly, use this method to handle changes.
        //this will be populated with more primitives.
        Transferable selection = null;
        try {
            switch (data.tag) {
                case IMAGE -> selection = new TransferableImage(pi.convertToImage(data.data));
                case STRING -> selection = new StringSelection(data.data);

            }
        } finally {
            clipboard.setContents(selection, (ClipboardOwner) selection);
        }

    }

    @Override
    public void addNewClipboardEntry(CopyEntry entry) {
        this.entryList.add(entry);
        if (this.entryList.size() > ENTRY_LIMIT) { //sixth entry
            this.entryList.remove(0); //oldest entry
        }
        setClipboardToEntry(entry);

    }

    @Override
    public CopyEntry getLastentry() {
        return this.entryList.get(entryList.size() - 1);
    }

    @Override
    public CopyEntry getCopyEntryAt(int index) {
        return this.entryList.get(index);
    }


    @Override
    public boolean fixDesync() {

        //Console print of copyentrylist
    //    System.out.println(Arrays.toString(entryList.toArray()));

        try {
            Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
            CopyEntry newEntry = new CopyEntry(author, OffsetDateTime.now(), null);
            if (t != null) {

                //for strings
                if (t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    //System.out.println("TransferData: " + (String) t.getTransferData(DataFlavor.stringFlavor));
                    newEntry.data = (String) clipboard.getData(DataFlavor.stringFlavor);
                    newEntry.tag = CopyTag.STRING;
                    return verifyData(newEntry);

                }
                //For images...
                if (t.isDataFlavorSupported(DataFlavor.imageFlavor)) {
                    newEntry.data = pi.convertToString((BufferedImage) clipboard.getData(DataFlavor.imageFlavor));
                    newEntry.tag = CopyTag.IMAGE;
                    return verifyData(newEntry);
                }

            }
        } catch (IOException | UnsupportedFlavorException ignored) {

        }
        return false;
    }

    @Override
    public boolean verifyData(CopyEntry entry) {
        //we should only be adding new copy data.
        if (!existsinEntryList(entry.data)) {

            addNewClipboardEntry(entry);

            return true;
        }
        return false;
    }

    @Override
    public boolean existsinEntryList(String dataCheck) {
        for (CopyEntry entry: this.entryList) {
            if (entry.data.equals(dataCheck)) {
                return true;
            }

        }
        return false;
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {
        //unused
        //System.out.println("Key Released: " + NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode()));
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
        //check if user is using copy/past shortcut.
        String keyName = NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode());
        if (keyName.equals("Ctrl")) {
            ctrlPressed = true;
        }

        if (keyName.equals("C")) {
            cPressed = true;
        }

        //queue selection from our entrylist
        //press Ctrl and a number to select the nth most recent selection to paste.
        //we shouldn't send updates if the data exists in our entryList
        if (ctrlPressed) {
            //we're using a Ctrl + Key shortcut.
            //if it's one of our binded numbers (1-5)
            try {
                int index = Integer.parseInt(keyName);
                if (index > 0 && ENTRY_LIMIT >= index) {
                    this.numSelection = this.entryList.size() - index; // 1 -> entryList.get(4), 2 -> entryList.get(3)..


                    CopyEntry entry = this.entryList.get(numSelection);
                    setClipboardToEntry(entry);

                }
            } catch (NumberFormatException ignored) {
            }

        }

        if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_ESCAPE) {
            try {

                GlobalScreen.unregisterNativeHook();
            } catch (NativeHookException nativeHookException) {
                nativeHookException.printStackTrace();
            }
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
        //disable our checks on release
        //System.out.println("Key Released: " + NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode()));
        if (NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode()).equals("Ctrl")) {
            ctrlPressed = false;
        }

        if (NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode()).equals("C")) {
            cPressed = false;
        }


    }

}

