package io.globalcopy.client.CopyRetriever;



import io.globalcopy.client.CopyEntry.CopyEntry;

import java.util.ArrayList;

public interface CopyRetriever {


    /**
     * Gets Local Clipboard data
     */
    ArrayList<CopyEntry> getClipboard();

    /**
     * Sets Local Clipboard to the value of data
     */
    void setClipboard(ArrayList<CopyEntry> entry);

    void setClipboardToEntry(CopyEntry data);

    void addNewClipboardEntry(CopyEntry entry);

    /**
     * Retrieves the timestamp of our internal CopyEntry
     */

    CopyEntry getLastentry();

    //Useful for debugging, retrieves timestamp of specified slot.
    CopyEntry getCopyEntryAt(int index);

    /**
     * Do an internal comparison between our internal CopyEntry and the OS clipboard.
     * If there is a difference, update the CopyEntry to match, and return true.
     */
    boolean fixDesync();

    boolean verifyData(CopyEntry entry);

    boolean existsinEntryList(String dataCheck);

}
