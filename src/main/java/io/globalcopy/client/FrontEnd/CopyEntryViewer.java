package io.globalcopy.client.FrontEnd;


import io.globalcopy.client.CopyEntry.CopyEntry;
import io.globalcopy.client.CopyRetriever.CopyRetriever;
import io.globalcopy.client.PropertyManager;
import io.globalcopy.client.RequestSender;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 * The {@code CopyEntryViewer} class extends {@link JFrame} and provides a graphical user interface
 * to display entries retrieved from a {@link CopyRetriever}. It features a table for showing
 * clipboard entries.
 */
public class CopyEntryViewer extends JFrame {
    private JTextField authorField;

    private RequestSender requestor;
    private CopyRetriever retriever;
    private JButton searchButton;
    private JTable resultsTable;
    private DefaultTableModel tableModel;
    private TrayIcon trayIcon;
    private SystemTray tray;

    public void setRequestorRetriever(RequestSender requestSender, CopyRetriever copyRetriever){
        //We're going to let the circular dependcy slide since ClipboardSync is
        //a reasonable point of entry for viewer; We are using both the requestor and
        //sender for the UI.
        this.requestor = requestSender;
        this.retriever = copyRetriever;
    }

    /**
     * Initializes and sets up the user interface components of the window.
     * This method configures the layout, adds a text field and initializes
     * a table to display the clipboard data.
     */
    public void createUI() {
        setTitle("Copy Entry Viewer");
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(Exception ignored){
        }
        if(SystemTray.isSupported()) {
            tray=SystemTray.getSystemTray();
            Image image= null;
            try {
                image = ImageIO.read(this.getClass().getResource("/globe-clipboard.png"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ActionListener exitListener=new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            };
            PopupMenu popup=new PopupMenu();
            MenuItem defaultItem=new MenuItem("Exit");
            defaultItem.addActionListener(exitListener);
            popup.add(defaultItem);
            defaultItem=new MenuItem("Open");
            defaultItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(true);
                    setExtendedState(JFrame.NORMAL);
                }
            });
            popup.add(defaultItem);
            trayIcon=new TrayIcon(image, "Global Copy", popup);
            trayIcon.setImageAutoSize(true);
        }
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addWindowStateListener(new WindowStateListener() {
            public void windowStateChanged(WindowEvent e) {
                if(e.getNewState()==ICONIFIED){
                    try {
                        tray.add(trayIcon);
                        setVisible(false);
                    } catch (AWTException ignored) {
                    }
                }
                if(e.getNewState()==7){
                    try{
                        tray.add(trayIcon);
                        setVisible(false);
                    }catch(AWTException ignored){
                    }
                }
                if(e.getNewState()==MAXIMIZED_BOTH){
                    tray.remove(trayIcon);
                    setVisible(true);
                }
                if(e.getNewState()==NORMAL){
                    tray.remove(trayIcon);
                    setVisible(true);
                }
            }
        });
        try {
            setIconImage(ImageIO.read(this.getClass().getResource("/globe-clipboard.png")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        setSize(600, 400);
        setLayout(new BorderLayout());

        // Author input field
        authorField = new JTextField();
        add(authorField, BorderLayout.NORTH);

        tableModel = new DefaultTableModel();
        tableModel.addColumn("Author");
        tableModel.addColumn("Data");
        tableModel.addColumn("Timestamp");
        for (CopyEntry entry:
                retriever.getClipboard()) {
            tableModel.addRow(new Object[] { entry.author, entry.data, entry.timestamp });
        }

        resultsTable = new JTable(tableModel);

        add(new JScrollPane(resultsTable), BorderLayout.CENTER);

        //Add reencrypt button. for panel view.
        JButton handleHmac = new JButton("Update Encryption");
        handleHmac.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                    openHmacDialouge();
            }
        });
        add(handleHmac,BorderLayout.SOUTH);

    }

    public void openHmacDialouge() {

        JFrame hmachFrame = new JFrame();

        //construct components
        JButton enterButton = new JButton ("Enter");
        JTextField textField = new JTextField (5);
        JLabel newKeyText = new JLabel ("Enter New Key");
        JLabel currentKeyTooltip = new JLabel ("Current Key:");
        JLabel hmacReplaceable = new JLabel (PropertyManager.getHmac());

        //and listener for submission on update

        enterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (!textField.getText().isEmpty()) {

                        //Here we have all of our data, conveniently decrypted after this call.
                    ArrayList<CopyEntry> fromServerDecrypted = null;
                    try {
                        fromServerDecrypted = requestor.sendGetAllReq();
                    } catch (Exception e) {
                        //..Unless we're currently trying to rectify an error, such as:
                        if (e.getMessage().contains("HMAC verification failed")) {
                            //Take a new hmac update, don't touch anything else.
                            PropertyManager.setHmacKey(textField.getText());
                            hmachFrame.dispose();
                        }
                        else {
                            throw new RuntimeException();
                        }
                        return;
                    }

                    //Assuming the server had things to begin with...
                        if (!fromServerDecrypted.isEmpty()) {
                            //We now locally have our data, let's clear out the server since we won't
                            //be able to access it again after this conversion.
                            try {
                                requestor.sendClearReq();
                            } catch (IOException | InterruptedException e) {
                                throw new RuntimeException(e);
                            }

                            //Set our new Hmac Key locally
                            PropertyManager.setHmacKey(textField.getText());

                            //Lastly, put the data we have back up on the server.
                           requestor.sendPostAllReq(fromServerDecrypted);
                        }

                       hmachFrame.dispose();


                }
            }
        });


        hmachFrame.setLayout(null);

        //add components
        hmachFrame.add(enterButton);
        hmachFrame.add(textField);
        hmachFrame.add(newKeyText);
        hmachFrame.add(currentKeyTooltip);
        hmachFrame.add(hmacReplaceable);

        //set component bounds (only needed by Absolute Positioning)
        enterButton.setBounds (150, 140, 105, 25);
        textField.setBounds (50, 140, 100, 25);
        newKeyText.setBounds (50, 105, 180, 25);
        currentKeyTooltip.setBounds (50, 75, 100, 25);
        hmacReplaceable.setBounds (150, 75, 100, 25);

        hmachFrame.setSize(300,270);
        hmachFrame.setLocation(this.getX() + (this.getWidth() / 4), this.getY() + (this.getHeight() / 4));
        hmachFrame.setVisible(true);
    }

    public void updateTableData() {
        tableModel.setRowCount(0); // Clear existing data
        for (CopyEntry entry : retriever.getClipboard()) {
            tableModel.addRow(new Object[] { entry.author, entry.data, entry.timestamp });
        }
        tableModel.fireTableDataChanged(); // Notify the table model
    }



    /**
     * The main method to launch the Copy Entry Viewer application.
     * It uses {@link SwingUtilities#invokeLater} to ensure the GUI is created and updated on the
     * Event Dispatch Thread.
     *
     * @param args Command-line arguments (not used).
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(CopyEntryViewer::new);
    }

}
