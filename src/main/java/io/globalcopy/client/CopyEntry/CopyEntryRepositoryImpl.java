//package io.globalcopy.client;
//
//import java.util.List;
//import java.util.Optional;
//import java.util.function.Function;
//import org.springframework.data.domain.Example;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.repository.query.FluentQuery;
//
//public class CopyEntryRepositoryImpl implements CopyEntryRepository {
//    @Override public void flush() {
//
//    }
//
//    @Override public <S extends CopyEntry> S saveAndFlush(final S entity) {
//        return null;
//    }
//
//    @Override public <S extends CopyEntry> List<S> saveAllAndFlush(final Iterable<S> entities) {
//        return null;
//    }
//
//    @Override public void deleteAllInBatch(final Iterable<CopyEntry> entities) {
//
//    }
//
//    @Override public void deleteAllByIdInBatch(final Iterable<Long> longs) {
//
//    }
//
//    @Override public void deleteAllInBatch() {
//
//    }
//
//    @Override public CopyEntry getOne(final Long aLong) {
//        return null;
//    }
//
//    @Override public CopyEntry getById(final Long aLong) {
//        return null;
//    }
//
//    @Override public CopyEntry getReferenceById(final Long aLong) {
//        return null;
//    }
//
//    @Override public <S extends CopyEntry> Optional<S> findOne(final Example<S> example) {
//        return Optional.empty();
//    }
//
//    @Override public <S extends CopyEntry> List<S> findAll(final Example<S> example) {
//        return null;
//    }
//
//    @Override public <S extends CopyEntry> List<S> findAll(final Example<S> example, final Sort sort) {
//        return null;
//    }
//
//    @Override public <S extends CopyEntry> Page<S> findAll(final Example<S> example, final Pageable pageable) {
//        return null;
//    }
//
//    @Override public <S extends CopyEntry> long count(final Example<S> example) {
//        return 0;
//    }
//
//    @Override public <S extends CopyEntry> boolean exists(final Example<S> example) {
//        return false;
//    }
//
//    @Override public <S extends CopyEntry, R> R findBy(final Example<S> example, final Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
//        return null;
//    }
//
//    @Override public <S extends CopyEntry> S save(final S entity) {
//        return null;
//    }
//
//    @Override public <S extends CopyEntry> List<S> saveAll(final Iterable<S> entities) {
//        return null;
//    }
//
//    @Override public Optional<CopyEntry> findById(final Long aLong) {
//        return Optional.empty();
//    }
//
//    @Override public boolean existsById(final Long aLong) {
//        return false;
//    }
//
//    @Override public List<CopyEntry> findAll() {
//        return null;
//    }
//
//    @Override public List<CopyEntry> findAllById(final Iterable<Long> longs) {
//        return null;
//    }
//
//    @Override public long count() {
//        return 0;
//    }
//
//    @Override public void deleteById(final Long aLong) {
//
//    }
//
//    @Override public void delete(final CopyEntry entity) {
//
//    }
//
//    @Override public void deleteAllById(final Iterable<? extends Long> longs) {
//
//    }
//
//    @Override public void deleteAll(final Iterable<? extends CopyEntry> entities) {
//
//    }
//
//    @Override public void deleteAll() {
//
//    }
//
//    @Override public List<CopyEntry> findAll(final Sort sort) {
//        return null;
//    }
//
//    @Override public Page<CopyEntry> findAll(final Pageable pageable) {
//        return null;
//    }
//}
