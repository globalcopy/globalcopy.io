package io.globalcopy.client.CopyEntry;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Provides support for JPA
 * @author Jermaine L Brown
 */
public interface CopyEntryRepository extends JpaRepository<CopyEntry, Long> {

    @Query(value = "SELECT * FROM copy_entry WHERE copy_entry.author=?", nativeQuery = true)
    public List<CopyEntry> findEntriesByUsername(String username);

}
