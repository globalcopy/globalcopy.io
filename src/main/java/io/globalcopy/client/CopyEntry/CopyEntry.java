package io.globalcopy.client.CopyEntry;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.OffsetDateTime;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Represents an individual entry for copy.
 * @author brookskm
 */


/*
These headers are used for the JPA implementation on the database.
 */
@Entity
@Table(name = "copyEntry")
@Data


/*
These Headers are used for lombok to remove boilderplate.
 */
@Builder
@IdClass(CopyEntryId.class)
@NoArgsConstructor
@AllArgsConstructor

public class CopyEntry implements Serializable {

    /** Represents the Timestamp of entry creation.
     */
    @Id
    public OffsetDateTime timestamp;

    @Column (name = "data", length = 104857600) // = 100 MB, so 500MB per user total on server (When server gets cleared.)
    public String data;
    @Id
    public String author;

    /** Represents the CopyTag associated with the CopyEntry data.
     * Used to ensure that the encrypted data is propertly handled
     * by recieving systems.
     */
    @Builder.Default
    public CopyTag tag = CopyTag.STRING;

    /** Generates the CopyEntry with specified data.
     * @param author the associated author of the timestamp
     * @param timestamp the time that this entry was created
     * @param data the data that is to be stored, as a string representation.
     */
    public CopyEntry (String author, OffsetDateTime timestamp, String data) {
        this.author = author;
        this.timestamp = timestamp;
        this.data = data;
    }

    /**
     * Makes a lazy compare on CopyEntry.data with another CopyEntry
     * @param otherEntry another copyEntry.
     * @return0 boolean
     */
    public boolean compareTo(CopyEntry otherEntry) {
        return this.data.equals(otherEntry.data);
    }

}

