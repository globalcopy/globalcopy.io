package io.globalcopy.client.CopyEntry;

import java.io.Serializable;
import java.time.OffsetDateTime;

/**
 * Used by JPA to establish @IdClass and entity composite keys
 * @author Jermaine L Brown
 * JavaDocs by brookskm
 */
public class CopyEntryId implements Serializable {
    private OffsetDateTime timestamp;
    private String author;

    public CopyEntryId() {}

    public CopyEntryId(OffsetDateTime timestamp, String author) {
        this.timestamp = timestamp;
        this.author = author;
    }
}