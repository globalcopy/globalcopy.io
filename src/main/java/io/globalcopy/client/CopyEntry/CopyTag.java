package io.globalcopy.client.CopyEntry;

/**
 * The currently suppoted file types of GlobalCopy
 * @author brooks
 */
public enum CopyTag {
    STRING,
    IMAGE
}
