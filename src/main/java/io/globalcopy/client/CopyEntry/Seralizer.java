package io.globalcopy.client.CopyEntry;

import java.io.*;
import java.util.List;

/**
 * Allows for CopyEntry to be serializeable
 * @author brookskm
 */

public class Seralizer {
    /*
    Note that we are doing serializtion by list of entries, rather than individual CopyEntries in system.
    TODO: Should we send our RESTFul Requests with these serializable files?
    Potential Answer: https://crashtest-security.com/java-serialization/
    Archive: https://web.archive.org/web/20230124001516/https://crashtest-security.com/java-serialization/
     */

    /**
     * Creates a serialized file of the entrylist
     * @param entryList List of CopyEntries
     * @param filepath location to place serialized file
     * @throws IOException
     */
    public void serialize(List<CopyEntry> entryList, String filepath) throws IOException {

        FileOutputStream fileOutputStream
                = new FileOutputStream(filepath);
        ObjectOutputStream objectOutputStream
                = new ObjectOutputStream(fileOutputStream);

        objectOutputStream.writeObject(entryList);
        objectOutputStream.flush();
        objectOutputStream.close();
    }


    /**
     *Generates EntryList from serialized file
     * @param filepath file to deserialize
     */
    public List<CopyEntry> deserialize(String filepath) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream
                = new FileInputStream(filepath);
        ObjectInputStream objectInputStream
                = new ObjectInputStream(fileInputStream);

        List<CopyEntry> newEntryList = (List<CopyEntry>) objectInputStream.readObject();
        objectInputStream.close();
        return newEntryList;
    }

}
