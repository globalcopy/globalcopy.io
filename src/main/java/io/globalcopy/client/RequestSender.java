package io.globalcopy.client;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.globalcopy.client.CopyEntry.CopyEntry;
import io.globalcopy.security.Encryptor;
import org.apache.hc.core5.net.URIBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.OffsetDateTime;
import java.util.*;

// @Service FIXME: Why is this marked as @Service? Causes error on Server-sided runs -K
public class RequestSender {
    private HttpClient client = HttpClient.newHttpClient();


    private Encryptor encryptor;
    private String token;
    private String author;
    private boolean validCredentials = true;
    private String link = "http://localhost:8080";
    private ObjectMapper mapper = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);



    //Is this only for mocks? - Khyree
    //Fixme get rid of this and update the test cases.
    public RequestSender(HttpClient cli, String token, Encryptor encryptor, String author) {
        client = cli;
        this.encryptor = encryptor;
        this.encryptor.cycleEncryption();
        this.token = token;
        this.author = author;
    }



    //Literally everything the RequestSender needs to function is in the user.properties file.
    public RequestSender() {
        this.author = PropertyManager.getAuthor();
        this.encryptor = new Encryptor(new IvParameterSpec(new byte[16]));
        this.link = PropertyManager.getLink();
    }

    //Check if the author exists,
    //If they do, get all of their entries, so we can update our current instance.
    //Otherwise, create them with one entry to get the ball rolling.
    public int sendCreateRequest() throws IOException, InterruptedException, URISyntaxException {
        HttpRequest req = HttpRequest.newBuilder()
                    .uri(new URIBuilder(URI.create(link + "/api/v0/signup"))
                            .addParameter("username", PropertyManager.getAuthor())
                            .addParameter("email", PropertyManager.getEmail())
                            .addParameter("pw1", PropertyManager.getPassword())
                            .addParameter("pw2", PropertyManager.getPassword())
                            .build())
                    .method("POST", HttpRequest.BodyPublishers.noBody())
                   .build();
           HttpResponse<String> res = client.send(req, HttpResponse.BodyHandlers.ofString());

            token = "Bearer " + res.headers().firstValue(HttpHeaders.AUTHORIZATION).orElse("");

        return res.statusCode();
    }

    public int sendGenerateReq() throws IOException, InterruptedException {
        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create(link + "/api/v0/generate")).GET().build();
        return client.send(req, HttpResponse.BodyHandlers.ofString()).statusCode();
    }


    public CopyEntry sendGetReq() throws Exception {
        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create(link + "/api/v0/history/1"))
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .header(HttpHeaders.AUTHORIZATION, token)
                .build();

        //Now decrypt it.
        HttpResponse<String> res = client.send(req, HttpResponse.BodyHandlers.ofString());
//        Optional<String> lastModifiedHeader = res.headers().firstValue("last-modified");
//        OffsetDateTime lastModified = mapper.readValue(
//                lastModifiedHeader.orElse(null),
//                new TypeReference<>() {});
        List<CopyEntry> temp =  mapper.readValue(res.body(), new TypeReference<>(){});
        CopyEntry entry = null;
        if (!temp.isEmpty()) {
            entry = temp.get(0); //This should be a list 1 according to our HTTP Request above.
            this.encryptor.cycleEncryption();
            entry.setData(encryptor.decryptData(entry.data));
            return entry;
        }
        return entry;
    }


    public ArrayList<CopyEntry> sendGetAllReq() throws IOException, InterruptedException {
        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create(link + "/api/v0/history"))
                .method("GET", HttpRequest.BodyPublishers.noBody())

                .header(HttpHeaders.AUTHORIZATION, token)
                .build();


        HttpResponse<String> res = client.send(req, HttpResponse.BodyHandlers.ofString());
        //If we haven't recieved anything, make the assumption we are stating from scratch.
        if (res.body().isEmpty()) {
            return new ArrayList<>();
        }
        List<CopyEntry> temp =  mapper.readValue(res.body(), new TypeReference<>(){});
        this.encryptor.cycleEncryption();
        for (CopyEntry entry: temp) {
            try {
                entry.data = encryptor.decryptData(entry.data);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return (ArrayList<CopyEntry>) temp;
    }

    public String sendHeadReq() throws IOException, InterruptedException {
        HttpRequest req = HttpRequest.newBuilder()
                                     .uri(URI.create(link + "/api/v0/hello"))
                                     .method("HEAD", HttpRequest.BodyPublishers.noBody())
                .header(HttpHeaders.AUTHORIZATION, token).build();
        HttpResponse<String> res = client.send(req, HttpResponse.BodyHandlers.ofString());
        Optional<String> lastModifiedHeader = res.headers().firstValue("last-modified");
        OffsetDateTime lastModified = mapper.readValue(
                lastModifiedHeader.orElse(null),
                new TypeReference<>() {});
        return lastModified.toString();
    }

    public boolean sendPostReq(CopyEntry entry) throws Exception {
        //Band-aid Solution: give a new entry to be supplied to the database, then encrypt this dummy entry.
        //Decyprion should be able to revert it back on GET.
        encryptor.cycleEncryption();
        CopyEntry toSend = CopyEntry.builder()
                .timestamp(entry.timestamp)
                .author(author)
                .data(encryptor.encryptData(entry.data))
                .tag(entry.tag)
                .build();
        //entry.timestamp, encryptor.encryptData(entry.data);
                String requestBody = mapper
                .writeValueAsString(toSend);


        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create(link+"/api/v0/post"))
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header(HttpHeaders.AUTHORIZATION, token)
                .build();


        HttpResponse<String> res = client.send(req, HttpResponse.BodyHandlers.ofString());

        return (res.statusCode() == HttpStatus.CREATED.value());
    }

    public int sendLoginRequest() throws IOException, InterruptedException, URISyntaxException {
        HttpRequest req = HttpRequest.newBuilder()
                .uri(new URIBuilder(URI.create(link+"/api/v0/login/process"))
                        .addParameter("username", PropertyManager.getAuthor())
                        .addParameter("password", PropertyManager.getPassword())
                        .addParameter("pw2", PropertyManager.getPassword())
                        .build())
                .POST(HttpRequest.BodyPublishers.noBody())
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
            //    .header(HttpHeaders.AUTHORIZATION, token)
                .build();
        HttpResponse<String> res = client.send(req, HttpResponse.BodyHandlers.ofString());
        token = "Bearer " + res.headers().firstValue(HttpHeaders.AUTHORIZATION).orElse("");
        return res.statusCode();
    }

    public void sendClearReq() throws IOException, InterruptedException {
        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create(link + "/api/v0/history/-1"))
                .method("DELETE", HttpRequest.BodyPublishers.noBody())
                .header(HttpHeaders.AUTHORIZATION, token)
                .build();

        HttpResponse<String> res = client.send(req, HttpResponse.BodyHandlers.ofString());
    }

    public void sendPostAllReq(ArrayList<CopyEntry> toServer) {
        //This is just a for loop on sendPostReq.
        //This doesn't seem right but all ears for suggestions -K
        for (CopyEntry entry : toServer) {
            try {
                sendPostReq(entry);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
