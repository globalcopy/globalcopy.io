package io.globalcopy.client.conversion;

import java.awt.datatransfer.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/*
This class is serving as a comparable implementation of Transferable StringSelection.
Assume that errors are releated to associated ParseImg
 */
public class TransferableImage implements Transferable, ClipboardOwner {
    private final BufferedImage img;
    public TransferableImage(BufferedImage newImg) {
        this.img = newImg;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[]{DataFlavor.imageFlavor};
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return (flavor.equals(DataFlavor.imageFlavor));
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return img;
    }

    @Override
    public void lostOwnership(Clipboard clipboard, Transferable contents) {

    }
}
