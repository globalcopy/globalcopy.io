package io.globalcopy.client.conversion;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;

public class ParseImg {
    BufferedImage subImage;
    public String convertToString(BufferedImage img) {
        //Grab System Clipboard
        this.subImage = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = subImage.createGraphics();
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, subImage.getWidth(), subImage.getHeight());
        g2d.drawImage(img, 0, 0, null);
        g2d.dispose();
        //convert the image to base64
        String imageString = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(this.subImage, "png", bos);
            byte[] imageBytes = bos.toByteArray();
            imageString = Base64.getEncoder().encodeToString(imageBytes);

            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }

    public BufferedImage convertToImage(String encodedString) {
        //now convert back to bufferredimage.
        String returnString = null;
        returnString = Arrays.toString(Base64.getDecoder().decode(encodedString));
        ByteArrayInputStream bis = new ByteArrayInputStream(Base64.getDecoder().decode(encodedString));

        //ImageIO
        try {
            return ImageIO.read(bis);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
