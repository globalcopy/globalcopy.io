package io.globalcopy.client;

import io.globalcopy.security.Encryptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Properties;

public class PropertyManager {

    static Properties appProps = new Properties();

    public static void generateNewPropertyFile(String filepath, String username, String link, String password, String email) {
        //generate a key for the user.

        try {
            appProps.setProperty("key", Base64.getEncoder().encodeToString(Encryptor.generateAESKey(password).getEncoded()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        appProps.setProperty("password", password);
        appProps.setProperty("hmac", Base64.getEncoder().encodeToString(Encryptor.generateHMACKey(password).getEncoded()));
        appProps.setProperty("email", email);
        //add info
        appProps.setProperty("author", username);
        appProps.setProperty("link", link);
        appProps.setProperty("currentFilePath", filepath);
        try {
            File file = new File(filepath);
            if (file.createNewFile()) {
                appProps.store(new FileOutputStream(filepath), "generated from ClientMain");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void loadPropertyFile(String path) {
        try {
            appProps.load(new FileInputStream(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void updateFile(){

        try {
            appProps.store(new FileOutputStream(appProps.getProperty("currentFilePath")), "generated from ClientMain");
        } catch (IOException e) {
            System.out.println("Failed regenerating properties file");
        }
    }

    /*
    Getters/Setters, necessary since we have to do File
    Update side effects.
     */
    public static void setFilePath(String path) {
        appProps.setProperty("currentFilePath", path);
        updateFile();
    }

    public static void setPassword(String password) {
        //It is the callee's responsibility to ensure this is properly formatted
        try {
            appProps.setProperty("password", password);
            updateFile();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

    public static void setHmacKey(String key) {
        //It is the callee's responsibility to ensure this is properly formatted
        appProps.setProperty("hmac", Base64.getEncoder().encodeToString(Encryptor.generateHMACKey(key).getEncoded()));
        updateFile();
    }

    public static void setUserName(String key) {
        appProps.setProperty("user", key);
        updateFile();

    }

    public static String getLink() {
        return appProps.getProperty("link");
    }

    public static String getHmac() {
        return appProps.getProperty("hmac");
    }

    public static String getAuthor() {
        return appProps.getProperty("author");
    }

    public static String getKey() {
        return appProps.getProperty("key");
    }

    public static String getPassword() {
        return appProps.getProperty("password");
    }

    public static void setProperty(Properties appProps) {
        PropertyManager.appProps = appProps;
    }


    public static String getEmail() {
        return appProps.getProperty("email");
    }

    public static void deletePropertyFile() {
        File file = new File(appProps.getProperty("currentFilePath"));
        file.delete();
    }
}
