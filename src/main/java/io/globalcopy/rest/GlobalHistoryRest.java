package io.globalcopy.rest;


import io.globalcopy.client.CopyEntry.CopyEntry;
import io.globalcopy.client.CopyEntry.CopyEntryRepository;

import java.sql.SQLException;

import java.util.*;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@RestController
@RequestMapping(value = "/api/v0")
public class GlobalHistoryRest {
    private Integer ENTRY_LIMIT = 5;
    @Autowired
    private  CopyEntryRepository copyEntryRepository;

    public GlobalHistoryRest(CopyEntryRepository copyEntryRepository) {
        this.copyEntryRepository = copyEntryRepository;
    }

    @GetMapping("/history")
    public ResponseEntity<List<CopyEntry>> historyAt(@RequestParam("Author") String author) {

        List<CopyEntry> allEntries = copyEntryRepository.findEntriesByUsername(author);

        HttpHeaders headers = new HttpHeaders();
        headers.add("last-modified", (!allEntries.isEmpty()) ? allEntries.get(0).timestamp.toString() : "N/A");
        return new ResponseEntity<>(allEntries, headers, HttpStatus.CREATED);
    }


    @GetMapping("/history/{num}")
    public ResponseEntity<List<CopyEntry>> historyAtSpecification(@PathVariable Long num, @RequestParam("Author") String author) { //Map is temporarily being sent to test functionality TODO: replace map with JSON from actual GET request

        List<CopyEntry> allEntries = this.historyAt(author).getBody();
        if(allEntries == null || num > allEntries.size()) {return new ResponseEntity<>(allEntries, HttpStatus.CREATED);}

        List<CopyEntry> history = new ArrayList<>();
        for(int i = 0; i < num; i++){
            history.add(allEntries.get(i));
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("last-modified",  (!history.isEmpty()) ? history.get(0).timestamp.toString(): "N/A");
        return new ResponseEntity<>(history, headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/history/{num}")
    public ResponseEntity<List<CopyEntry>> deleteHistoryAtSpecification(@RequestParam("Author") String author, @PathVariable Long num) { //Map is temporarily being sent to test functionality TODO: replace map with JSON from actual GET request
        List<CopyEntry> allEntries = this.historyAt(author).getBody();

        if(allEntries == null || num > allEntries.size()) {return new ResponseEntity<>(allEntries, HttpStatus.CREATED);}

        List<CopyEntry> history = new ArrayList<>();
        int entrySize = allEntries.size();
        //lazy way of "clear all," no delete all avaliable.
        if (num == -1) {
            num = (long) allEntries.size();
            System.out.println(num);
        }

        for(int i = 0; i < num ; i++){
            copyEntryRepository.delete(allEntries.get((entrySize - 1) - i));
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("last-modified",  (!history.isEmpty()) ? history.get(0).timestamp.toString(): "N/A");
        return new ResponseEntity<>(history, headers, HttpStatus.CREATED);
    }


    @GetMapping("/list")
    public ResponseEntity<List<CopyEntry>> getCopyEntries(@RequestParam("Author") String author) {
        //System.out.println(author);
            //no args
            //Select entries from Users....

        //FIXME do not get every entry...
        List<CopyEntry> allEntries = copyEntryRepository.findAll();
        HttpHeaders headers = new HttpHeaders();
        headers.add("last-modified",  (!allEntries.isEmpty()) ? allEntries.get(0).timestamp.toString(): "N/A");
        return new ResponseEntity<>(allEntries, headers, HttpStatus.CREATED);
    }


    @PostMapping(value = "/post",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CopyEntry> create(@RequestBody CopyEntry newEntry) throws SQLException {
        copyEntryRepository.save(newEntry);
        //Keep users at 5 entries only
        List<CopyEntry> entries = copyEntryRepository.findEntriesByUsername(newEntry.author);
        if(entries.size() > ENTRY_LIMIT) {
            copyEntryRepository.delete(entries.get(0));
        }
        return new ResponseEntity<>(newEntry, HttpStatus.CREATED);
    }
}
