package io.globalcopy.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class HelloWorldREST {
    @GetMapping("/api/v0/hello")
    String hello() {
        return "Hello World";
    }
}

