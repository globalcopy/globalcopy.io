package io.globalcopy.rest;

import io.globalcopy.authentication.config.dtos.JwtAuthenticationResponse;
import io.globalcopy.authentication.config.dtos.SignInRequest;
import io.globalcopy.authentication.config.dtos.SignUpRequest;
import io.globalcopy.authentication.config.managers.AuthenticationService;
import io.globalcopy.authentication.config.models.GlobalCopyUser;
import io.globalcopy.authentication.config.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v0")
@RequiredArgsConstructor
@Slf4j
public class LoginREST {
    private final UserRepository userRepo;
    private final AuthenticationService authService;

    @GetMapping("/login/generate")
    public ResponseEntity<Void> generateUser() {
        GlobalCopyUser user1 = new GlobalCopyUser();
        user1.setUsername("popo");
        user1.setPwhash("password1234");
        this.userRepo.save(user1);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(null, headers, HttpStatus.CREATED);
    }

    @PostMapping("/login/process")
    public ResponseEntity<JwtAuthenticationResponse> handleLogin(@RequestParam("username") String username, @RequestParam("password") String password) {
        SignInRequest srq = new SignInRequest(username, password);
        JwtAuthenticationResponse jwtres = authService.signin(srq);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, jwtres.getToken());
        return new ResponseEntity<>(jwtres, headers, HttpStatus.CREATED);
    }

    @PostMapping(value = "/signup")
    public ResponseEntity<JwtAuthenticationResponse> handleSignup(@RequestParam("username") String username,
                               @RequestParam("email") String email,
                               @RequestParam("pw1") String password1,
                               @RequestParam("pw2") String password2) {
        if(null == password1 || !password1.equals(password2)) {
            throw new IllegalArgumentException("Re-entered password does not match provided password");
        }
        try {
            SignUpRequest srq = new SignUpRequest(username,email, password1);
            JwtAuthenticationResponse jwres = authService.signup(srq);
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.AUTHORIZATION, jwres.getToken());
            return new ResponseEntity<>(jwres, headers, HttpStatus.CREATED);
        } catch (UsernameNotFoundException e) {
            return new ResponseEntity<>(null, null, HttpStatus.CONFLICT);
        }
    }
}
