package io.globalcopy.authentication.config.filters;

import io.globalcopy.authentication.config.managers.IGlobalCopyUserMngr;
import io.globalcopy.authentication.config.managers.JwtService;
import io.globalcopy.authentication.config.wrappers.HttpServletAddParamRequest;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;


/*
This pulls the JWT of the web request, which is distibuted to the rest of our system.
 */
@Component
@RequiredArgsConstructor
@Slf4j //helps to log events to server to check filter goes throuh, can be ommitted
public class JwtAuthenticationFilter extends OncePerRequestFilter { //runs once per reuqest to server.
    private final JwtService jwtService;
    @Qualifier("GlobalCopyUserMngr")
    private final IGlobalCopyUserMngr userService;


    @Override
    protected void doFilterInternal( HttpServletRequest req, //keeping notnulls for testing
                                     HttpServletResponse response,
                                     FilterChain filterChain)
            throws ServletException, IOException {
        // Note: Consider what endpoints need the author value
        HttpServletAddParamRequest paramRequest = new HttpServletAddParamRequest(req);
        final String authHeader = paramRequest.getHeader(HttpHeaders.AUTHORIZATION);
        final String jwt;
        final String username; //the username for our case.
        if (StringUtils.isEmpty(authHeader)  || !StringUtils.startsWithIgnoreCase(authHeader, "Bearer ")) {
            filterChain.doFilter(paramRequest, response);
            return;
        }

        jwt = authHeader.substring(7);
        username = jwtService.extractUserName(jwt);
        if (!StringUtils.isEmpty(authHeader)  && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = userService.loadUserByUsername(username);
            if(null == userDetails) {
                return;
            }
            log.info("User - {}", userDetails);
            //create context for this user.
            SecurityContext context = SecurityContextHolder.createEmptyContext();

            //token creation, but ot as important.

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    userDetails, null, userDetails.getAuthorities());
            authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(paramRequest));
            context.setAuthentication(authToken);
            SecurityContextHolder.setContext(context);
            //System.out.println("User Details Username: "+ userDetails.getUsername());
            paramRequest.setParameter("Author", userDetails.getUsername());
            //System.out.println("Author: "+ paramRequest.getParameterMap().get((String)"Author")[0]);
        } else {
            return;
        }
        filterChain.doFilter(paramRequest, response);
    }
    @Override
    protected boolean shouldNotFilterAsyncDispatch() {
        return false;
    }

    @Override
    protected boolean shouldNotFilterErrorDispatch() {
        return false;
    }
}
