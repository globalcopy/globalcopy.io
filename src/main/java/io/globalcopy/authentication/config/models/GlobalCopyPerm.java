package io.globalcopy.authentication.config.models;

import org.springframework.security.core.GrantedAuthority;

public enum GlobalCopyPerm {
    USER("User");
    public final String auth;
    private record GlobalCopyAuthority(String authType) implements GrantedAuthority {
        @Override public String getAuthority() {
            return authType;
        }
    }

    private GlobalCopyPerm(String val) {
        this.auth = val;
    }

    public String getAuth() {
        return this.auth;
    }

    public GrantedAuthority getGrantedAuthority() {
        return new GlobalCopyAuthority(this.auth);
    }
}
