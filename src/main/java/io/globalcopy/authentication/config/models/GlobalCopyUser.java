package io.globalcopy.authentication.config.models;

import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;

@Entity
//@Transactional
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "global_copy_user")
public class GlobalCopyUser implements UserDetails {
    @Id
    @GeneratedValue
    private Long id;

    @Basic
    @Column(unique = true)
    private String username;

    @Getter
    @Basic
    @Column(unique = true)
    private String email;

    @Basic
    private String pwhash;

    @Enumerated(EnumType.STRING)
    GlobalCopyPerm perm;

    @ElementCollection(targetClass = GlobalCopyPerm.class)
    @Enumerated
    private Set<GlobalCopyPerm> grantedAuthorities;

    @Basic
    private boolean isEnabled;

    @CreationTimestamp
    LocalDateTime dateCreated;

    @CreationTimestamp
    LocalDateTime lastUpdated;

    @Getter
    @Setter
    String pwRecoveryToken;

    @Getter
    @Setter
    LocalDateTime pwRecoveryTokenExpiration;

//    public GlobalCopyUser() {
//        this.isEnabled = true;
//        this.grantedAuthorities = new HashSet<>();
//    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities.stream().map(GlobalCopyPerm::getGrantedAuthority).toList();
    }

    public void addAuthority(GlobalCopyPerm perm) {
        grantedAuthorities.add(perm);
    }

    public void removeAuthority(GlobalCopyPerm perm) {
        grantedAuthorities.remove(perm);
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public String getPassword() {
        return this.pwhash;
    }


// public String getEmail() { return this.email; }

    public void setUsername(final String username) { this.username = username; }
    public void setEmail(final String username) { this.email = email; }

    public void setPwhash(final String pwhash) { this.pwhash = pwhash; }

    // public void setEmail(final String email) { this.email = email; }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }
}
