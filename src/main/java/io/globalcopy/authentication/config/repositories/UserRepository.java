package io.globalcopy.authentication.config.repositories;

import io.globalcopy.authentication.config.models.GlobalCopyUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<GlobalCopyUser, Long> {
    Optional<GlobalCopyUser> findByUsername(String username);
    Optional<GlobalCopyUser> findByEmail(String email);
    Optional<GlobalCopyUser> findBypwRecoveryToken(String pwRecoveryToken);
}
