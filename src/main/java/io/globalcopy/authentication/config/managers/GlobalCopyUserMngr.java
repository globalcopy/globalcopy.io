package io.globalcopy.authentication.config.managers;

import io.globalcopy.authentication.config.models.GlobalCopyUser;
import io.globalcopy.authentication.config.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Component("GlobalCopyUserMngr")
public class GlobalCopyUserMngr implements IGlobalCopyUserMngr{
    private final UserRepository userRepository;

    @Autowired
    public GlobalCopyUserMngr(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public GlobalCopyUser createUser(GlobalCopyUser userToAdd) {
        return userRepository.save(userToAdd);
    }

    @Override
    public GlobalCopyUser loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<GlobalCopyUser> requestedUser = userRepository.findByUsername(username);
        if(!requestedUser.isPresent()) {
            throw new UsernameNotFoundException("The given username doesn't exist.");
        }
        return requestedUser.get();
    }
}
