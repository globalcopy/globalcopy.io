package io.globalcopy.authentication.config.managers;

import io.globalcopy.authentication.config.models.GlobalCopyUser;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public interface IGlobalCopyUserMngr extends UserDetailsService {
    GlobalCopyUser createUser(GlobalCopyUser userToAdd);
    @Override
    GlobalCopyUser loadUserByUsername(String username) throws UsernameNotFoundException;
}
