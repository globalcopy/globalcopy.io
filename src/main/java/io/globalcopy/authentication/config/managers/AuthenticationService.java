package io.globalcopy.authentication.config.managers;

import io.globalcopy.authentication.config.dtos.JwtAuthenticationResponse;
import io.globalcopy.authentication.config.dtos.SignInRequest;
import io.globalcopy.authentication.config.dtos.SignUpRequest;
import io.globalcopy.authentication.config.models.GlobalCopyPerm;
import io.globalcopy.authentication.config.repositories.UserRepository;
import io.globalcopy.authentication.config.models.GlobalCopyUser;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Set;

/*
Authenticate the give username/assword
 */
@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;
    @Qualifier("GlobalCopyUserMngr")
    private final IGlobalCopyUserMngr userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    //user sign-up
    public JwtAuthenticationResponse signup(SignUpRequest request) {
        var user = GlobalCopyUser
                .builder()
                .username(request.getUsername())
                .email(request.getEmail())
                .pwhash(passwordEncoder.encode(request.getPassword()))
                .grantedAuthorities(Set.of(GlobalCopyPerm.USER))
                .isEnabled(true)
                .build();

        user = userService.createUser(user);
        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }

    //user sign-in, token generation, jwt check/response calls here.
    public JwtAuthenticationResponse signin(SignInRequest request) {
        GlobalCopyUser user;
        try {
            user = userService.loadUserByUsername(request.getUsername());
            var jwt = jwtService.generateToken(user);
            System.out.println(user.getUsername());
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
            return JwtAuthenticationResponse.builder().token(jwt).build();
        } catch (UsernameNotFoundException unfe) {
            throw(new IllegalArgumentException("Invalid email or password"));
        }
    }


}
