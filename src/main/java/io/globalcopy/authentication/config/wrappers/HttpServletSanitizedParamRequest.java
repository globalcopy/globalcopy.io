package io.globalcopy.authentication.config.wrappers;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class HttpServletSanitizedParamRequest extends HttpServletRequestWrapper {
    private final Map<String, String[]> sanitizedMap;
    public HttpServletSanitizedParamRequest(HttpServletRequest request) {
        super(request);
        sanitizedMap =
                request.getParameterMap().entrySet().stream()
                        .collect(Collectors.toMap(
                                Map.Entry::getKey,
                                stringEntry -> Arrays.stream(stringEntry.getValue())
                                        .map(StringEscapeUtils::escapeHtml4)
                                        .toArray(String[]::new)
                        ));
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return sanitizedMap;
    }

    @Override
    public String[] getParameterValues(String name) {
        return Optional.ofNullable(getParameterMap().get(name))
                .map(values -> Arrays.copyOf(values, values.length))
                .orElse(null);
    }

    @Override
    public String getParameter(String name) {
        return Optional.ofNullable(getParameterValues(name))
                .map(values -> values[0])
                .orElse(null);
    }
}
