package io.globalcopy.controllers;

import io.globalcopy.authentication.config.models.GlobalCopyUser;
import io.globalcopy.authentication.config.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/signup")
public class SignupController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/signup")
    public String signup() {
        return "signup";
    }

    @PostMapping
    public ModelAndView registerUser(@RequestParam("username") String username,
                                     @RequestParam("email") String email,
                                     @RequestParam("pw1") String password) {
        System.out.println("Entered register user");
        GlobalCopyUser newUser = new GlobalCopyUser();
        newUser.setUsername(username);
        newUser.setEmail(email);
        newUser.setPwhash(password); // Ideally, you'd hash the password first

        userRepository.save(newUser);

        return new ModelAndView("redirect:/login"); // Redirect to login page after signup
    }
}
