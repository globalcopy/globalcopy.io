package io.globalcopy.controllers;

import io.globalcopy.authentication.config.models.GlobalCopyUser;
import io.globalcopy.authentication.config.repositories.UserRepository;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

import org.springframework.mail.javamail.MimeMessageHelper;

import java.util.Optional;
import java.util.regex.Pattern;
import java.security.SecureRandom;
import java.util.Base64;


@Controller
@RequiredArgsConstructor
public class PasswordRecoveryController {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepo;
    String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";
    Pattern pat = Pattern.compile(emailRegex);
    @Autowired
    private JavaMailSender mailSender;

    private String domain ="stage.globalcopy.io";

    //TODO configure domain name

    @GetMapping("/password-recovery")
        public String about() {
            return "password-recovery";
        }

    @PostMapping("/api/v0/recover-password")
    public ModelAndView recoverPassword(@RequestParam("email") String email) {
        Optional<GlobalCopyUser> requestedUser = userRepo.findByEmail(email);
        if(pat.matcher(email).matches() && requestedUser.isPresent()){ //
            sendRecoveryEmail(requestedUser);
            return new ModelAndView("recover-email-sent");
        }
        else{
            return new ModelAndView("failed-recovery");
        }
    }
    @GetMapping("/change-password")
    public ModelAndView validateToken(@RequestParam String token) {
        Optional<GlobalCopyUser> user = userRepo.findBypwRecoveryToken(token);
        if(user.isPresent() && LocalDateTime.now().isBefore(user.get().getPwRecoveryTokenExpiration())){
            return new ModelAndView("change-password");
        }
        return new ModelAndView("failed-recovery");

    }


    @PostMapping("/api/v0/reset-password")
    public ModelAndView resetPassword(@RequestParam("newPassword") String newPassword,
                                      @RequestParam("confirmPassword") String confirmPassword,
                                      @RequestParam("token") String token) {
        System.out.println("entered api call");

        ModelAndView modelAndView = new ModelAndView();
        Optional<GlobalCopyUser> user = userRepo.findBypwRecoveryToken(token);

        System.out.println("User: "+ user);
        System.out.println("Token: "+ token);
        if (user.isEmpty() || !LocalDateTime.now().isBefore(user.get().getPwRecoveryTokenExpiration())) {
            modelAndView.addObject("message", "Invalid or expired token.");
            modelAndView.setViewName("expired-token");
            return modelAndView;
        }


        if (!newPassword.equals(confirmPassword)) {
            modelAndView.addObject("message", "Passwords do not match.");
            modelAndView.setViewName("change-password");
            return modelAndView;
        }

        user.get().setPwhash(passwordEncoder.encode(newPassword));
        user.get().setPwRecoveryToken(null);
        user.get().setPwRecoveryTokenExpiration(null);
        userRepo.save(user.get());
        modelAndView.addObject("message", "Password successfully updated.");
        modelAndView.setViewName("login");

        return modelAndView;
    }


    private String generateToken() {
        SecureRandom random = new SecureRandom();
        byte[] values = new byte[24]; // 192 bits
        random.nextBytes(values);
        return Base64.getUrlEncoder().withoutPadding().encodeToString(values);
    }

    private void sendRecoveryEmail(Optional<GlobalCopyUser> user) {
        String token = this.generateToken();

        String resetLink = domain + "/change-password?token=" + token;
        user.get().setPwRecoveryToken(token);
        user.get().setPwRecoveryTokenExpiration(LocalDateTime.now().plusMinutes(10));
        userRepo.save(user.get());
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);



        try {
            helper.setTo(user.get().getEmail());
            helper.setSubject("Password Recovery");
            helper.setText("Click the link to reset your password: "+ resetLink);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        mailSender.send(message);
    }


}


