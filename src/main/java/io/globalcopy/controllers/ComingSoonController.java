package io.globalcopy.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
class ComingSoonController {
    @GetMapping("/")
    public String hello() {
        return "index";
    }
}
