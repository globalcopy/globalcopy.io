#!/usr/bin/env bash

# This script is run by the www-data user on the production box to fetch the newest jar file and restart the service

set -e
set -x

KEEP_VERSIONS=10
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

GIT_DIR_PATH="$BASE_DIR/.git"
DEPLOY_KEY="$(cat ~/.globalcopy-prod-deploy-key-ro)"

# figure out if we are prod or staging
if [[ "$BASE_DIR" =~ stage.globalcopy.io ]]; then
    DEPLOY_BRANCH="stage"
    DEPLOY_SERVICE_NAME="globalcopy-io-stage"
elif [[ "$GIT_DIR_PATH" =~ cmyers.dev.globalcopy.io ]]; then
    DEPLOY_BRANCH="dev/cmyers"
    DEPLOY_SERVICE_NAME="globalcopy-io-cmyers"
elif [[ "$GIT_DIR_PATH" =~ globalcopy.io ]]; then
    DEPLOY_BRANCH="prod"
    DEPLOY_SERVICE_NAME="globalcopy-io"
else
    echo "Unrecognized path: '$GIT_DIR_PATH' must contain staging or production, or an alpha environment" 1>&2
    exit 1
fi

FORCE_DEPLOY="${FORCE_DEPLOY:-false}"

ssh-agent bash -c "ssh-add ~/.ssh/id_rsa; git --git-dir='$GIT_DIR_PATH' fetch -q origin '$DEPLOY_BRANCH'"

# first figure out if the git checkout has changed
NEW_REV="$(git --git-dir="$GIT_DIR_PATH" rev-parse origin/$DEPLOY_BRANCH)"
WD_REV="$(git --git-dir="$GIT_DIR_PATH" rev-parse HEAD)"
if [[ "$NEW_REV" != "$WD_REV" && "$RE_EXEC" != "true" ]]; then
    cd "$BASE_DIR" && git reset --hard "origin/$DEPLOY_BRANCH"
    # re-exec ourself, but prevent infinite loop just in case
    echo "Updating repo and Re-executing ourself to run newest version of deploy script..."
    cd "$BASE_DIR" && git reset --hard "origin/$DEPLOY_BRANCH"
    export RE_EXEC="true"
    export FORCE_DEPLOY="true"
    exec "$BASE_DIR/bin/deploy.sh"
fi

# calculate last successfully deployed rev
if [[ -f "$BASE_DIR/current/version.txt" ]]; then
    CURRENT_REV="$(cat "$BASE_DIR/current/version.txt")"
else
    CURRENT_REV="$(git --git-dir="$GIT_DIR_PATH" rev-parse HEAD)"
fi

echo "Currently Deployed Revision: $CURRENT_REV"
echo "Current Prod Branch Revision: $NEW_REV"

if [[ "$FORCE_DEPLOY" != "true" ]]; then
    if [[ "$CURRENT_REV" == "$NEW_REV" ]]; then
        echo "Nothing to deploy"
        exit 0
    fi
else
    echo "Forcing deploy..."
fi

echo "Updating repo..."
cd "$BASE_DIR" && git reset --hard "origin/$DEPLOY_BRANCH"

echo "Getting artifacts..."

LATEST_PIPELINE_OBJ="$(curl -H "PRIVATE-TOKEN: $DEPLOY_KEY" https://gitlab.com/api/v4/projects/globalcopy%2Fglobalcopy.io/pipelines/latest?ref=$DEPLOY_BRANCH)"
PIPELINE_SHA="$(echo "$LATEST_PIPELINE_OBJ" | jq -r .sha)"
PIPELINE_STATUS="$(echo "$LATEST_PIPELINE_OBJ" | jq -r .status)"
PIPELINE_ID="$(echo "$LATEST_PIPELINE_OBJ" | jq -r .id)"

echo "Got pipeline id: $PIPELINE_ID, sha $PIPELINE_SHA, status $PIPELINE_STATUS"
if [[ "$PIPELINE_STATUS" != "success" ]]; then
    echo "Status not successful, exiting"
    exit 1
fi
if [[ "$PIPELINE_SHA" != "$NEW_REV" ]]; then
    echo "latest pipeline not for new rev, exiting"
    exit 1
fi

PACKAGE_JOB_ID="$(curl -H "PRIVATE-TOKEN: $DEPLOY_KEY" https://gitlab.com/api/v4/projects/globalcopy%2Fglobalcopy.io/pipelines/"$PIPELINE_ID"/jobs | jq '.[] | select(.name | contains("Package")).id')"
echo "Got package job id: $PACKAGE_JOB_ID"

# create temp dir
SCRATCH=$(mktemp -d -t tmp.XXXXXXXXXX)
function finish {
    rm -rf "$SCRATCH"
    # if we detect current version was moved out of the way, put it back
    if [[ -d "$BASE_DIR/$NEW_REV.old" ]]; then
        rm -rf "${BASE_DIR:?}/$NEW_REV"
        mv "$BASE_DIR/$NEW_REV.old" "$BASE_DIR/$NEW_REV"
        sudo service $DEPLOY_SERVICE_NAME restart
    fi
}
trap finish EXIT

echo "Fetching artifacts.zip..."
curl -v -L -H "PRIVATE-TOKEN: $DEPLOY_KEY" https://gitlab.com/api/v4/projects/globalcopy%2Fglobalcopy.io/jobs/"$PACKAGE_JOB_ID"/artifacts --output "$SCRATCH/artifacts.zip"

echo "Unzipping..."

# delete old static content
#rm -rf "$STATIC_PATH"

# place the server and client files where they belong
unzip -j -d "$SCRATCH" "$SCRATCH/artifacts.zip"

echo "Validating artifact..."
# we only need two files, the war and the jar
SERVERWAR="$(ls "$SCRATCH/"globalcopy.io-server*.war)"
CLIENTJAR="$(ls "$SCRATCH/"globalcopy.io-client*.jar)"
if [[ ! -f "$SERVERWAR" ]]; then
    echo "Server war not found!" 1>&2
    exit 1
fi
if [[ ! -f "$CLIENTJAR" ]]; then
    echo "Client jar not found!" 1>&2
    exit 1
fi

echo "Setting up new revision $NEW_REV..."

if [[ "$NEW_REV" == "$CURRENT_REV" ]]; then
    if [[ -d "$BASE_DIR/versions/$NEW_REV" ]]; then
        echo "New rev and current rev are the same, moving current version out of the way"
        mv "$BASE_DIR/versions/$NEW_REV" "$BASE_DIR/versions/$NEW_REV.old"
    fi
fi

echo "Making directory structure under $BASE_DIR/versions/$NEW_REV"
mkdir -p "$BASE_DIR/versions/$NEW_REV"
mkdir -p "$BASE_DIR/versions/$NEW_REV/static/clients"

echo "Placing artifacts in correct location"
mv "$SCRATCH"/globalcopy.io-server*.war "$BASE_DIR/versions/$NEW_REV/"
mv "$SCRATCH"/globalcopy.io-client*.jar "$BASE_DIR/versions/$NEW_REV/static/clients"

echo "Creating latest symlinks"
ln -s "$BASE_DIR/versions/$NEW_REV"/globalcopy.io-server*.war "$BASE_DIR/versions/$NEW_REV/globalcopy.io.war"
ln -s "$BASE_DIR/versions/$NEW_REV"/static/clients/globalcopy.io-client*.jar "$BASE_DIR/versions/$NEW_REV/static/clients/globalcopy.io-client-latest.jar"

# update "current" symlink as an atomic flip
echo "Flipping 'current' symlink from $(readlink "$BASE_DIR/current") to $BASE_DIR/versions/$NEW_REV"
# -T -f will ensure existing symlink is overwritten
ln -T -f -s "$BASE_DIR/versions/$NEW_REV" "$BASE_DIR/current"

# confirm success by updating version.txt
echo "Writing version.txt stamp file"
echo -n "$NEW_REV" > "$BASE_DIR/current/version.txt"

echo "Restarting service..."
sudo service $DEPLOY_SERVICE_NAME restart

echo "Service status:"
sudo service $DEPLOY_SERVICE_NAME status

echo "Checking for versions needing cleanup..."
# https://superuser.com/a/708232
VERSIONS_TO_DELETE="$(find "$BASE_DIR/versions" -maxdepth 1 -type d -printf '%Ts\t%P\n' | sort -n | head -n -"$KEEP_VERSIONS" | cut -f 2-)"
if [[ -n "$VERSIONS_TO_DELETE" ]]; then
    echo "Versions to delete: $VERSIONS_TO_DELETE"
    for i in $VERSIONS_TO_DELETE; do
        echo "Deleting $BASE_DIR/versions/$i"
        rm -rf "$BASE_DIR/versions/$i"
    done
fi
echo "Done."
