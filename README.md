# globalcopy.io

GlobalCopy is a service which makes your copy buffers global across all the machines you use!

GlobalCopy is a work in progress.

* We have a community [Code of Conduct](CODE_OF_CONDUCT.md)
* We have a [Contributor Guide](CONTRIBUTING.md)
* We have a Style Guide (explained how to use in the [Development Guide](docs/dev-setup.md), or see [IntelliJ Code Style XML](docs/codestyle/Project.xml))
* We have [Development Guide](docs/dev-setup.md)

# Development Guide

## Dependencies

GlobalCopy requires a sufficiently new Java. You can obtain JDK19 [here](https://jdk.java.net/19/).

GlobalCopy uses Gradle to build and fetch all other necessary dependencies.

## Running tests

You can run tests by running the following command:
```
$ ./gradlew test
```

## Setting up your IDE

I recommend using IntelliJ's IDEA project. You can download the [community edition](https://www.jetbrains.com/idea/download/) for free.

Generate project files by running:
```
$ ./gradlew idea
```
Then, you can load the project in your IDE.

## Running the service locally

To run the service locally, you can invoke this gradle target:
```
$ ./gradlew bootRun
> Task :bootRun

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v3.1.0)

2023-05-26T13:46:19.678-07:00  INFO 1246439 --- [           main] io.globalcopy.GlobalCopyApp              : Starting GlobalCopyApp using Java 19.0.2 with PID 1246439 (/home/
cmyers/projects/globalcopy.io/build/classes/java/main started by cmyers in /home/cmyers/projects/globalcopy.io)
2023-05-26T13:46:19.680-07:00  INFO 1246439 --- [           main] io.globalcopy.GlobalCopyApp              : No active profile set, falling back to 1 default profile: "defaul
t"
2023-05-26T13:46:20.121-07:00  INFO 1246439 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2023-05-26T13:46:20.126-07:00  INFO 1246439 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2023-05-26T13:46:20.127-07:00  INFO 1246439 --- [           main] o.apache.catalina.core.StandardEngine    : Starting Servlet engine: [Apache Tomcat/10.1.8]
2023-05-26T13:46:20.178-07:00  INFO 1246439 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2023-05-26T13:46:20.179-07:00  INFO 1246439 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 476 ms
2023-05-26T13:46:20.280-07:00  INFO 1246439 --- [           main] o.s.b.a.w.s.WelcomePageHandlerMapping    : Adding welcome page: ServletContext resource [/index.html]
2023-05-26T13:46:20.357-07:00  INFO 1246439 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2023-05-26T13:46:20.362-07:00  INFO 1246439 --- [           main] io.globalcopy.GlobalCopyApp              : Started GlobalCopyApp in 0.857 seconds (process running for 1.002
)
2023-05-26T13:46:23.789-07:00  INFO 1246439 --- [nio-8080-exec-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring DispatcherServlet 'dispatcherServlet'
2023-05-26T13:46:23.789-07:00  INFO 1246439 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Initializing Servlet 'dispatcherServlet'
2023-05-26T13:46:23.790-07:00  INFO 1246439 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Completed initialization in 1 ms
```
You can see from the above output that the service is running on localhost port 8080.

Some useful endpoints are:
* http://localhost:8080/ => "Coming Soon" landing page
* http://localhost:8080/hello?name=MyName => says hello
* http://localhost:8080/actuator/health => returns `{"status":"UP"}` if service is healthy

## Running the service in production

For full details, see [docs/production-setup.md](docs/production-setup.md).
