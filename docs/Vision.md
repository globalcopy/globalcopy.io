# Vision for GlobalCopy.io

GlobalCopy is imagined as a global copy buffer, shared across all of your
devices, allowing you to instantly synchronize content between devices, and to
copy content using device A then immediately paste that content on device B.
Additionally, the operator of the service should have no ability to view the
content, making it safe to use for passwords, or just about anything, even
without running your own instance of the service.

## Project Motivation

I have spent the great majority of my career working on internal tools. I am a
big believer in tools as an investment in your future. You might delay your
day-to-day work in order to write a tool for yourself, and this might set you
back an hour or a day, even though it only saves you a few minutes. Still, if
that tool goes on to continue saving you a few minutes every day for weeks or
months or years, it justifies itself. In my experience, 10x engineers often set
themselves apart from others by their willingness to create, tailor, and invest
in their tools. It is an investment in yourself!

This project idea came from my own desires for a tool to solve a problem. During a normal work day, I interact with many devices:
* My work desktop
* My work laptop
* My personal lapto
* My phone
* My dedicated Zoom machine

I might want to copy content between any of these devices at any time. I
frequently switch between them multiple times even across 5 minutes.  Imagine
someone sends me a link over zoom chat, and I need to get it to my work desktop
to view it.  Or, someone sends me a zoom link and I need to move it to the zoom
machine. Sometimes, I grab a password from my password manager (available on my
phone) and I need to get it somewhere else.  I might like not to run my
personal password manager on my work machines, but today I do this because it
is too frustrating not to do so.

This tool finally gives one a better alternative than just sending self-addressed emails. It is easy to imagine additional features that considerably enhance what a copy buffer even means to the user as well! If this content is going to hit a server anyways, why not store a history? Can I request the 4th most recent copy? How about the last thing copied from device X? This is similar to copy buffers in Vim or Emacs.

Security is a must from the begining if this is to be trusted for passwords,
OTPs, and so on. The operator of the service must not be able to view the
copied content, so end-to-end local encryption must be used at all times, and
the encryption keys must be derived from client-side shared secrets and never
stored by or known to the service.

## MVP

The MVP is the simplest version of this service that could be minimally useful to anyone.  I imagine it might be composed of the following milestones:

Milestone 1
* A user may create and authenticate an account with a username and password
** Optional Extension: A user is able to instead create or authenticate using an OpenID provider (e.g. Github, Google, Microsoft, Apple, Facebook, etc)
** Suggested Implementation: The password is stored using Bcrypt
* A user is able to get an authentication token and use it to make authenticated API calls without the username or password
** Optional Extension: Tokens can be read-only, or read-write
** Suggested Implementation: JSON Web Tokens (JWTs)
* A user can POST content directly using the API
* A user can GET or HEAD content directly using the API
* A website exists where a user can view or change the content via a web browser. The website should call the same APIs created above.

Milestone 2: first device client (probably linux with xsel and a cronjob)
* A user copies content on machine A, then pastes on machine B, and the contents is carried over to the second machine
* A user copies content on machine A, then copies content on machine B shortly after, then pastes on machine C. They get the contents from machine B.
* <same as above but paste on machine A>
* A user may configure the polling rate of their client
* A user may manually request a poll (i.e. by copying a special string, or pressing a special keybinding)
* The client should use the same "public" APIs produced in the first milestone, modifying or extending them if needed

Milestone 3: Security
* A user may enable end-to-end encryption by entering a password in their client, which is saved client-side, never sent to the server, and used to encrypt and decrypt all content
** Suggested Implementation: Ideally the encryption used will be AES256
** If the encryption password is wrong (ensure it can decrypt old value?), the client should refuse to overwrite the value (to prevent accidental corruption by two clients with different passwords)
* A user may rotate their encryption password via the web client.
** Again, this must not send the password to the server
** This process should be atomic and will probably require a bulk fetch and update API
** After invoking this function, a user will have to update the password used by all their clients, so the feature above about refusing to overwrite values when the password is wrong becomes extra important

Milestone 4: DoS protection, History

* The service operator may choose a maximum size for each content entry
* The service operator may specify a minimum polling interval and/or throttle connections by account
* A user may request previous values via a history API.
* The service operator may specify a maximum count of items and total size of items to store per account
* A user may delete a particular entry from history if desired
* A user may choose a maximum history length to keep by age, count, or size, as long as it does not exceed the service operator's value

Milestone 5+: Additional clients

* Everything client-side from milestone 2, 3, and 4 - ported over to additional clients

## Commercial Opportunities

I (cmyers) do not plan to sell or commercialize this idea, necessarily. If what
we create is wildly successful, however, I could imagine pursing it. In that
case, my commitment to the contributors of this project is that the source code
will remain open (so anyone could spin up a competing version) and licensed
under the UNLICENSE, and if possible any service I run will always offer a free
tier.

The most likely way for this to be commercialized, IMO, is for people to pay a
monthly fee for more storage, faster polling rate, and history. I can't imagine
being unable to support a free tier with a 1kb limit, no history, and minimum 5
minute polling interval just with ads or whatever. The expensive part of this
service will always be scaling (i.e. storing a 10MB image in a copy buffer,
handling over 100 requests per second, over 1,000 concurrent users, etc), which
fortunately is NOT a problem I anticipate having any time soon =)

