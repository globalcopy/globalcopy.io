# Persistence Solution For Backend

The server will need a place to store and back-up it's data.
This will prevent data loss in the event of a crash, maintenance, or other outage.
Needs to store the actual copy buffer, the history of the copy buffer, and the account info.
Client doesn’t need access to this, it can just store it’s account info and settings in a config file.
The bandwidth required of the solution chosen will depend on how widely the system ends up being used.
Upsizing later is an option.

Options:
Local Computer (SQLite):

- No costs other than electricity and maintenance
- Simple
- Hard to scale up
- Susceptible to outages
- Easy to test

Documentation:
https://www.sqlite.org/about.html
https://www.tutorialspoint.com/sqlite/sqlite_java.htm

In-memory DB (with optional database dumps for persistence):

- No costs other than electricity and maintenance
- Simple
- Faster compared to using disk space
- Hard to scale up
- Very convenient for testing
- Not designed to truly persist, better as a temporary database


Documentation:
https://www.baeldung.com/java-in-memory-databases

Run Postgres on the Production Box ourselves:

- Similar to RDS but with a lot more manual management overhead
- “free” in that we are already paying for the server
- All the other benefits of postgresql
- Not as scalable
- More work for the client

Amazon RDS:

https://instances.vantage.sh/rds/

- Cheapest option $8.47 per month bought yearly or $11.68 on demand (cost estimator says actual cost is ~$14/mo, details here)
- Needs to be set up
- Postgresql is open source and compatible with many drop-in alternatives
- Postgresql is extremely common and well documented and has examples all over the internet
- Easy to upsize
- Reliable
- Local testing could use a local postgresql db, making prod and dev more similar

Datastax Astra DB

https://www.datastax.com/products/datastax-astra/pricing

- Free at lower volumes, more expensive than alternatives when more traffic is involved
- Needs to be set up
- Easiest to upsize because it is serverless, price scales with usage, only solution that can scale larger than a single instance without difficulty
- Reliable
- Need to look into testing but likely similar to Amazon
- Based upon Apache Cassandra, but API/Driver looks proprietary (need to confirm this?)
- This document explains some drawbacks of Apache Cassandra (which Datastax is based upon): https://www.pythian.com/blog/cassandra-use-cases
- Slightly less expensive than RDS up until around 25 million request per month.


Decision:
After analysis of the pricing and discussion with the client, we’ve decided on the Amazon RDS solution due to the scalability and easier to work with architecture of it.
