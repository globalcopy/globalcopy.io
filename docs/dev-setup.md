# Developer Setup

This document captures the steps to set up a development environment. Steps are clearly identified as either required, suggested, or optional. Steps should also specify if they are OS-specific.

# Linux Environment Options

If you are currently in a windows environment, you can download the [Windows Terminal](https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701) app from the Microsoft Store. 

After opening it, near the top of the window select the `dropdown arrow` near the top -> `Settings` -> `Default Profile` -> `Ubuntu`. Select `Save`. You can now use the Ubuntu profile as your default profile to run your linux environment. 

Virtual Machine setup is also supported, such as following the [Guide for running Ubuntu on VirtualBox](https://ubuntu.com/tutorials/how-to-run-ubuntu-desktop-on-a-virtual-machine-using-virtualbox#1-overview). 

For response speed, is highly reccomneded that the host machine has hardware virtualization to increase virtual CPUs in VirtualBox. Found under `Settings` -> `System` -> `Processor`

# Local dependencies

Globalcopy depends upon the following dependencies:

* a JDK17 on your path and a properly set JAVA_HOME
* the git source control tool

## JDK

You can validate your setup by doing this:

```
$ java -version
java -version
openjdk version "19.0.2" 2023-01-17
OpenJDK Runtime Environment (build 19.0.2+7-44)
OpenJDK 64-Bit Server VM (build 19.0.2+7-44, mixed mode, sharing)
$ which java
/home/cmyers/jvms/jdk-19.0.2/bin/java
$ echo $JAVA_HOME
/home/cmyers/jvms/jdk-19.0.2
```

If you don't have similar output, you may need to obtain a JDK and/or set some environment variables.

_TODO: add directions for Mac with Homebrew_

On most Debian-derived Linux distributions, you can install the JDK using your package manager:
```
$ sudo apt install openjdk-19-jdk
```
Or, you can download the appropriate tarball from [jdk.java.net](https://jdk.java.net/19/) and install it in user space, then add it to your path:
```
$ export JAVA_HOME="/home/cmyers/jvms/jdk-19.0.2"
$ export PATH="$PATH:$JAVA_HOME/bin"
```

Add these lines to your `~/.bashrc` (or the appropriate file for your shell) to make them affect all shells.

## Git

The Git source control tool is a distributed version control system. You can validate you have it like this:

```
$ git --version
git version 2.30.2
```

On most systems, you can install it using your package manager. Debian-derived Linux will look like this:
```
$ sudo apt install git
```

Unless your version of git is very old, it should just work. Git is not very version-sensitive.

# Fetch code (Gitlab setup)

With dependencies installed, you should be ready to fetch the code from Gitlab. While you are at it, you should also set up your SSH key.

If you already have an SSH key, you can use it, although generating a new one for use with this project is not a bad idea either.
```
$ file ~/.ssh/id_rsa
/home/cmyers/.ssh/id_rsa: PEM RSA private key
$ file ~/.ssh/id_rsa.pub
/home/cmyers/.ssh/id_rsa.pub: OpenSSH RSA public key
```
If these files do not exist, you can generate them. I recommend doing it like this:
```
$ ssh-keygen -t rsa -b 4096 ~/.ssh/id_rsa
```
You can use an alternate filename if desired (if you choose `foo`, the resulting files will be `foo` and `foo.pub`)

You will be prompted for a password - you can leave it blank if you trust your local machine, or use a password. If you use a password, you will be prompted to enter it each time you use the key, or you will want to set up an SSH agent.

_TODO: we could add ssh agent instructions here, but you can also just google it_

You are now ready to upload your SSH public key. Go to [Gitlab Profile SSH Keys](https://gitlab.com/-/profile/keys). Log in if prompted. Click "add new key" and paste the contents of `~/id_rsa.pub` into the `Key` field. On Linux, you can put the contents of the file into your copy buffer like this:
```
$ cat ~/.ssh/id_rsa.pub | xsel --clipboard
```
If the above command doesn't work and you are using Windows Terminal, you can also try this command:
```
cat ~/.ssh/id_rsa.pub | clip.exe
```
Put in a descriptive title, select "authentication and signing" for usage type, and click "add key".  Now, your key will authenticate you to gitlab. If you chose a name other than `id_rsa`, you can set it up to use your file automatically by adding an entry to `~/.ssh/config` that looks like this:
```
Host gitlab.com
  HostName gitlab.com
  IdentityFile ~/.ssh/id_rsa
```
Finally, you are ready to clone the repository:
```
$ cd ~/projects
$ git clone git@gitlab.com:globalcopy/globalcopy.io
Cloning into 'globalcopy.io'...
remote: Enumerating objects: 139, done.
remote: Counting objects: 100% (96/96), done.
remote: Compressing objects: 100% (76/76), done.
remote: Total 139 (delta 24), reused 0 (delta 0), pack-reused 43
Receiving objects: 100% (139/139), 230.68 KiB | 1.95 MiB/s, done.
Resolving deltas: 100% (28/28), done.
$ cd globalcopy.io
```

# Build the code

Now that you have the code, you can build it using Gradle. Gradle is designed to bootstrap itself, so it will automatically download itself and any additional dependencies you are missing (other than those specified in prior sections).

```
$ ./gradlew clean test assemble
To honour the JVM settings for this build a single-use Daemon process will be forked. See https://docs.gradle.org/8.1.1/userguide/gradle_daemon.html#sec:disabling_the_daemon.
Daemon will be stopped at the end of the build 

> Task :test
OpenJDK 64-Bit Server VM warning: Sharing is only supported for boot loader classes because bootstrap classpath has been appended

BUILD SUCCESSFUL in 8s
8 actionable tasks: 8 executed
```

You should see output similar to the above. You can run a local copy of globalcopy like this:
```
$ ./gradlew bootRun
To honour the JVM settings for this build a single-use Daemon process will be forked. See https://docs.gradle.org/8.1.1/userguide/gradle_daemon.html#sec:disabling_the_daemon.
Daemon will be stopped at the end of the build 

> Task :bootRun

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v3.1.0)

2023-09-04T16:25:10.192-07:00  INFO 3307670 --- [           main] io.globalcopy.GlobalCopyApp              : Starting GlobalCopyApp using Java 19.0.2 with PID 3307670 (/home/cmyers/projects/globalcopy.io/build/classes/java/main started by 
cmyers in /home/cmyers/projects/globalcopy.io)
2023-09-04T16:25:10.194-07:00  INFO 3307670 --- [           main] io.globalcopy.GlobalCopyApp              : No active profile set, falling back to 1 default profile: "default"
2023-09-04T16:25:10.704-07:00  INFO 3307670 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2023-09-04T16:25:10.712-07:00  INFO 3307670 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2023-09-04T16:25:10.712-07:00  INFO 3307670 --- [           main] o.apache.catalina.core.StandardEngine    : Starting Servlet engine: [Apache Tomcat/10.1.8]
2023-09-04T16:25:10.757-07:00  INFO 3307670 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2023-09-04T16:25:10.758-07:00  INFO 3307670 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 541 ms
2023-09-04T16:25:10.895-07:00  INFO 3307670 --- [           main] o.s.b.a.w.s.WelcomePageHandlerMapping    : Adding welcome page template: index
2023-09-04T16:25:11.011-07:00  INFO 3307670 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 1 endpoint(s) beneath base path '/actuator'
2023-09-04T16:25:11.042-07:00  INFO 3307670 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2023-09-04T16:25:11.051-07:00  INFO 3307670 --- [           main] io.globalcopy.GlobalCopyApp              : Started GlobalCopyApp in 1.029 seconds (process running for 1.222)
<==========---> 80% EXECUTING [39s]
> :bootRun
```

It will keep running in the foreground. You can see the results on localhost port 8080 in a different shell:
```
$ curl http://localhost:8080
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Global Copy: Coming Soon</title>
</head>
<body>
    <h2>Global Copy: Coming Soon</h2>
    <p>
        Global Copy is an open source project to allow you to make your copy buffers global across all the machines you use, with safety, convenience, and privacy.
    </p>
</body>
</html>
```

# Setting up your IDE

We recommend using IntelliJ IDEA for this project. IntelliJ has fantastic context-sensitive support for Java, consistent code style formatting, and plugins to support VIM and EMACS modes.  It is also free for non-commerical use =)

## Download and Install IntelliJ

To download IntelliJ IDEA, head to [the Jetbrains Website](https://www.jetbrains.com/idea/download/). You can download the community edition for free and it has everything you need. As of this writing, they are on version 2023.2.1.

## Generate Project Files

Project settings often include custom and user-specific things, like where your JDK lives, or what color schemes you prefer.  For this reason, the project files are not checked into source control.  Instead, base project files can be generated by running a gradle command. They are git-ignored so they will not be committed.

```
$ ./gradlew idea
To honour the JVM settings for this build a single-use Daemon process will be forked. See https://docs.gradle.org/8.1.1/userguide/gradle_daemon.html#sec:disabling_the_daemon.
Daemon will be stopped at the end of the build 

> Task :idea
Generated IDEA project at file:///home/cmyers/projects/globalcopy.io/globalcopy.io.ipr

BUILD SUCCESSFUL in 8s
4 actionable tasks: 4 executed
```

## Import code style

As a final step, you should import the code style settings so your IDE can format the code for you.

Consistent code style is very important to make a codebase easy to read and understand, and to ensure diffs created by different developers apply cleanly. If you change the whitespace formatting or order of imports in a file, then your diff is more likely to create confusion and conflicts.  For this reason, GlobalCopy uses an intelliJ format file to specify all formatting and it is expected that all devs will follow the style so that "reformat code" run on the entire project would not create any diffs.

Go to `File`->`Settings`, select `Editor`, `Code Style`. Click the gear icon next to `Scheme:` and select `Import Scheme`-> `IntelliJ IDEA code style XML`. Navigate to the project repository, then `docs/codestyle/Project.xml`. Click "ok" then name the scheme (something like `GlobalCopy`). Click ok.


You should now see your new scheme selected in the `Scheme:` dropdown.  Click the gear next to it again and select `Copy to Project...` and click "yes".

To test if this worked, you can click the `src` directory, then right-click it and select "Reformat Code...", click the scope checkbox and specify "All Places", then "Run". After doing this, you should see no changes if you run `git diff`.

## TODO: Other steps

There may be a step here where you have to tell the project to use the correct JDK (17 or 19), and/or import the gradle project file so it uses gradle and gradle settings to build.

It has been a while since I did that so I don't remember.  Ping cmyers@ if you have problems and then we'll fill this section out better.

## Optional IDE Settings

You can now install the VIM or ECLIPSE plugins for IntelliJ, and do any other customization you want.

# Pushing changes to Gitlab

You can create a local commit and push your changes to a feature branch in gitlab.

```
# always start development on a development branch, to minimize confusion
$ git checkout -b dev/cmyers origin/main
$ vim src/main/resources/templates/about.html
#... edit the file...
$ git add src/main/resources/templates/about.html
$ git commit -m"Adding Carl's information to the about page"
$ git push origin HEAD HEAD:dev/cmyers
```
Now you can create a merge request in Gitlab to begin the code review process.

# Setting up a staging environment

A staging environment is an entire globalcopy stack running on the production host, with its own DNS name (i.e. https://yourname.dev.globalcopy.io) where you can test your changes in a production-like environment and show others your work (i.e. UI/UX review).

It isn't necessary to have one, you can always just run it locally, but it may be convenient, especially since some of our features may need to be tested from multiple devices like phones and computers on the internet.

In order to do this, you will need a shell on the production box and sudo access.  _Nothing is critical here, outages don't mean dollars, but that said, pretend we are a real production service and double-check your commands before you hit enter!_

You will notice the root user on the prod box has a special prompt with red `PRODUCTION` in it to help you notice =)

## Set branch permissions

Once your staging environment is created, *anyone* who can push to your branch can run code on our production box. For this reason, you should protect all of your branches.

* Go to [Repository Settings](https://gitlab.com/globalcopy/globalcopy.io/-/settings/repository) and click on "Protected Branches"
* Click "Add protected branch"
* In the Branch dropdown, enter `dev/YOURNAME*` and click "Create Wildcard"
* For "Allowed to merge" and "Allowed to push and merge" put only yourself
* For "Allowed to force push" set it to enabled
* For "Require approval from code owners" set it to disabled
* Click "Protect"

Now only you can push to your staging environemnt (and your other branches that start with `dev/YOURNAME`).

## Create a directory for your stack

I recommend using your username so it is easy to figure out.  For example, mine is `cmyers.dev.globalcopy.io`. On the production host, create: `/var/www/YOUR_NAME.dev.globalcopy.io`
Copy the production stack initially, to populate it.
```
PRODUCTION root@globalcopy # mkdir /var/www/YOURNAME.dev.globalcopy.io
PRODUCTION root@globalcopy # cp -r /var/www/globalcopy.io/* /var/www/YOURNAME.dev.globalcopy.io/
PRODUCTION root@globalcopy # cp -r /var/www/globalcopy.io/.git* /var/www/YOURNAME.dev.globalcopy.io/
PRODUCTION root@globalcopy # chown -R www-data:www-data /var/www/YOURNAME.dev.globalcopy.io
```

## Set up the service to run

Create the following files:

### Environment File
```
# contents of /etc/globalcopy/YOURNAME.env
ROOT_DIR=/usr/bin
EXEC_JAR="/var/www/YOURNAME.dev.globalcopy.io/globalcopy.io.war"
JAVA_OPTS="-Xmx512m -Dserver.port=YOURPORT"
USER="www-data"
```
Choose a value for YOURPORT that is not yet used.  You can confirm using `lsof`:
```
PRODUCTION root@globalcopy # lsof -P | grep LISTEN | grep 8080
java        61993                     www-data   11u     IPv6             280410       0t0        TCP *:8080 (LISTEN)
java        61993   61995 java        www-data   11u     IPv6             280410       0t0        TCP *:8080 (LISTEN)
java        61993   61999 GC\x20Thr   www-data   11u     IPv6             280410       0t0        TCP *:8080 (LISTEN)
```

so 8080 is used....

```
PRODUCTION root@globalcopy # lsof -P | grep LISTEN | grep 8082
PRODUCTION root@globalcopy #
```

No output, so 8082 is available

### Systemd service file
```
# contents of /etc/systemd/system/globalcopy-io-YOURNAME.service
[Unit]
Description=GlobalCopy.io Application (YOURNAME)
Documentation=https://gitlab.com/globalcopy/globalcopy.io
After=network.target

[Service]
EnvironmentFile=/etc/globalcopy/YOURNAME.env
Type=simple
User=www-data
WorkingDirectory=/usr/bin
ExecStart=/usr/bin/java $JAVA_OPTS -jar $EXEC_JAR StandardOutput=journal StandardError=journal SyslogIdentifier=globalcopy-io-stage SuccessExitStatus=143 TimeoutStopSec=10 Restart=on-failure RestartSec=60

[Install]
WantedBy=multi-user.target
```

### Start the service

```
$ sudo systemctl daemon-reload
$ sudo systemctl enable globalcopy-io-YOURNAME.service
$ sudo service globalcopy-io-YOURNAME start
```

You can check the status with this:
```
$ sudo service globalcopy-io-YOURNAME status
● globalcopy-io-cmyers.service - GlobalCopy.io Application (cmyers)
     Loaded: loaded (/etc/systemd/system/globalcopy-io-cmyers.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2023-09-05 13:58:46 UTC; 10s ago
       Docs: https://gitlab.com/globalcopy/globalcopy.io
   Main PID: 1813111 (java)
      Tasks: 35 (limit: 4695)
     Memory: 124.4M
        CPU: 7.593s
     CGroup: /system.slice/globalcopy-io-cmyers.service
             └─1813111 /usr/bin/java -Xmx512m -Dserver.port=8082 -jar /var/www/cmyers.dev.globalcopy.io/globalcopy.io.war StandardOutput=journal StandardError=journal SyslogIdentifier=globalcopy-io-stage SuccessExitStatus=143 TimeoutStopSec=1>

Sep 05 13:58:48 globalcopy.io java[1813111]: 2023-09-05T13:58:48.156Z  INFO 1813111 --- [           main] io.globalcopy.GlobalCopyApp              : No active profile set, falling back to 1 default profile: "default"
Sep 05 13:58:49 globalcopy.io java[1813111]: 2023-09-05T13:58:49.828Z  INFO 1813111 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8082 (http)
Sep 05 13:58:49 globalcopy.io java[1813111]: 2023-09-05T13:58:49.839Z  INFO 1813111 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
Sep 05 13:58:49 globalcopy.io java[1813111]: 2023-09-05T13:58:49.840Z  INFO 1813111 --- [           main] o.apache.catalina.core.StandardEngine    : Starting Servlet engine: [Apache Tomcat/10.1.8]
Sep 05 13:58:49 globalcopy.io java[1813111]: 2023-09-05T13:58:49.962Z  INFO 1813111 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
Sep 05 13:58:49 globalcopy.io java[1813111]: 2023-09-05T13:58:49.964Z  INFO 1813111 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1697 ms
Sep 05 13:58:50 globalcopy.io java[1813111]: 2023-09-05T13:58:50.330Z  INFO 1813111 --- [           main] o.s.b.a.w.s.WelcomePageHandlerMapping    : Adding welcome page template: index
Sep 05 13:58:50 globalcopy.io java[1813111]: 2023-09-05T13:58:50.592Z  INFO 1813111 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 1 endpoint(s) beneath base path '/actuator'
Sep 05 13:58:50 globalcopy.io java[1813111]: 2023-09-05T13:58:50.703Z  INFO 1813111 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8082 (http) with context path ''
Sep 05 13:58:50 globalcopy.io java[1813111]: 2023-09-05T13:58:50.731Z  INFO 1813111 --- [           main] io.globalcopy.GlobalCopyApp              : Started GlobalCopyApp in 3.178 seconds (process running for 3.777)
```

## Configure nginx

Finally, we need to set up nginx to proxy from the internal port (YOURPORT) to the external port with SSL (443). Here is the relevant bit from the nginx config files:

Add the following to the bottom of `/etc/nginx/sites-enabled/default`
```
# YOURNAME instance
server {

    server_name YOURNAME.dev.globalcopy.io;

    location / {
        proxy_pass http://localhost:YOURPORT;
    }

    listen [::]:80;
    listen 80;
}
```

Next, we need to run certbot so it can request an SSL cert for YOURNAME.dev.globalcopy.io and update the configs.  I have a script in the root user's bin directory to assist with this.  Edit the script `/root/bin/run-certbot.sh`. It should look like this:
```
#!/usr/bin/env bash

certbot --nginx -d globalcopy.io -d stage.globalcopy.io -d cmyers.dev.globalcopy.io
```

Add `-d YOURNAME.dev.globalcopy.io` to the line, save and exit, then run it. You will be prompted if you want to expand the list of domains, say "E" for expand. THe output of the script looks like this:

```
PRODUCTION root@globalcopy # ~/bin/run-certbot.sh
Saving debug log to /var/log/letsencrypt/letsencrypt.log

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
You have an existing certificate that contains a portion of the domains you
requested (ref: /etc/letsencrypt/renewal/globalcopy.io.conf)

It contains these names: globalcopy.io, stage.globalcopy.io

You requested these names for the new certificate: globalcopy.io,
stage.globalcopy.io, cmyers.dev.globalcopy.io.

Do you want to expand and replace this existing certificate with the new
certificate?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(E)xpand/(C)ancel: E
Renewing an existing certificate for globalcopy.io and 2 more domains

Successfully received certificate.
Certificate is saved at: /etc/letsencrypt/live/globalcopy.io/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/globalcopy.io/privkey.pem
This certificate expires on 2023-12-04.
These files will be updated when the certificate renews.
Certbot has set up a scheduled task to automatically renew this certificate in the background.

Deploying certificate
Successfully deployed certificate for globalcopy.io to /etc/nginx/sites-enabled/default
Successfully deployed certificate for stage.globalcopy.io to /etc/nginx/sites-enabled/default
Successfully deployed certificate for cmyers.dev.globalcopy.io to /etc/nginx/sites-enabled/default
Your existing certificate has been successfully renewed, and the new certificate has been installed.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
If you like Certbot, please consider supporting our work by:
 * Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
 * Donating to EFF:                    https://eff.org/donate-le
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```


It should write a config that looks something like this:
```
server {

    server_name cmyers.dev.globalcopy.io;

    location / {
        proxy_pass http://localhost:8082;
    }

    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/globalcopy.io/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/globalcopy.io/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}


server {
    if ($host = cmyers.dev.globalcopy.io) {
        return 301 https://$host$request_uri;
    } # managed by Certbot



    server_name cmyers.dev.globalcopy.io;

    listen [::]:80;
    listen 80;
    return 404; # managed by Certbot


}
```

You should now be able to go to http://YOURNAME.dev.globalcopy.io/, and it should redirect you to https://YOURNAME.dev.globalcopy.io/ and you should see the coming soon page.

## Set up cronjob to automatically update your branch

The final step is to update the deploy script and make a cron job that automatically fetches the newest version of your branch and builds it.

### Update Deploy Script

In your personal dev environment, you can edit the deploy script (`bin/deploy.sh`). Find the block that looks like this:
```
# figure out if we are prod or staging
if [[ "$DEPLOY_PATH" =~ stage.globalcopy.io ]]; then
    DEPLOY_BRANCH="stage"
    WAR_PATH="/var/www/stage.globalcopy.io"
    DEPLOY_SERVICE_NAME="globalcopy-io-stage"
elif [[ "$DEPLOY_PATH" =~ globalcopy.io ]]; then
    DEPLOY_BRANCH="prod"
    WAR_PATH="/var/www/globalcopy.io"
    DEPLOY_SERVICE_NAME="globalcopy-io"
else
    echo "Unrecognized path: '$DEPLOY_PATH' must contain staging or production, or an alpha environment" 1>&2
    exit 1
fi
```
Add another block to the list like this:
```
# figure out if we are prod or staging
if [[ "$DEPLOY_PATH" =~ stage.globalcopy.io ]]; then
    DEPLOY_BRANCH="stage"
    WAR_PATH="/var/www/stage.globalcopy.io"
    DEPLOY_SERVICE_NAME="globalcopy-io-stage"
elif [[ "$DEPLOY_PATH" =~ YOURNAME.dev.globalcopy.io ]]; then
    DEPLOY_BRANCH="dev/YOURNAME"
    WAR_PATH="/var/www/YOURNAME.dev.globalcopy.io"
    DEPLOY_SERVICE_NAME="globalcopy-io-YOURNAME"
elif [[ "$DEPLOY_PATH" =~ globalcopy.io ]]; then
    DEPLOY_BRANCH="prod"
    WAR_PATH="/var/www/globalcopy.io"
    DEPLOY_SERVICE_NAME="globalcopy-io"
else
    echo "Unrecognized path: '$DEPLOY_PATH' must contain staging or production, or an alpha environment" 1>&2
    exit 1
fi
```

Commit this change, push it to your `dev/YOURNAME` branch.

### Update crontab

Back on the production box, you can see the existing cronjobs as the `www-data` user like this:

```
PRODUCTION root@globalcopy # crontab -u www-data -l
# Edit this file to introduce tasks to be run by cron.
#
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
#
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').
#
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
#
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
#
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
#
# For more information see the manual pages of crontab(5) and cron(8)
#
# m h  dom mon dow   command
*/5 * * * * /var/www/stage.globalcopy.io/bin/deploy.sh
*/5 * * * * /var/www/globalcopy.io/bin/deploy.sh
```

Simply add an additional line to the end:
```
*/5 * * * * /var/www/YOURNAME.dev.globalcopy.io/bin/deploy.sh
```

The root user is set up to use vim by default.  If you don't know how to use vim, you can set EDITOR to override it.
```
PRODUCTION root@globalcopy # EDITOR=/bin/nano crontab -u www-data -e
```

After doing this, wait 5 minutes then look at `/var/www/YOURNAME.dev.globalcopy.io/bin/deploy.sh`.  It should be updated with your new deploy script, meaning the crontab ran successfully.

You can now create an MR to get your changes merged into the main branch.
