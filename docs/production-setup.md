# Running the service in production

We will build and deploy a `war` file to production. This is a self-contained jar file that includes the application server and everything (in our case, Tomcat).
```
$ ./gradlew clean test bootWar
$ find build | egrep 'war$'
build/libs/globalcopy.io-0.0.1-SNAPSHOT.war
$ java -jar build/libs/globalcopy.io-0.0.1-SNAPSHOT.war
java -jar build/libs/globalcopy.io-0.0.1-SNAPSHOT.war

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v3.1.0)

2023-05-30T07:59:18.250-07:00  INFO 3484713 --- [           main] io.globalcopy.GlobalCopyApp              : Starting GlobalCopyApp v0.0.1-SNAPSHOT using Java 19.0.2 with PID 3484713 (/home/cmyers/projects/globalcopy.io/build/libs/globalcopy.io-0.0.1-SNAPSHOT.war started by cmyers in /home/cmyers/projects/globalcopy.io)
2023-05-30T07:59:18.252-07:00  INFO 3484713 --- [           main] io.globalcopy.GlobalCopyApp              : No active profile set, falling back to 1 default profile: "default"
2023-05-30T07:59:19.029-07:00  INFO 3484713 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2023-05-30T07:59:19.036-07:00  INFO 3484713 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2023-05-30T07:59:19.036-07:00  INFO 3484713 --- [           main] o.apache.catalina.core.StandardEngine    : Starting Servlet engine: [Apache Tomcat/10.1.8]
2023-05-30T07:59:19.098-07:00  INFO 3484713 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2023-05-30T07:59:19.099-07:00  INFO 3484713 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 813 ms
2023-05-30T07:59:19.291-07:00  INFO 3484713 --- [           main] o.s.b.a.w.s.WelcomePageHandlerMapping    : Adding welcome page template: index
2023-05-30T07:59:19.464-07:00  INFO 3484713 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 1 endpoint(s) beneath base path '/actuator'
2023-05-30T07:59:19.505-07:00  INFO 3484713 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2023-05-30T07:59:19.516-07:00  INFO 3484713 --- [           main] io.globalcopy.GlobalCopyApp              : Started GlobalCopyApp in 1.51 seconds (process running for 1.789)
```

To run on a production Linux server, a few configuration files are needed. I generated them by following [this guidle](https://sma.im/how-to-run-your-java-springboot-app-jar-war-as-systemd-service-on-linux/):

Environment File (staging):
```
# contents of /etc/globalcopy/stage.env
ROOT_DIR=/usr/bin
EXEC_JAR="/var/www/stage.globalcopy.io/globalcopy.io.war"
JAVA_OPTS="-Xmx512m -Dserver.port=8080"
USER="www-data"
```

Systemd service file (staging):
```
# contents of /etc/systemd/system/globalcopy-io-stage.service
[Unit]
Description=GlobalCopy.io Application (stage)
Documentation=https://gitlab.com/globalcopy/globalcopy.io
After=network.target

[Service]
EnvironmentFile=/etc/globalcopy/stage.env
Type=simple
User=www-data
WorkingDirectory=/usr/bin
ExecStart=/usr/bin/java $JAVA_OPTS -jar $EXEC_JAR StandardOutput=journal StandardError=journal SyslogIdentifier=globalcopy-io-stage SuccessExitStatus=143 TimeoutStopSec=10 Restart=on-failure RestartSec=60

[Install]
WantedBy=multi-user.target
```

I installed OpenJDK 17 and some other deps:
```
$ sudo apt install openjdk-17-jdk jq git curl
```

I placed the `war` file in `/var/www/stage.globalcopy.io/globalcopy.io.jar`

I reloaded systemd to read the new config files, and enabled the service, and finally, started the service.
```
$ sudo systemctl daemon-reload
$ sudo systemctl enable globalcopy-io-stage.service
$ sudo service globalcopy-io-stage start
```

You can check the status with this:
```
$ sudo service globalcopy-io-stage status
● globalcopy-io-stage.service - GlobalCopy.io Application (stage)
     Loaded: loaded (/etc/systemd/system/globalcopy-io-stage.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2023-05-30 15:24:57 UTC; 2s ago
       Docs: https://gitlab.com/globalcopy/globalcopy.io
   Main PID: 55268 (java)
      Tasks: 20 (limit: 4695)
     Memory: 116.9M
        CPU: 5.048s
     CGroup: /system.slice/globalcopy-io-stage.service
             └─55268 /usr/bin/java -Xmx512m -jar /var/www/stage.globalcopy.io/globalcopy.io.war StandardOutput=journal StandardError=journal SyslogIdentifier=globalcopy-io-s>

May 30 15:24:57 globalcopy.io systemd[1]: Started GlobalCopy.io Application (stage).
May 30 15:24:58 globalcopy.io java[55268]:   .   ____          _            __ _ _
May 30 15:24:58 globalcopy.io java[55268]:  /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
May 30 15:24:58 globalcopy.io java[55268]: ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
May 30 15:24:58 globalcopy.io java[55268]:  \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
May 30 15:24:58 globalcopy.io java[55268]:   '  |____| .__|_| |_|_| |_\__, | / / / /
May 30 15:24:58 globalcopy.io java[55268]:  =========|_|==============|___/=/_/_/_/
May 30 15:24:58 globalcopy.io java[55268]:  :: Spring Boot ::                (v3.1.0)
May 30 15:24:58 globalcopy.io java[55268]: 2023-05-30T15:24:58.907Z  INFO 55268 --- [           main] io.globalcopy.GlobalCopyApp              : Starting GlobalCopyApp v0.0.>
May 30 15:24:58 globalcopy.io java[55268]: 2023-05-30T15:24:58.911Z  INFO 55268 --- [           main] io.globalcopy.GlobalCopyApp              : No active profile set, falli
```

Finally, we need to set up nginx to proxy from the internal port (8080) to the external port with SSL (443). Here is the relevant bit from the nginx config files:
```
# Staging instance
# Staging instance
server {
    server_name stage.globalcopy.io;

    location / {
        proxy_pass http://localhost:8080;
    }

    root /var/www/stage.globalcopy.io/current;
    location /static/ {
        autoindex on;
        try_files $uri $uri/ =404;
    }


    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/globalcopy.io/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/globalcopy.io/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}
```
Most of the SSL magic was written by certbot, if you write this and then run certbot, it will fill in the rest, including redirects to SSL.
```
# Staging instance
server {

    server_name stage.globalcopy.io;

    location / {
        proxy_pass http://localhost:8080;
    }

    root /var/www/stage.globalcopy.io/current;
    location /static/ {
        autoindex on;
        try_files $uri $uri/ =404;
    }

    listen [::]:80;
    listen 80;
}
```
