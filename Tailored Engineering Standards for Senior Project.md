# 6.4.2 Stakeholder Needs and Requirements Definition process
 
## a. Prepare for Stakeholder Needs and Requirements Definition
 
### 1. Identify the stakeholders who have an interest in the software system throughout its lifecycle.
a
 
"Make people whose lives and business are affected by your system part of the team". This quote from Chapter 9-Real Customer involvment; shows that any involvment with our software considers them a part of the team or in otherwords a Stakeholder.
 
### 2. Define the stakeholder needs and requirements definition strategy.
a
XP moves away from the term "requirements" and states that they arent often required in a literal sense. Instead we adopt stories that give a description of functionality that is to be implemented in the way we see fit.
"...Not “requirements”; they weren’t really mandatory or obligatory." Chapter 7-PairProgramming.
 
### 3. Identify and plan for necessary enabling systems or services needed to support stakeholder needs and requirements definition.
b,g 

Having constant communication with the client through syncronous meetings ensures that we are able to identify stakeholder meeds throughout iterations, and make preperations and requirements for the needs in advance through virtual stoty planning software. From Beck "XP-style planning is that stories are estimated very early," ensuring there is foresight for integration. Identification was done in advanced to establish cadence for the project.


 
### 4. Obtain or acquire access to the enabling systems or services to be used.
h

XP allows us to use TDD to ensure that enabling systems and services are functional for our system and for deployment. 
 
## b. Define stakeholder needs.
 
### 1. Define context of use within the concept of operations and the preliminary life cycle concepts.
 b

Context of use is defined by the stakeholder and fulfilling their needs through stories, which are also generated with the client. It is also defined as the stakeholder provides information on features and needs of the final user of the system. Concepts of operations are covered by the stories, following a format of "Currently [the system is able to] ... Make it so [the system is able to] ..."
 
### 2. Identify stakeholder needs.
d
 
In XP, the needs of the stakeholder are met by:

1. Constant meetings with the stakeholder to determine needs of the system per integration cycle.
2. Stories created to meet the needs of the stakeholder for the next and future integration cycles, with stakeholder involvement.
3. Clarifications made through internal communication with the stakeholder to clarify/generate stories.
4. Continuous integration of stories for review by other relevant stakeholders

### 3. Prioritize and down-select needs.
f, e

Establishing a weekly theme of the integration cycle and selecting stories relevant to the theme establishes prioritization of needs. This is done with each cycle: "The product manager adapts the story and theme to what is really happening now."
 
### 4. Define stakeholder needs and rationale.
d, e, i

XP focouses on the needs of the stakeholder exclusively, and are defined through stories generated with the client. Rationale is possible through critical thought of what would provide the most value to the stakeholder at the current time, with other needs being down-selected if they cannot be allocated.
 
## c. Develop the operational concept and other life cycle concepts.
 
### 1. Define a representative set of scenarios to identify the required capabiliies that corresepond to anticipated operational and other life cycle concepts.
b

The creation of stories using the "Currently ... Make it so ..." format gives us an idea of the status of the system and the intenion of the change for resolving such issue. This aids in defining anticipated activities as some stories "block" one another, and progress for the need to be address has an anticipated operation.
 
### 2. Identify the factors affecting interactions between users and the system.
c, f

XP allows us to specify stories that outline user interaction, environment and equipment, and system conditions, and appropriate responses dependent on the conditions. By ensure that we "Plan using units of customer-visible functionality," we can express the interactions of the system.
 
## d. Transform stakeholder needs into stakeholder requirements.
 
### 1. Identify constraints on system solution.
c
XP allows us to Spike system solutions for viability in the system, how they are to interact with the systen, etc. Constraints external of the system as legal or stakeholder requirement are factored as stories to ensure acceptable completion.
 
### 2. Identify the stakeholder requirements and functions that relate to critical quality characteristics, such as assurance, safety, security, environment, or health.
e
XP allows us to integrate stakeholder requiremnents in integration during weekly and quarterly cycles, and can be fielded as stories to ensure acceptable review/acceptance. " The clearer the quarterly cycle becomes at expressing the business priorities, the slimmer the requirement document needs to be."
 
### 3. Define stakeholder requirements, consistent with life cycle concepts, scenarios, interactions, constraints, and critical quality characteristics.
f
Communication with stakeholders through the software lifecycle via weekly meetings ensure that the software is able to meet needs timely, or as Beck writes: "Sometimes XP can facilitate shifting the constraint out of software development completely" through work and ensuring that other management tsks address them.

## e. Analyze stakeholder requirements.

### 1. Analyze the complete set of stakeholder requirements.

e 
As mentioned before, in XP stakeholder requirements are integrated in quarterly and weekly cycles and part of that integration is the understanding of the requirements.

### 2. Define critical performance measures that enable the assessment of technical achievement.

f 
XP has a large amount of importance put in the review of assignments and stories. Performance is measured and reviewed after each cycle. The measures are defined early, "One feature of XP-style planning is that stories are estimated very early in their life". The estimation is the resources spent in hours and tests that define how correctly they are implemented are the first thing completed.

### 3. Feed back the analyzed requirements to applicable stakeholders to validate that their needs and expectations have been adequately captured and expressed.

g 
In our work cycles on the same day we define our "requirments" or stories we have the stakeholder approve of the stories and help with the estimation done before work. "Have the customers pick a week’s worth of stories to implement this week."

### 4. Resolve stakeholder requirements issues.

g 
In the same meeting we establish requirements we ask for a mutual understanding and feedback related to the requirements.

## f. Manage the stakeholder needs and requirements definition.

### 1. Obtain explicit agreement with designated stakeholders on the stakeholder requirements.

g 
XP dictates that for every story created that the stakeholder is a part of the process and approval before work begins on it. Every story created is approved within minutes.

### 2. Maintain traceability of stakeholder needs and requirements.

h, i 
All stories are stored and kept on the boards for review. Since we do not have traditional "needs" or "requirements" they are linked and encapsulated in stories. 

### 3. Provide key artifacts and information items that have been selected for baselines.

h, i 
In XP we only provide explicit documention if it is requested. We do not maintain documents weekly. The only objects maintained are the story cards that contain all the information needed for our tasks and requirments.

# 6.4.4 Architecture Definition Process
 
## 1. Prepare for Architecture Definition 
### a. Review pertinent information and identify key drivers of the architecture 
H
The extreme programming implementation of this task is having stakeholders as a member of our team while we are developing because the key drivers of an architecture will ultimately stem from stakeholder needs, and so having our customers on board, helps us understand the pertinent information with regards to the project, and helps us identify key drivers of the architecture. “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.”
 
### b. Identify stakeholder concerns
A
In extreme programming we would likely have stakeholders as a member of our team while we are developing in order to adequately identify stakeholder concerns. “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.”

### c. Define the Architecture Definition roadmap, approach, and strategy
C
The roadmap, approach, and strategy is something that would be developed incrementally in extreme programming through incremental design so that the architecture definition roadmap, approach, and strategy satisfies the needs of the system that we are working on in a given day. “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.” 

### d. Define architecture evaluation criteria based on stakeholder concerns and key requirements 
A
In extreme programming we would likely have stakeholders as a member of our team while we are developing, thereby we would be able to evaluate the architecture with stakeholders as we are developing the software “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.”

### e. Identify and plan for the necessary enabling systems or services to support the Architecture Definition Process
J
In extreme programming, this task would be implemented by identifying the necessary systems or services for a given story in a certain week through incremental design. Then, we would subsequently make iterative changes when the stories we are working on warrant it. “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.” 

### f. Obtain or acquire access to the enabling systems or services to be used 
J
In extreme programming, this task would be implemented by obtaining or acquiring access to the necessary systems or services for a given story in a certain week through incremental design. Then, we would subsequently work to obtain access to new enabling systems or services when the stories we are working on warrant it. “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.” 
 
## 2. Develop architecture viewpoints
### a. Select, adapt, or develop viewpoints and models based on stakeholder concerns
B
The extreme programming implementation of this task would be to work closely with our stakeholders and through incremental design develop the viewpoints and models through our codes and tests. “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.” “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.” “Maintain only the code and the tests as permanent artifacts. Generate other documents from the code and tests. Rely on social mechanisms to keep alive the important history of the project.”
 
### b. Establish or identify potential architecture frameworks to be used in developing   models and views
D
The extreme programming implementation of this task is to use incremental design to identify frameworks that would help develop models and views given the current understanding of that system, and then once our understanding of the system changes through consistent stakeholder engagement, we would then re-evaluate if we can use the same framework or pivot to the use of a different framework.  “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.” “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.” 
	
## 3. Develop models and views of candidate architectures 
### a. Define the software system context and boundaries in terms of interfaces and interactions with external entities
C
In extreme programming, this task would be implemented by creating user stories that indicate the context and boundaries between the interfaces and interactions with external entities. “Plan using units of customer-visible functionality. “Handle five times the traffic with the same response time.” “Provide a two-click way for users to dial frequently used numbers.” As soon as a story is written, try to estimate the development effort necessary to implement it.”
### b. Identify architectural entities and relationships between entities that address key stakeholder concerns and critical software system requirements
A
The following task would be implemented in extreme programming by having stakeholders be active members of the development process in having them confirm that architectural entities and their relationships address their concerns through the stories that we develop and address in our development. “Plan using units of customer-visible functionality. “Handle five times the traffic with the same response time.” “Provide a two-click way for users to dial frequently used numbers.” As soon as a story is written, try to estimate the development effort necessary to implement it.” “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.”
	
### c. Allocate concepts, properties, characteristics, behaviors, functions, or constraints that are significant to architecture decisions of the software system to architectural entities
E 
This task would be addressed with incremental design because we would allocate concepts, properties, characteristics, behaviors, functions, or constraints that are significant to architecture decisions of the software system to architectural entities given our current knowledge of the system at that point in time and then make small improvements on these concepts, properties, etc. as our knowledge of the system grows. “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.” 
### d. Select, adapt, or develop models of the candidate architectures of the software system
G
This task would be implemented with incremental design by developing the simplest models of the candidate architectures that help address stakeholder needs, and then make subsequent improvements in order to better address these needs.“Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.” 
### e. Compose views from the models in accordance with identified viewpoints to express how the architecture addresses stakeholder concerns and meets stakeholder and system/software requirements
D
This task would be implemented with incremental design by developing the simplest models that help address stakeholder needs in order to compose views, and then make subsequent improvements in order to better address these needs.“Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work 
### f. Harmonize the architecture models and views with each other
D
This task would be implemented using continuous integration so that we ensure there is no variation between our models and views. “Integrate and test changes after no more than a couple of hours …The integration step is unpredictable, but can easily take more time than the original programming. The longer you wait to integrate, the more it costs and the more unpredictable the cost becomes.” 
 
## 4. Relate the architecture to design
	
### a. Identify software system elements that relate to architectural entities and the nature of these relationships.
F
This task would be implemented in extreme programming through user stories and continuous integration. The user stories would help us identify the software system elements that relate to architectural entities, and continuous integration would consistently reveal the nature of these relationships as we would then see what parts of our system break/succeed due to this relationship. “Integrate and test changes after no more than a couple of hours …The integration step is unpredictable, but can easily take more time than the original programming. The longer you wait to integrate, the more it costs and the more unpredictable the cost becomes.” “Plan using units of customer-visible functionality. “Handle five times the traffic with the same response time.” “Provide a two-click way for users to dial frequently used numbers.” As soon as a story is written, try to estimate the development effort necessary to implement it.”
 
### b. Define the interfaces and interactions among the software system elements and external entities
F
The task would be implemented in extreme programming by defining the interfaces and interactions among the software system elements and external entities within our user stories. “Plan using units of customer-visible functionality… When the team knows the cost of features it can split, combine, or extend scope based on what it knows about the features’ value.” 
	
### c. Partition, align and allocate requirements to architectural entities and system elements.
E
The task would be implemented in extreme programming through incremental design as we would allocate requirements to architectural entities and system elements given our understanding of the system at that point in time and then make subsequent changes when they are warranted. “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.”                                                                                                                                                            
### d. Map software system elements and architectural entities to design characteristics.
I 
The task would be implemented in extreme programming through incremental design and continuous integration as with incremental design, we would need to make constant changes to our mappings as system elements, architecture, and/or design changes and then through continuous integration, we would validate that these mappings work the way that we envision them to. “Integrate and test changes after no more than a couple of hours …The integration step is unpredictable, but can easily take more time than the original programming. The longer you wait to integrate, the more it costs and the more unpredictable the cost becomes.” “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.”    
	
### e. Define principles for the software system design and evolution.
C
In extreme programming, the task would be implemented through collective responsibility and incremental design. Collectively, the team will determine principles for the design and evolution as it seems fit, weighing in the knowledge and experiences of each member. Then, through incremental design, as the system changes these principles can subsequently evolve. “Anyone on the team can improve any part of the system at any time… Until the team has developed a sense of collective responsibility, no one is responsible and quality will deteriorate.”“Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.”  
 
## 5. Assess architecture candidates
### a. Assess each candidate architecture against constraints and requirements. 
G
This task would be implemented by including stakeholders into our team and by incremental design. We would execute this task by first assessing each candidate architecture against constraints and requirements through small, exploratory, iterative programs. These programs, combined with customer feedback, allow the team to evaluate how well each architecture meets the requirements, and to adapt the architecture to ensure it aligns with the changing needs of the project.“Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.”  “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.”
 
### b. Assess each candidate architecture against stakeholder concerns using evaluation criteria.
K
The task would be implemented through extreme programming by having stakeholders as a part of our team so that they can help define evaluation criteria that the candidate architectures can be subsequently evaluated against. “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.”

### c. Select the preferred architecture(s) and capture key decisions and rationale.
G
he task would be implemented by having stakeholders as a part of the team because in doing so, we would be able to clearly define the needs they want to have be met by our system, which we can then use to select the architecture that does so in the best and most efficient way. “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.”  
### d. Establish the architecture baseline of the selected architecture.
D
This task would be implemented through incremental design as the baseline would be established by creating the simplest solution that works and is aligned with our selected architecture. Then, in subsequent iterations and improvements the baseline is improved upon to align with the architecture and satisfy our user stories. “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.” 
 
## 6. Manage the selected architecture
### a. Formalize the architecture governance approach and specify governance-related roles and responsibilities, accountabilities, and authorities related to design, quality, security, and safety. 
E
This task would be implemented through collective responsibility by having the team come together and specify the roles, responsibilities, etc. of the architecture governance so that the team can subsequently determine if all of these criteria are satisfied. “Anyone on the team can improve any part of the system at any time… Until the team has developed a sense of collective responsibility, no one is responsible and quality will deteriorate.”

### b. Obtain explicit acceptance of the architecture by stakeholders. 
A 
In extreme programming we would likely have stakeholders as a member of our team while we are developing the software. That way, everything that is developed is tailored to their needs and subsequently we would gain acceptance of the architecture this way.  “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.”
### c. Maintain concordance and completeness of the architectural entities and their architectural characteristics. 
E
In extreme programming, this task is implemented through incremental design and continuous integration as small continuous improvements need to be made to ensure completeness and concordance of architectural entities and characteristics given our knowledge of the system and the time and then integrate these changes into our system to validate them. “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.”  “Integrate and test changes after no more than a couple of hours …The integration step is unpredictable, but can easily take more time than the original programming. The longer you wait to integrate, the more it costs and the more unpredictable the cost becomes.” 
### d. Organize, assess and control evolution of the architecture models and views to help ensure that the architectural intent is met and the architectural vision and key concepts are correctly implemented. 
D
In extreme programming, this task is implemented through incremental design and continuous integration as small continuous improvements need to be made to ensure that the architectural intent is met and the architectural vision and key concepts are correctly implemented and then integrate these changes into our system to validate them. “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.”  “Integrate and test changes after no more than a couple of hours …The integration step is unpredictable, but can easily take more time than the original programming. The longer you wait to integrate, the more it costs and the more unpredictable the cost becomes.”
### e. Maintain the architecture definition and evaluation strategy.
J
In extreme programming, this task is implemented through incremental design, continuous integration, and making stakeholders members of the team as small continuous improvements need to be made to ensure that the architecture definition and evaluation strategy is maintained given evolving requirements from our stakeholders and then integrate these changes into our system to validate them. “Invest in the design of the system every day. Strive to make the design of the system an excellent fit for the needs of the system that day. When your understanding of the best possible design leaps forward, work gradually but persistently to bring the design back into alignment with your understanding.”  “Integrate and test changes after no more than a couple of hours …The integration step is unpredictable, but can easily take more time than the original programming. The longer you wait to integrate, the more it costs and the more unpredictable the cost becomes.” “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.”
### f. Maintain traceability of the architecture. 
K
In extreme programming we would likely have stakeholders as a member of our team while we are developing in order to adequately identify stakeholder concerns. Traceability essentially ensures that we are identifying which parts of our software adequately satisfy stakeholder concerns, and traceability can be maintained when having our stakeholders working with us. “Make people whose lives and business are affected by your system part of the team … The point of customer involvement is to reduce wasted effort by putting the people with the needs in direct contact with the people who can fill those needs.”
### g. Provide key artifacts and information items that have been selected for baselines. 
J
According to extreme programming, we should only keep code and tests as artifacts, and generate other subsequent artifacts based off of this code and tests. “Maintain only the code and the tests as permanent artifacts. Generate other documents from the code and tests. Rely on social mechanisms to keep alive important history of the project.”

# 6.4.5 Design Definition process

## a. Prepare for software system design definition.

### 1.  Define the design definition strategy, consistent with the selected life cycle model and anticipated design artifacts.

a, f 

For Extreme Programming, we will be having a weekly life cycle model, relying on code, unit testing, and the stories that drive them as our artifacts of work. weekly cycle work is as follows:

"1. Review progress to date, including how actual progress for the previous week matched expected progress.

2. Have the customers pick a week’s worth of stories to implement this week.

3. Break the stories into tasks. Team members sign up for tasks and estimate them."



### 2. Select and prioritize design principles and design characteristics.

a

As Chapter 2 mentions, simplicity is one of the 5 values that guides XP development. Scalability isn’t only considered in terms of the number of users using the product, but also in terms of how it can be divided-and-conquered among a development team or company. Chapter 15 mentions one of the ways development scales is “the number of members” on the team. Additionally, XP aims to reduce the cost of change. “XP teams work hard to create conditions under which the cost of changing the software doesn’t rise catastrophically.” Chapter 7 points this out. As such, encapsulation should be an important design principle to minimize the cost of change as the project gets bigger.

### 3. Identify and plan for the necessary enabling systems or services needed to support design definition.

c, g

Through Continuous integreation and automated testing, Test Driven Development, and close colloboration, the enabling systems and services are identified in automated builds to display requirements for the system to run. They also are communicated to external sources to expand on the system for subsequent deployments

"Continuous integration should be complete enough that the eventual first deployment of the system is no big deal"



### 4. Obtain or acquire access to the enabling systems or services to be used.

c, g

Although the XP book has little comments directly on the process of obtaining enabling systems, there is some discussion on topics that can affect the said task. Early story creation helps make an “informed decision.” As such, obtaining an enabling system may not be simple and can come with its “constraints.” XP begs when such systems should be acquired or obtained so that a system isn’t front-loaded with all of them at once. 

## b. Etablish designs related to each software system element.

### 1. Transform architectural and design characteristics into the design of software system elements.

a, b, h

XP uses continuous integreation, customer feedback and stories to drive the design and allocations of the system. These aspects are changing as the system matures, so we have certainty that the software contains the necessary elements of the design to meet business goals. It also alleviates pressure to alter the design elements to meet future needs.

### 2. Define and prepare or obtain the necessary design enablers.

c

XP utilizes story points to represent the connection between design enablers and characteristics. Stories are “estimated very early in their life”, resulting in an opportunity to consider “how to get the greatest return from the smallest investment.” One capability for test-first programming is to provide parameter values initially before the implementation is done. Since documentation is primarily represented through the code, tests are the best way to represent the design; otherwise, external documentation will have to be consistently managed as the project diverges from the initial design. Incremental design is a design pattern utilized by XP to get feedback sooner, keep the project in “alignment with your understanding,” and “keep the design investment in proportion to the needs of the system so far”.

### 3. Examine design alternatives and feasibility of implementation.

e

Because XP aims to have design contribution every day, we aim to have the "simplest thing that could possibly work." Pair programming allows for "dialogue between two people", increasing the amount of examination occuring during the time of writing the code.

### 4. Refine or define the interfaces among the software system elements and with external entities.

d

By investing "in the design of the system every day" through incremental development, we can get quicker feedback on our design, which includes interfaces. Based on this feedback we can refine any existing interface definitions or define new  ones. By practicing the simplicity value of XP, we can ideally "solve only today's problem." This can lead to interfaces that are much simpler and straightforward in purpose. These interfaces will constantly evolve as more problems are solved and our system grows.

### 5. Establish the design artificts.

f

In XP we "maintain only the code and tests as permanent artifacts." Since we practice incremental design, the design is constantly changing, and the code is the only thing to consistently reflect the design in realtime. The customers are concerned with "what the system does today."

## c. Assess alternatives for obtaining software system elements.

### 1. Determine technologies required for each element composing the software system.

a, e

Test-first programming states "explicitly and objectively what the program is supposed to do." By doing so we can evaluate what technologies are needed when tests aren't passing. When the code is "loosely coupled" and "highly cohesive", we can make tests for the individual components easily.

### 2. Identify candidate alternatives for the software system elements.

e

Maintaining "customer involvement" "provides us with continuous feedback." We also drive spikes through our design with tes-first programming. These two factors can make alternative assement much faster by giving feedback sooner. We can "catch problems early" by deploying our code "at least once per day."

### 3. Assess each candidate alternative against criteria developed from expected design characteristics and element requirements to determine suitability for the intended application.

a, b, e

Again, test-first programming should follow the guideline of "explicitly and objectively" stating "what the program is supposed to do." This is effectively laying out the criteria for what the candidates should be capable of doing. 

### 4. Choose the preferred alternatives among candidate design solutions for the software system elements.

e, f

Test-first programming makes it "clearer what to do next" after testing alternatives. Improvements of test-first programming include continuous testing so that tests are ran "on every program change."

## d. Manage the design.

### 1. Capture the design and rationale.

f

"Communication is important for creating a sense of team and effective cooperation." The design and rationale is understood throughout the team from discussions and pair programming. The better a team's communication is, the better the "system structure and intent" can be conveyed.

### 2. Establish traceability between the detailed design elements, the system/software requirements, and the architectural entities of the software system architecture.

h

Stories correlate to "themes you want to address." A list of stories should follow through with a list of tests. "Write a test, make it work, write another test, and make them both work until the list is done."

### 3. Determine the status of the software system and element design.

f

In XP, the status of the software is determined by completed stories, and stories to be completed. Since each story is representative of an element of a software (or associated task for the element), it's progress on the Kaban determines progress: "If the “Done” area isn’t collecting cards, what does the team needs to improve in its planning, estimation, or execution?"

### 4. Provide key artifacts and information items that have been selected for baselines.

f

In XP, artifacts are the test cases and the code that satisfies them, according to Beck: "Customers pay for the what the system does today and what the team can make the system do tomorrow. Any artifacts contributing to these two sources of value are themselves valuable. Everything else is waste." As a result, the code and tests, along with the stories created to complete them, are provided for review.
